LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := cocos2dcpp_shared
LOCAL_MODULE_FILENAME := libcocos2dcpp
LOCAL_SRC_FILES := hellocpp/main.cpp \
../../Classes/AppDelegate.cpp\
../../Classes/HelloWorldScene.cpp\
../../Classes/Layer/MathLayer.cpp\
../../Classes/Layer/ZMBallSprite.cpp\
../../Classes/MapEdite/MapEditeScene.cpp\
../../Classes/Scene/GameScene.cpp\
../../Classes/Scene/MainMenu.cpp\
../../Classes/Scene/SelectStage.cpp\
../../Classes/data/LevelData.cpp\
../../Classes/data/PlayerManager/PlayerManager.cpp\
../../Classes/data/ResoureManager.cpp\
../../Classes/data/XmlParse.cpp\
../../Classes/tools/AnimotePack.cpp\
../../Classes/tools/CReadWriteFile.cpp\
../../Classes/tools/CSingleton.cpp\
../../Classes/tools/CWordCache.cpp\
../../Classes/tools/RDTableViewl.cpp\
../../Classes/tools/VisibleRect.cpp\
../../Classes/tools/publicFunc.cpp
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Layer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/MapEdite
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Scene
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/data
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/data/PlayerManager
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/tools

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static
include $(BUILD_SHARED_LIBRARY)
$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)	