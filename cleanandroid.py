import os
def delete_file_folder(src):
    '''delete files and folders'''

    if os.path.isfile(src):
        try:
            os.remove(src)
        except:
            pass
    elif os.path.isdir(src):
        for item in os.listdir(src):
            itemsrc=os.path.join(src,item)
            delete_file_folder(itemsrc)
        try:
            os.rmdir(src)
        except:
            pass
    else :
         os.mkdir(src) 

if __name__=='__main__':

    print 'clean obj data begin<====='
    dirname= os.path.abspath('.')+'/proj.android/obj/'
    print dirname
    print delete_file_folder(dirname)
    print 'clean obj data end======>'
    print 'clean bin data'

   
    dirname= os.path.abspath('.')+'/proj.android/bin/'
    print dirname
    print delete_file_folder(dirname)
    print 'clean bin data end======>'

    print 'clean libs data'
    dirname= os.path.abspath('.')+'/proj.android/libs/'
    print dirname
    print delete_file_folder(dirname)
    print 'clean libs data end======>'

    print 'clean libs data'
    dirname= os.path.abspath('.')+'/proj.android/assets/'
    print dirname
    print delete_file_folder(dirname)
    print 'clean libs data end======>'