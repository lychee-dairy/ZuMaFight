
#ifndef _ZHUMAFIGHT_GameScene_H_
#define _ZHUMAFIGHT_GameScene_H_

#include "cocos2d.h"
#include "MathLayer.h"
#include "LevelData.h"
#include "Header.h"

USING_NS_CC;

using namespace std;


class ZMBallSprite;
class MenuLayer;

class GameScene :public MathLayer
{
public:
	GameScene();
	~GameScene();

	static Scene* createScene();
	CREATE_FUNC(GameScene);

	virtual bool init();


	void updateBallRuning(float delta);
	bool onTouchBegan(Touch *touch, Event *unused_event);
	void onTouchMoved(Touch *touch, Event *unused_event);
	void onTouchEnded(Touch *touch, Event *unused_event);
private:
	//initGame
	void initVariables();
	void loadLevelXML();
	void loadGameInfo();
	void loadBackgroundImg();

	void checkGameOver();

	//Engine
	void initEngine();
	void loadShooter();
	void loadBalls();
	void addPro(ZMBallSprite* sp);
	void loadBallColor();
	bool makeShootBall();
	void addShooteBall();
	void updatePowerIndex(bool bHit);
	int getFirstIndexOfChain(int nLastIndex);
	int getLastIndexOfChain(int nFirstIndex);
	void FitBall(int nIndex);
	float getDistanceBetweenBalls(int nFirstIndex, int nSecondIndex);
	void onHit(int shootedIndex);

	void procBallAnimation();
	void BallAnimation();
	void BallAnimationFrom(int nFromIndex, int nToIndex, bool bIsRightArrow, float foffset);
	void BallAnimationBackPower(int nFromIndex , int nToIndex , bool bIsRightArrow);

	int getFirstIndexSameColorOfChain(int nLastIndex);
	int getLastIndexSameColorOfChain(int nFirstIndex);
	void removeBallsFromChainFrom(int nFromIndex, int nToIndex);
	void deleteBallsByDeleteTag();

	void playSecondHit(float dt);
	void playThirdHit(float dt);
	void playFourthHit(float dt);

	void calcBallKind();

	void procFastSort();
	int checkBackChainToPowerBall();

	
	void BallFlyingAnimation(float ft);
	void checkAddingFlyingBallToChain();
	bool isMyBallFrontOfBall(Point ptMyBall, Point ptOther, int nIndex);
	void procFiring();

	void procGameFinish();
	void CompleteAnimation();
	void GameOverAnimation();
	Point getNextPos(Point ptCurrent , int nIndex, float foffset);
	Point getNextPosFromLine(Point ptCurrent , Point ptTarget ,float foffset);
	Point getNextPosFromCircle(Point ptCurrent, int nFirstIndex, float foffset);
	void GameOverProc();
	void setFastSortChain(int nSortIndex);


	void updateProgress();


	void onPlayPause(EventCustom* event);
	void onPlayOn(Ref* sender);
	void onEndGame(Ref* sender);
	void onMoreGames(Ref* sender);
	void onContinue(Ref* sender);
	void onNewGames(EventCustom* event);
	void onLink(Ref* sender);


	void setContinue();
	void restartGame();
	void actionShooter();
	void rotateShooter(Point touchPos);

	void GameCompleteProc();
	void flashScore();
	void setFlyingBall(Point location);
	float getDistanceToTrace(Point ptFlying, int nIndex);
	bool isCrashingTwoBalls(Point ptFirst , Point ptSecond , int nBallWidth);

private:
	MenuLayer* menuLayer;
	
private:
	EventListenerTouchOneByOne* m_touch;

	LevelData*          m_parseOperation;       // loading leve data from XML
	//PauseLayer*         m_layerPause;

	/***************************ball**********************************/
	//new
	vector<ZMBallSprite*> m_pZMBallSprites; //ball vector
	/***************************ball****************************************/
	
	Layout* m_ui;

	Sprite*           m_sprFires[20][10];     // When the game is completed, these sprites are shown and animated.
	Sprite*           m_sprBomb[3][3];        // [x][y]:x means the count of bomb kind. y means the count of animation of one bomb.
	Layout*           m_sprShooter;           // character image
	ZMBallSprite*     m_sprShootBall;         // the image of ball to shoot from character to trace
	ZMBallSprite*     m_sprFlyingBall;        // the image of ball being flying from character to trace

	MenuItemImage*    m_btnPause;             // pause button standing on right top corner of screen

	Size              m_winSize;              // screen size
	Size              m_FlyingSpeed;          // speed of ball flying from character to trace. it is depends on DEF_Ball_FlyingSpeed and angle.
	Point             m_ptComplete;           // After game is completed, some animation will be started from this point.

	float  m_ccUpdateBallTime;
	float  m_ccUpdateFlyingTime;
	float  m_ccUpdateBombShowTime;
	float  m_ccUpdateBombBackDelay;
	float  m_ccUpdateBombStopDelay;
	float  m_ccUpdateFastSortTime;
	float  m_ccUpdateScoreUpTime;
	float  m_ccGoheadlastTime;

	double              m_fRemainedDist;        // the distance to go in a time
	

	int                 m_nBombAnimationIndex;

	bool				m_nIsStop;
	bool				m_nIsHitBomb;
	bool                m_nIsBackPro;
	bool                m_scorePro;
	bool                m_isGoPro;

	int                 m_nShootBallColor;      // the color of ball to shoot
	int                 m_nFlyingBallColor;     // the color of ball being flying to trace
	int                 m_nCompletePosIndex;    // for animating when the game is completed
	//int                 m_nFireIndex;           // for animating when the game is completed
	int                 m_nFireFrom;            // for animating when the game is completed
	int                 m_nFireTo;              // for animating when the game is completed
	int                 m_nBackPower;
	int                 m_nLevel;               // level
	int                 m_nChapter;
	int                 m_nCurrntStage;
	int                 m_nScore;               // score
	int                 m_nCountOfBallKind;
	//int                 m_BallKind[20];
	vector<int>         m_BallKind;
	int                 m_nFastSortFrom;        // When the flying ball is near to trace, the chain of ball is sorted from m_nFastSortFrom
	int                 m_nFastSortTo;          // When the flying ball is near to trace, the chain of ball is sorted to m_nFastSortTo
	int                 m_nBallCount;
	int                 m_nPowerIndex;          // the first index of chain being moving

	bool                m_bOverLayerExist;
	bool                m_bFirstSlide;
	bool                m_bAllStopTimer;
	bool                m_bIsGameOver;
	bool                m_bIsGameComplete;
	bool                m_bIsGameDone;
	bool                m_bHasFlyingBall;
	bool                m_bIsFiring;
	bool                m_bIsFastSortingChain;
	bool                m_bBallLoaded;
	bool                m_bBackgroundMusicEnable;
	bool                m_bEffectMusicEnable;
	bool                m_bDanger;

	bool m_gamePause;
	float m_gameLastTime;
	EN_ZM_TYPE m_nextType;



	float m_DEF_Ball_Offset;
	float m_DEF_SpeedUpRateByLevel;

};
#endif _ZHUMAFIGHT_GameScene_H_
