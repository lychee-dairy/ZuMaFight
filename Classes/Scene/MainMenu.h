
#ifndef _ZHUMAFIGHT_MAINMENU_H_
#define _ZHUMAFIGHT_MAINMENU_H_

#include "cocos2d.h"
#include "Header.h"
USING_NS_CC;

class MainMenu :public Layer
{
public:
	MainMenu();
	~MainMenu();

	static Scene* createScene();
	CREATE_FUNC(MainMenu);

	virtual bool init();

private:
	void addUI();
	void buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type);

private:
	MenuItemSprite *menuItemLetsStart;
};
#endif _ZHUMAFIGHT_MAINMENU_H_
