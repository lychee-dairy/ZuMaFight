
#ifndef _ZHUMAFIGHT_SelectStage_H_
#define _ZHUMAFIGHT_SelectStage_H_

#include "Header.h"
#define LEVEL_COUNT 25

class RDTableView;

class SelectStage :public Layer, cocos2d::extension::TableViewDelegate, cocos2d::extension::TableViewDataSource
{
public:
	SelectStage();
	~SelectStage();

	static Scene* createScene(int stage);

	static SelectStage* create(int stage);
	//CREATE_FUNC(SelectStage);

	virtual bool init(int stage);


	//// tabelView
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) {}
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell){}

	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);


private:
	void freshCell(TableView *table, TableViewCell* cell, int index);

	void buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type);
	void touchStage(cocos2d::Ref *sender, Widget::TouchEventType type);

	void selectUnlockAllLevels();
	void onRemoveAds();
	void onGetFreeSkip();

	void onBack();
	void onMoreGames();
	void onNewGames();
	void onLink();
	void onSelectStage(Ref* sender);

	void TurnPageLeft();
	void TurnPageRight();
	void TrunPage(int page, float time);

	LabelTTF* initLevelScore(int nIndex);
private:
	LabelTTF      *m_lblLevelScore[LEVEL_COUNT];
	Vector<MenuItem*> stageMenuArray;
	
	Layout* m_ui;
	TableView* m_pTablview;
	int m_tableCellNums;

	int m_nPage;
};
#endif _ZHUMAFIGHT_SelectStage_H_
