
//
//  RDSceneManager.cpp
//  UGameMarMonst
//
//  Created by rainDreamer on 14-12-21.
//
//

#include "RDSceneManager.h"
#include "MainMenu.h"
#include "SelectStage.h"
#include "GameScene.h"
#include "PauseLayer.h"
#include "MapEditeScene.h"

#define topZorder 5

RDSceneManager::RDSceneManager()
{
    m_enFlickSceneType=EN_SCENE_NONE;
}
void RDSceneManager::RunTargetScene(EN_SCENE_TYPE enSceneType, int nValue)
{
	Director::getInstance()->getTextureCache()->removeUnusedTextures();
    m_enFlickSceneType=enSceneType;
    switch (enSceneType) {
        case EN_SCENE_MAINMENU:
			runMainScene();
            break;
        case EN_SCENE_SELECTSTAGE:
			runSelectStageScene(nValue);
            break;
        case EN_SCENE_FIGHT:
			runFightScene();
            break;
		case EN_SCENE_MAPEDIT:
			runMapEdit();
			break;
        default:
            break;
    }
}

Scene* RDSceneManager::getCurrentScene()
{
	
	Scene* scene_ = Director::getInstance()->getRunningScene();
	if (!scene_)
	{
		scene_ = Scene::create();
	}
	return scene_;
}

void RDSceneManager::runMainScene()
{
	Scene *pScene = MainMenu::createScene();
	if (Director::getInstance()->getRunningScene()) {
		Director::getInstance()->replaceScene(pScene);
	}
	else {
		Director::getInstance()->runWithScene(pScene);
	}
}

void RDSceneManager::runSelectStageScene(int stage)
{
	Scene *pScene = SelectStage::createScene(stage);
	if (Director::getInstance()->getRunningScene()) {
		Director::getInstance()->replaceScene(pScene);
	}
	else {
		Director::getInstance()->runWithScene(pScene);
	}
}

void RDSceneManager::runFightScene()
{
	Scene *pScene = GameScene::createScene();
	if (Director::getInstance()->getRunningScene()) {
		Director::getInstance()->replaceScene(pScene);
	}
	else {
		Director::getInstance()->runWithScene(pScene);
	}
}

void RDSceneManager::runMapEdit()
{
	Scene *pScene = MapEditeScene::createScene();
	if (Director::getInstance()->getRunningScene()) {
		Director::getInstance()->replaceScene(pScene);
	}
	else {
		Director::getInstance()->runWithScene(pScene);
	}
}

void RDSceneManager::showPauseLayer()
{
	PauseLayer* pauseLayer = PauseLayer::create();
	if (pauseLayer)
	{
		getCurrentScene()->addChild(pauseLayer, topZorder);
	}
	

}


RealityFrameRate RDSceneManager::getTypeByFrameRate()
{
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	Size frameSize = glview->getFrameSize(); 
	float TScreenRatioValue = frameSize.width / frameSize.height;

	if (TScreenRatioValue <= 1.9 && TScreenRatioValue > 1.6)
	{
		return EN_FRATERATE_16To9;
	}
	else if (TScreenRatioValue <= 1.6 && TScreenRatioValue > 1.3)
	{
		return EN_FRATERATE_3To2;
	}
	else
	{
		return EN_FRATERATE_16To9;
	}
}
