#include "MainMenu.h"
#include "SelectStage.h"
#include "RDSceneManager.h"
#include "EditBoxLayer.h"

using namespace std;
MainMenu::MainMenu()
{
}

MainMenu::~MainMenu()
{
	
}

Scene* MainMenu::createScene()
{
	Scene* scene = Scene::create();
	MainMenu* layer = MainMenu::create();
	scene->addChild(layer);
	return scene;
}

bool MainMenu::init()
{
	if (!Layer::init()) return false;
	addUI();

	this->runAction(Sequence::create(DelayTime::create(2.0f), CallFunc::create([this]()
	{
		RDPlatManager::instance()->showAdmobAds(EN_ADS_BANER, true);
	}),nullptr));

	return true;
}  



void MainMenu::addUI()
{
	Layout* m_ui = dynamic_cast<Layout*>(SP_GUIReader->widgetFromJsonFile("fight/ui/main_Scene.ExportJson"));
	m_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ui->setPosition(VisibleRect::center());
	this->addChild(m_ui, 2);

	//
	Button* button = SEEK_WIDGET_BY_NAME(Button,"button_stageM", m_ui);
	button->setPositionX(VisibleRect::right().x - button->getContentSize().width*0.6f);
	button->addTouchEventListener(CC_CALLBACK_2(MainMenu::buttonOver, this));

}


void MainMenu::buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type)
{
	if (type != Widget::TouchEventType::ENDED) return;
	/*
		EditBoxLayer* layer = EditBoxLayer::create();
		layer->setPosition(Vec2::ZERO);
		this->addChild(layer, 10);*/

	UserDefault::getInstance()->setBoolForKey("FirstLaunch",false);
	RDPlatManager::instance()->showAdmobAds(EN_ADS_BANER, true);

	RDSceneManager::instance()->RunTargetScene(EN_SCENE_SELECTSTAGE);
}