//
//  RDSceneManager.h
//  UGameMarMonst
//
//  Created by linminglu on 14-5-31.
//
//

#ifndef __SceneManager__
#define __SceneManager__

#include <iostream>
#include "cocos2d.h"
#include "CSingleton.h"
#include "Header.h"


using namespace std;
using namespace cocos2d;

class RDSceneManager :public  Singleton<RDSceneManager>
{
public:
	void RunTargetScene(EN_SCENE_TYPE enSceneType, int value = 1);
   
	//get current 
	Scene* getCurrentScene();


	void showPauseLayer();

	RealityFrameRate getTypeByFrameRate();
private:
	void runMainScene();
	void runSelectStageScene(int stage);
	void runFightScene();
	void runMapEdit();

private:
	EN_SCENE_TYPE  m_enFlickSceneType;
    RDSceneManager();
   
    SINGLETON_FRIENDS
};

#endif /* defined(__UGameMarMonst__RDSceneManager__) */
