#include "GameScene.h"
#include "PlayerManager.h"
#include "publicFunc.h"
#include "SelectStage.h"
#include "ZMBallSprite.h"
#include "AnimotePack.h"
#include "MenuLayer.h"
#include "GameOverLayer.h"

const float DEF_BallAni_Interval = 0.03f;
const float DEF_FastSortChain_Interval = 0.01f;
const float DEF_Firing_Interval = 0.01f;
const float  DEF_Ball_FlyingInterval = 0.03f;
const float  DEF_BombShow_Interval = 60.0f;
const float  DEF_BombBack_Delay = 3.0f;
const float  DEF_BombStop_Delay = 3.0f;

float  DEF_Ball_FlyingSpeed = 800.0f;
float  DEF_GameOverSpeed = 10;
float  DEF_Back_Power = 10;
float  DEF_DistCalc_Offset = 2.6f;
float  DEF_CheckValue = 1.5f;

const int    DEF_FiringBall_Count = 20;
const int    DEF_Fire_Count = 8;
const int    DEF_BombAni_Count = 1;


#define FIREANI "fireAnimation"
#define UPSCORELASTTIME 5.f //触发得分道具持续的时间
#define UPSCORERATE 2.0f; //触发得分道具增加得分的比例

#define GPHEADLASTTIME 1.0f //触发前进道具持续的时间


GameScene::GameScene()
{
	AnimotePack::getInstance()->AddAnimate("fire", 0.01f, 8, FIREANI);
	Director::getInstance()->getEventDispatcher()->addCustomEventListener(PAUSEGAME, CC_CALLBACK_1(GameScene::onPlayPause, this));
	Director::getInstance()->getEventDispatcher()->addCustomEventListener(REPLAYGAME, CC_CALLBACK_1(GameScene::onNewGames, this));
	
	string filenamePlist = "effect/bomb10.plist";
	string filenamepng = "effect/bomb10.png";
	string fileJson = "effect/bomb1.ExportJson";
	ArmatureDataManager::getInstance()->addArmatureFileInfo(filenamepng, filenamePlist, fileJson);


}

GameScene::~GameScene()
{ 
	//delete
	Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(PAUSEGAME);
	Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(REPLAYGAME);
}

Scene* GameScene::createScene()
{
	Scene* scene = Scene::create();
	GameScene* layer = GameScene::create();
	scene->addChild(layer);
	return scene;
}

bool GameScene::init()
{
	if (!MathLayer::init()) return false;

	setTouchEnabled(true);
	initVariables();
	loadLevelXML();
	loadGameInfo();
	loadBackgroundImg();
	initEngine();

	this->schedule(schedule_selector(GameScene::updateBallRuning), 0.01f);

	m_touch = cocos2d::EventListenerTouchOneByOne::create();
	m_touch->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	m_touch->onTouchMoved = CC_CALLBACK_2(GameScene::onTouchMoved, this);
	m_touch->onTouchEnded = CC_CALLBACK_2(GameScene::onTouchEnded, this);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(m_touch, this);
	m_touch->setSwallowTouches(true);

	return true;
}

void GameScene::initVariables()
{
	int m_nLevel = PlayerManager::instance()->getCurrentStage();
	m_nChapter = PlayerManager::instance()->getSelectedCharacter();
	m_DEF_Ball_Offset = PlayerManager::instance()->defaultBallOffect();
	m_DEF_SpeedUpRateByLevel = PlayerManager::instance()->defaultSpeedUpByLevel();

	m_nCurrntStage = (m_nChapter - 1)*CHAPTERSTAGEMAX + m_nLevel;
	m_gamePause = false;
	m_gameLastTime = 0.0f;
	m_scorePro = false;
	m_isGoPro = false;

	m_nextType = EN_ZM_NONE;

	m_sprShootBall = nullptr;
	m_nPowerIndex = 0;
	m_ccGoheadlastTime = 0;
	m_ccUpdateScoreUpTime = 0;
	m_ccUpdateBallTime = 0;
	m_ccUpdateFlyingTime = 0;
	m_ccUpdateBombShowTime = 0;
	m_ccUpdateFastSortTime = 0;
	m_ccUpdateBombBackDelay = 0;
	m_ccUpdateBombStopDelay = 0;
	m_bIsGameOver = false;
	m_bIsGameComplete = false;
	m_pZMBallSprites.clear();
	m_bBallLoaded = false;
	m_nCountOfBallKind = 0;
	m_bHasFlyingBall = false;
	m_bIsFastSortingChain = false;
	m_bIsGameDone = false;
	m_nBackPower = 0;
	m_bFirstSlide = true;
	m_nBombAnimationIndex = 0;
	
	m_nIsStop = false;
	m_nIsHitBomb= false;
	m_nIsBackPro = false;

	m_bOverLayerExist = false;
	m_bBackgroundMusicEnable = true;
	m_bEffectMusicEnable = true;
	m_bDanger = false;
	m_nScore = 0;
}

void GameScene::loadLevelXML()
{
	Size size = Director::getInstance()->getWinSize();
	m_winSize = size;

	m_parseOperation = new LevelData();
	m_parseOperation->initWithXML(m_nCurrntStage, m_winSize);
	log("color nusm%d ",m_parseOperation->getColorCount());
}

void GameScene::loadGameInfo()
{

}

void GameScene::procGameFinish()
{
	if (m_ccUpdateFastSortTime > DEF_FastSortChain_Interval)
	{
		if (m_bIsGameComplete)
			CompleteAnimation();
		else if (m_bIsGameOver)
			GameOverAnimation();
	}

}

void GameScene::CompleteAnimation()
{
	Rect boundingRect = m_sprFires[0][0]->boundingBox();
	Point ptCurrent = m_sprFires[0][0]->getPosition();
	m_fRemainedDist = boundingRect.size.width;
	m_sprFires[0][0]->setVisible(true);
	while (m_fRemainedDist >= 0.01 || m_fRemainedDist <= -0.01)
	{
		ptCurrent = getNextPos(ptCurrent,m_nCompletePosIndex,m_fRemainedDist);
		if (m_fRemainedDist >= 0.01 || m_fRemainedDist <= -0.01)
		{
			m_nCompletePosIndex += 2;
		}
		if (m_parseOperation->getLocationCount() <= m_nCompletePosIndex + 2)
		{ 
			m_sprFires[0][0]->setVisible(false);
			m_bIsGameDone = true;
			GameCompleteProc();
			break;
		}
	}
	m_sprFires[0][0]->setPosition(ptCurrent);

	if (m_bIsGameDone)
	{
		for (int i = 0; i < DEF_Fire_Count; i++)
		{
			m_sprFires[i][i]->setVisible(false);
		}
		return;
	}

	int nMovingIndex = m_nCompletePosIndex;
	for (int i = 1; i < DEF_Fire_Count; i++)
	{
		m_sprFires[i][i]->setVisible(true);
		Point ptCurrent = m_sprFires[i - 1][i - 1]->getPosition();
		m_fRemainedDist = -boundingRect.size.width;
		while (m_fRemainedDist >= 0.01 || m_fRemainedDist <= -0.01)
		{
			ptCurrent = getNextPos(ptCurrent,nMovingIndex,m_fRemainedDist);
			if (m_fRemainedDist >= 0.01 || m_fRemainedDist <= -0.01)
			{
				nMovingIndex -= 2;
			}
			if (nMovingIndex <= 0)
			{
				m_sprFires[i][i]->setVisible(false);
				break;
			}
		}
		m_sprFires[i][i]->setPosition(ptCurrent);
	}
}

void GameScene::GameOverAnimation()
{
	for (vector<ZMBallSprite*>::iterator it = m_pZMBallSprites.begin(); it != m_pZMBallSprites.end();)
	{
		ZMBallSprite* oneBallSprite = *it;

		bool isdelete = false;

		Point ptCurrent = oneBallSprite->getPosition();
		m_fRemainedDist = DEF_GameOverSpeed;
		while (m_fRemainedDist >= 0.01 || m_fRemainedDist <= -0.01)
		{
			int numBallPosIndex = oneBallSprite->getNextTargetPosIndex();
			ptCurrent = getNextPos(ptCurrent, numBallPosIndex, m_fRemainedDist);

			if (m_parseOperation->getLocationCount() <= numBallPosIndex + 2)
			{
				isdelete = true;
				oneBallSprite->setPosition(Vec2(1000,0));
				oneBallSprite->runAction(RemoveSelf::create(true));
				it = m_pZMBallSprites.erase(it);
				if (m_pZMBallSprites.size() == 0)
				{
					m_bIsGameDone = true;
					GameOverProc();
					return;
				}
				break;
			}

			if ((m_fRemainedDist >= 0.01 || m_fRemainedDist <= -0.01)&&!isdelete)
			{
				int newNum = numBallPosIndex + 2;
				oneBallSprite->setNextTargetPosIndex(newNum);
				oneBallSprite->setLocalZOrder(m_parseOperation->getZorderByLocalPosIndex(newNum));
			}
		}


		log("currnetPosX%f PosY%f",ptCurrent.x,ptCurrent.y);
		if ((ptCurrent.x != 0 || ptCurrent.y != 0) &&!isdelete)
		{
			oneBallSprite->setPosition(ptCurrent);
		}

		if (!isdelete)
		{
			it++;
		}
		
	}
}

void GameScene::setFastSortChain(int nSortIndex)
{
	m_bIsFastSortingChain = true;
	m_nFastSortFrom = getFirstIndexOfChain(nSortIndex);
	m_nFastSortTo = nSortIndex;
}


void GameScene::updateProgress()
{
	/*if (m_pZMBallSprites.size() == 0) return;

	int nums = 0;
	for (ZMBallSprite* var :m_pZMBallSprites)
	{
	if (!var->getDeleteTag() && var->getNextTargetPosIndex() >= 2)
	{

	}
	else
	{
	nums++;
	}
	}
	float percent = nums / (float)m_parseOperation->getLocationCount();
	menuLayer->updateProgress((1-percent)*100);*/
}

void GameScene::GameOverProc()
{
	m_bOverLayerExist = true;
	PlayerManager::instance()->setLevelScore(m_nCurrntStage, m_nScore);

	ValueMap value;
	value["score"] = m_nScore;
	value["time"] = UGameTool::getTimeBySecond(m_gameLastTime);
	value["win"] = true;

	if (m_sprShootBall)
	{
		m_sprShootBall->runAction(RemoveSelf::create(true));
		m_sprShootBall = nullptr;
	}

	GameOverLayer* gamover = GameOverLayer::create();
	gamover->addGameInf(value);
	this->addChild(gamover,100);

	this->unschedule(schedule_selector(GameScene::updateBallRuning));
	RDPlatManager::instance()->showAdmobAds(EN_ADS_INSTER, true);
}

Point GameScene::getNextPos(Point ptCurrent, int nIndex, float foffset)
{
	// check if it is circle or line...
	int nFirstIndex;
	if (nIndex % 2 == 0)
	{
		nFirstIndex = nIndex;
	}
	else
	{
		nFirstIndex = nIndex - 1;
	}

	Point zeroPt; zeroPt.x = 0; zeroPt.y = 0;

	if (foffset > 0 && m_parseOperation->getLocationCount() <= nFirstIndex + 2)
		return zeroPt;

	Point ptFirst = m_parseOperation->getPosFromIndex(nFirstIndex);
	Point ptSecond = m_parseOperation->getPosFromIndex(nFirstIndex + 1);
	Point ptThird = m_parseOperation->getPosFromIndex(nFirstIndex + 2);
	if (isLine(ptFirst, ptSecond, ptThird))  // it is a line
	{
		if (foffset > 0)
			return getNextPosFromLine(ptCurrent, m_parseOperation->getPosFromIndex(nIndex + 2), foffset);
		else
			return  getNextPosFromLine(ptCurrent, m_parseOperation->getPosFromIndex(nIndex), -foffset);
	}
	return getNextPosFromCircle(ptCurrent, nFirstIndex, foffset);
}

Point GameScene::getNextPosFromLine(Point ptCurrent, Point ptTarget, float foffset)
{
	float nLargeLine = sqrt((ptCurrent.x - ptTarget.x)*(ptCurrent.x - ptTarget.x) + (ptCurrent.y - ptTarget.y)*(ptCurrent.y - ptTarget.y));
	float nDifX = ptTarget.x - ptCurrent.x;
	float nDifY = ptTarget.y - ptCurrent.y;
	float rX, rY;

	if (nLargeLine == 0)
	{
		return ptCurrent;
	}

	rX = (foffset*nDifX) / nLargeLine;
	rY = (foffset*nDifY) / nLargeLine;

	float nNewLargeLine = sqrt(rX*rX + rY*rY);

	if (nNewLargeLine > nLargeLine)
	{
		if (m_fRemainedDist < 0 && nNewLargeLine - nLargeLine > 0)
		{
			m_fRemainedDist = nLargeLine - nNewLargeLine;
		}
		else
		{
			m_fRemainedDist = nNewLargeLine - nLargeLine;
		}
		return ptTarget;
	}

	m_fRemainedDist = 0;
	ptCurrent.x += rX;
	ptCurrent.y += rY;

	return ptCurrent;
}

Point GameScene::getNextPosFromCircle(Point ptCurrent, int nFirstIndex, float foffset)
{
	Point zeroPt; zeroPt.x = 0; zeroPt.y = 0;
	if (foffset >= 0 && m_parseOperation->getLocationCount() <= (nFirstIndex + 2))
		return zeroPt;


	Point ptFirst = m_parseOperation->getPosFromIndex(nFirstIndex);
	Point ptSecond = m_parseOperation->getPosFromIndex(nFirstIndex + 1);
	Point ptThird = m_parseOperation->getPosFromIndex(nFirstIndex + 2);

	//    float nCheckValue = (ptFirst.x-ptSecond.x)*(ptThird.y-ptFirst.y)-(ptFirst.x-ptThird.x)*(ptSecond.y-ptFirst.y);
	//    if (nCheckValue > -0.01 && nCheckValue < 0.01)
	if (isLine(ptFirst, ptSecond, ptThird))
	{
		Point zeroPt; zeroPt.x = 0; zeroPt.y = 0;
		return zeroPt;
	}

	float cx = getCXOfCircleEquation(ptFirst, ptSecond, ptThird);
	float cy = getCYOfCircleEquation(ptFirst, ptSecond, ptThird);
	float r = getROfCircleEquation(ptFirst, ptSecond, ptThird);

	// get the way ( right or left)
	float alpha1 = acosfnear((m_parseOperation->getPosFromIndex(nFirstIndex).x - cx) / r);
	float alpha2 = asinfnear((m_parseOperation->getPosFromIndex(nFirstIndex).y - cy) / r);

	float firstAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	firstAlpha = (firstAlpha > 0) ? firstAlpha : firstAlpha + 2 * PI;

	alpha1 = acosfnear((m_parseOperation->getPosFromIndex(nFirstIndex + 1).x - cx) / r);
	alpha2 = asinfnear((m_parseOperation->getPosFromIndex(nFirstIndex + 1).y - cy) / r);
	float secondAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	secondAlpha = (secondAlpha > 0) ? secondAlpha : secondAlpha + 2 * PI;

	alpha1 = acosfnear((m_parseOperation->getPosFromIndex(nFirstIndex + 2).x - cx) / r);
	alpha2 = asinfnear((m_parseOperation->getPosFromIndex(nFirstIndex + 2).y - cy) / r);
	float thirdAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	thirdAlpha = (thirdAlpha > 0) ? thirdAlpha : thirdAlpha + 2 * PI;

	if (secondAlpha > firstAlpha && secondAlpha > thirdAlpha)
	{
		if (firstAlpha > PI)
			thirdAlpha += 2 * PI;
		else
			firstAlpha += 2 * PI;
	}
	else if (secondAlpha < firstAlpha && secondAlpha < thirdAlpha)
	{
		if (firstAlpha > PI)
		{
			secondAlpha += 2 * PI;
			thirdAlpha += 2 * PI;
		}
		else
		{
			firstAlpha += 2 * PI;
			secondAlpha += 2 * PI;
		}
	}

	float fArrow;
	if (firstAlpha > secondAlpha && secondAlpha > thirdAlpha)
	{
		fArrow = -1.0f;
	}
	else
	{
		fArrow = 1.0f;
	}

	// get the alpha of ptCurrent
	alpha1 = acosfnear((ptCurrent.x - cx) / r);
	alpha2 = asinfnear((ptCurrent.y - cy) / r);

	float orgAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	orgAlpha = (orgAlpha > 0) ? orgAlpha : orgAlpha + 2 * PI;
	orgAlpha = (firstAlpha < thirdAlpha && firstAlpha > 2 * PI && orgAlpha < 2 * PI) ? orgAlpha + 2 * PI : orgAlpha;
	orgAlpha = (firstAlpha > thirdAlpha && thirdAlpha > 2 * PI && orgAlpha < 2 * PI) ? orgAlpha + 2 * PI : orgAlpha;

	if (/*fArrow*fOffset < 0 && */firstAlpha > secondAlpha && firstAlpha > thirdAlpha && orgAlpha < thirdAlpha && firstAlpha - orgAlpha > PI)
	{
		orgAlpha += 2 * PI;
	}

	else if (/*fArrow*fOffset > 0 && */thirdAlpha > secondAlpha && thirdAlpha > firstAlpha && orgAlpha < firstAlpha && thirdAlpha - orgAlpha > PI)
	{
		orgAlpha += 2 * PI;
	}


	// get the next pos
	float alpha = orgAlpha + (foffset / r)*fArrow;
	while (alpha < 0)
	{
		alpha += 2 * PI;
	}
	Point ptNext;
	ptNext.x = cx + r*cos(alpha);
	ptNext.y = cy + r*sin(alpha);

	/*    if (firstAlpha > secondAlpha && orgAlpha-firstAlpha > 0 && orgAlpha-firstAlpha < 0.1)
	{
	m_fRemainedDist = 0;
	return ptNext;
	}
	else if (secondAlpha > firstAlpha && orgAlpha-thirdAlpha > 0 && orgAlpha-thirdAlpha < 0.1)
	{
	m_fRemainedDist = 0;
	return ptNext;
	}*/

	if (firstAlpha > secondAlpha)
	{
		if (orgAlpha > firstAlpha)
		{
			m_fRemainedDist = 0;
			return ptFirst;
		}
		else if (orgAlpha < thirdAlpha)
		{
			m_fRemainedDist = 0;
			return ptThird;
		}
	}
	else if (thirdAlpha > secondAlpha)
	{
		if (orgAlpha > thirdAlpha)
		{
			m_fRemainedDist = 0;
			return ptThird;
		}
		else if (orgAlpha < firstAlpha)
		{
			m_fRemainedDist = 0;
			return ptFirst;
		}
	}


	// check if the next pos is out of the last position
	/*    if ((orgAlpha > firstAlpha && orgAlpha < thirdAlpha) || (orgAlpha > thirdAlpha && orgAlpha < firstAlpha))
	{
	}
	else
	{
	m_fRemainedDist = 0;
	return ptNext;
	}
	*/
	if ((alpha >= firstAlpha && alpha <= thirdAlpha) || (alpha >= thirdAlpha && alpha <= firstAlpha))
	{
	}
	else
	{
		int nEndIndex = nFirstIndex + 2;
		if (foffset < 0)
			nEndIndex = nFirstIndex;
		ptNext = m_parseOperation->getPosFromIndex(nEndIndex);

		float fDist = (foffset > 0) ? thirdAlpha - orgAlpha : firstAlpha - orgAlpha;
		fDist = (fDist > 0) ? fDist : -fDist;
		fDist = (fDist > 2 * PI) ? fDist - 2 * PI : fDist;
		if (m_fRemainedDist > 0 && foffset - r*fDist < 0)
		{
			m_fRemainedDist = 0;
			float fDist = (ptNext.x - ptCurrent.x)*(ptNext.x - ptCurrent.x) + (ptNext.y - ptCurrent.y)*(ptNext.y - ptCurrent.y);
			fDist = sqrtf(fDist);

			return ptNext;
		}

		m_fRemainedDist = (foffset > 0) ? foffset - r*fDist : foffset + r*fDist;

		//        if (m_fRemainedDist < 0.01 && m_fRemainedDist > -0.01)
		//            m_fRemainedDist = 0;

		float fDist2 = (ptNext.x - ptCurrent.x)*(ptNext.x - ptCurrent.x) + (ptNext.y - ptCurrent.y)*(ptNext.y - ptCurrent.y);
		fDist2 = sqrtf(fDist2);

		return ptNext;
	}

	m_fRemainedDist = 0;
	float fDist3 = (ptNext.x - ptCurrent.x)*(ptNext.x - ptCurrent.x) + (ptNext.y - ptCurrent.y)*(ptNext.y - ptCurrent.y);
	fDist3 = sqrtf(fDist3);
	if (!fDist3)
	{
		ptNext.x = cx + r*cos(alpha+0.0001);
		ptNext.y = cy + r*sin(alpha + 0.0001);
	}

	return ptNext;
}

void GameScene::initEngine()
{
	loadShooter();
	loadBalls();
	loadBallColor();
	makeShootBall();
	m_nPowerIndex = 0;
}

void GameScene::loadShooter()
{
	m_sprShooter = SEEK_WIDGET_BY_NAME(Layout, "f_longtou", m_ui);
}

void GameScene::addPro(ZMBallSprite* sp)
{
	int rand = UGameTool::randint(0,m_pZMBallSprites.size()-1);
	
	/*for (int i = 0; i < m_parseOperation->getTrackFileNameJson(); i++)
	{

	}
	m_pZMBallSprites[rand]->setProType(EN_PRO_BOMB);
	*/

}

void GameScene::loadBalls()
{
	m_pZMBallSprites.clear();

	Size size = Director::getInstance()->getWinSize();
	m_winSize = size;


	m_nBallCount = m_parseOperation->getColorCount();
	log("color nums%d" , m_nBallCount);
	Rect ballRect;

	vector<int> nIndex;
	for (int i = 0; i < m_nBallCount; i++)
	{
		nIndex.push_back(i);
		ZMBallSprite* sprBall = ZMBallSprite::create((EN_ZM_TYPE)m_parseOperation->getColorFromIndex(i), m_parseOperation->getAppearBallTypes());
		sprBall->setLocalZOrder(5);
		Point ptLineFrom = m_parseOperation->getStartLineFrom();
		Point ptLineTo = m_parseOperation->getStartLineTo();
		if (i == 0)
		{
			sprBall->setPosition(ptLineTo);
			ballRect = sprBall->getBallSize();
		}
		else
		{
			ZMBallSprite* prevBall = m_pZMBallSprites[m_pZMBallSprites.size() - 1];
			Point prevPos = prevBall->getPosition();
			Point nextPos = getNextPosFromLine(prevPos, ptLineFrom, ballRect.size.width);
			sprBall->setPosition(nextPos); 
		}
		
		m_pZMBallSprites.push_back(sprBall);
		m_ui->addChild(sprBall);
	}

	if (m_nBallCount <= 2) return;

	//pro stop
	for (int i = 0; i < m_parseOperation->getStopAppearTimes(); i++)
	{
		int rand = UGameTool::randint(0, nIndex.size()-1);
		m_pZMBallSprites[nIndex[rand]]->changleToPro(EN_PRO_STOP);
		nIndex.erase(nIndex.begin()+rand);
	}

	//pro bomb
	for (int i = 0; i < m_parseOperation->getBombAppearTimes(); i++)
	{
		int rand = UGameTool::randint(0, nIndex.size() - 1);
		m_pZMBallSprites[nIndex[rand]]->changleToPro(EN_PRO_BOMB);
		nIndex.erase(nIndex.begin() + rand);
	}

	//pro back
	for (int i = 0; i < m_parseOperation->getBackAppearTimes(); i++)
	{
		int rand = UGameTool::randint(0, nIndex.size() - 1);
		m_pZMBallSprites[nIndex[rand]]->changleToPro(EN_PRO_BACK);
		nIndex.erase(nIndex.begin() + rand);
	}

	//pro go
	for (int i = 0; i < m_parseOperation->getGoAppearTimes(); i++)
	{
		int rand = UGameTool::randint(0, nIndex.size() - 1);
		m_pZMBallSprites[nIndex[rand]]->changleToPro(EN_PRO_GO);
		nIndex.erase(nIndex.begin() + rand);
	}

	//pro getscore
	for (int i = 0; i < m_parseOperation->getGetScoreAppearTimes(); i++)
	{
		int rand = UGameTool::randint(0, nIndex.size() - 1);
		m_pZMBallSprites[nIndex[rand]]->changleToPro(EN_RPO_GETSCORE);
		nIndex.erase(nIndex.begin() + rand);
	}

	//pro changecolor
	for (int i = 0; i < m_parseOperation->getChangeColorAppearTimes(); i++)
	{
		int rand = UGameTool::randint(0, nIndex.size() - 1);
		m_pZMBallSprites[nIndex[rand]]->changleToPro(EN_PRO_CHANGECOLOR);
		nIndex.erase(nIndex.begin() + rand);
	}

}

void GameScene::loadBallColor()
{
	m_BallKind.clear();
	m_nCountOfBallKind = 0;
	int nCount = m_parseOperation->getColorCount();
	bool bIsExist = false;
	for (int i = 0; i < nCount; i++)
	{
		bIsExist = false;
		if (m_BallKind.size() != 0)
		{
			for (int var : m_BallKind)
			{
				if (var == m_parseOperation->getColorFromIndex(i))
					bIsExist = true;
			}
		}
		
		if (!bIsExist)
		{
			int index = m_parseOperation->getColorFromIndex(i);
			m_BallKind.push_back(index);
			m_nCountOfBallKind++;
		}
	}
}

bool GameScene::makeShootBall()
{
	if (m_nCountOfBallKind == 0)
		return false;

	ImageView* shotball0 = SEEK_WIDGET_BY_NAME(ImageView, "shootball_0", m_ui);
	ImageView* shotball1 = SEEK_WIDGET_BY_NAME(ImageView, "shootball_1", m_ui);
	string strBall ="";
	int nIndex = UGameTool::randint(0, m_BallKind.size()-1);
	nIndex = m_BallKind[nIndex];
	if (m_nextType == EN_ZM_NONE)
	{
		strBall = "ball" + UGameTool::converInt2String(nIndex) + ".png";
		log("srpiteFrame1%s", strBall.c_str());
		shotball1->loadTexture(strBall, Widget::TextureResType::PLIST);
		shotball1->setTag(nIndex);
		log("srpiteFrame2%s", strBall.c_str());
		shotball0->loadTexture(strBall, Widget::TextureResType::PLIST);
		shotball0->setTag(nIndex);
		addShooteBall();
		m_nextType = (EN_ZM_TYPE)nIndex;  
	}
	else
	{
		strBall = "ball" + UGameTool::converInt2String(shotball1->getTag()) + ".png";
		log("srpiteFrame3%s", strBall.c_str());
		shotball0->loadTexture(strBall, Widget::TextureResType::PLIST);
		shotball0->setTag(shotball1->getTag());
		strBall = "ball" + UGameTool::converInt2String(nIndex) + ".png";
		log("srpiteFrame4%s", strBall.c_str());
		shotball1->loadTexture(strBall, Widget::TextureResType::PLIST);
		shotball1->setTag(nIndex);
		m_nextType = (EN_ZM_TYPE)shotball1->getTag();
	}

	return true;
}


void GameScene::addShooteBall()
{
	m_sprShootBall = nullptr;
	ImageView* shotball0 = SEEK_WIDGET_BY_NAME(ImageView, "shootball_0", m_ui);
	Point pt = shotball0->getParent()->convertToWorldSpace(shotball0->getPosition());
	if (shotball0->getTag() <= 0 || shotball0->getTag() > EN_ZM_Ball6)
		shotball0->setTag(EN_ZM_Ball1);
	m_sprShootBall = ZMBallSprite::create((EN_ZM_TYPE)shotball0->getTag(),m_parseOperation->getAppearBallTypes());
	m_parseOperation->setShooterPos(pt);
	m_sprShootBall->setPosition(pt);
	m_sprShootBall->setLocalZOrder(20);
	m_ui->addChild(m_sprShootBall);
}

void GameScene::updatePowerIndex(bool bHit)
{
	int nLastIndex = m_pZMBallSprites.size() - 1;
	if (nLastIndex < 0)
		return;

	int newPowerIndex = getFirstIndexOfChain(nLastIndex);

	if (newPowerIndex != m_nPowerIndex && m_nPowerIndex <= nLastIndex)
	{
		FitBall(m_nPowerIndex);
	}

	for (int i = 0; i < m_pZMBallSprites.size(); i++) {
		Rect ballRect = Rect(0, 0, BOXRECT, BOXRECT);
		float fDist1 = 0, fDist2 = 0;
		if (i == 0)
			fDist1 = ballRect.size.width;
		else if (getDistanceBetweenBalls(i - 1, i) > ballRect.size.width + DEF_DistCalc_Offset)
		{
			fDist1 = ballRect.size.width;
		}
		else
			fDist1 = getDistanceBetweenBalls(i - 1, i);

		if (i >= m_pZMBallSprites.size() - 1)
			fDist2 = ballRect.size.width;
		else if (getDistanceBetweenBalls(i + 1, i) > ballRect.size.width + DEF_DistCalc_Offset)
		{
			fDist2 = ballRect.size.width;
		}
		else
			fDist2 = getDistanceBetweenBalls(i + 1, i);

		if (!m_bIsFastSortingChain && (absf(fDist1 - ballRect.size.width) > DEF_CheckValue || absf(fDist2 - ballRect.size.width) > DEF_CheckValue))
		{
			FitBall(i);
		}
	}

	if (newPowerIndex < m_nPowerIndex && bHit && m_nPowerIndex <= nLastIndex)
	{
		int color1 = m_pZMBallSprites[m_nPowerIndex - 1]->getBallType();
		int color2 = m_pZMBallSprites[m_nPowerIndex]->getBallType();
		if (color1 == color2)
		{
			onHit(m_nPowerIndex);
			m_nBackPower = DEF_Back_Power;
		}
	}

	m_nPowerIndex = newPowerIndex;
}


void GameScene::FitBall(int nIndex)
{
	Rect ballRect = Rect(0, 0, BOXRECT, BOXRECT);
	float fDist1 = 0, fDist2 = 0;
	if (nIndex == 0)
		fDist1 = ballRect.size.width;
	else if (getDistanceBetweenBalls(nIndex - 1, nIndex) > ballRect.size.width + DEF_DistCalc_Offset)
	{
		fDist1 = ballRect.size.width;
	}
	else
		fDist1 = getDistanceBetweenBalls(nIndex - 1, nIndex);

	if (nIndex >= m_pZMBallSprites.size() - 1)
		fDist2 = ballRect.size.width;
	else if (getDistanceBetweenBalls(nIndex + 1, nIndex) > ballRect.size.width + DEF_DistCalc_Offset)
	{
		fDist2 = ballRect.size.width;
	}
	else
		fDist2 = getDistanceBetweenBalls(nIndex + 1, nIndex);

	int nFirst = getFirstIndexOfChain(nIndex);

	 if (absf(fDist1-ballRect.size.width) <= DEF_DistCalc_Offset || absf(fDist2-ballRect.size.width) <= DEF_DistCalc_Offset)
	{
		float fRemained = ballRect.size.width - fDist1;
		if (fRemained != 0)
		{
			BallAnimationFrom(nFirst, nIndex - 1, true, fRemained);
		}
		fRemained = ballRect.size.width - fDist2;
		if (fRemained != 0)
		{ 
			BallAnimationFrom(nFirst, nIndex, true, fRemained);
		}
	}
}

float GameScene::getDistanceBetweenBalls(int nFirstIndex, int nSecondIndex)
{
	if (nFirstIndex < 0 ||m_pZMBallSprites.size() == 0|| nFirstIndex > m_pZMBallSprites.size() - 1 || nSecondIndex < 0 || nSecondIndex > m_pZMBallSprites.size() - 1) return 0;
	
	//    printf ("ObjectAtIndex 10\n")
	ZMBallSprite* sprFirst = m_pZMBallSprites[nFirstIndex];
	//    printf ("ObjectAtIndex 11\n");
	ZMBallSprite* sprSecond = m_pZMBallSprites[nSecondIndex];

	Point ptFirst = sprFirst->getPosition();
	Point ptSecond = sprSecond->getPosition();
	float fDist = (ptFirst.x - ptSecond.x)*(ptFirst.x - ptSecond.x) + (ptFirst.y - ptSecond.y)*(ptFirst.y - ptSecond.y);

	fDist = sqrt(fDist);

	return fDist;
}

int GameScene::getFirstIndexSameColorOfChain(int nLastIndex)
{
	int nFirstIndexOfChain = getFirstIndexOfChain(nLastIndex);

	int nFirstIndexSameColor = nLastIndex;
	int nShootedColorNum = m_pZMBallSprites[nLastIndex]->getBallType();
	int nNum = m_pZMBallSprites[nFirstIndexSameColor]->getBallType();

	while (nShootedColorNum == nNum && nFirstIndexSameColor > nFirstIndexOfChain)
	{
		nFirstIndexSameColor--;
		nNum = m_pZMBallSprites[nFirstIndexSameColor]->getBallType();
	}
	if (nShootedColorNum != nNum)
		nFirstIndexSameColor++;

	return nFirstIndexSameColor;
}

int GameScene::getLastIndexSameColorOfChain(int nFirstIndex)
{
	int nLastIndexOfChain = getLastIndexOfChain(nFirstIndex);
	int nLastIndexSameColor = nFirstIndex;
	int nShootedColorNum = m_pZMBallSprites[nFirstIndex]->getBallType();
	int nNum = m_pZMBallSprites[nLastIndexSameColor]->getBallType();
	while (nShootedColorNum == nNum && nLastIndexSameColor < nLastIndexOfChain)
	{
		nLastIndexSameColor++;
		nNum = m_pZMBallSprites[nLastIndexSameColor]->getBallType();
	}
	if (nShootedColorNum != nNum)
		nLastIndexSameColor--;

	return nLastIndexSameColor;
}

void GameScene::deleteBallsByDeleteTag()
{
	for (vector<ZMBallSprite*>::iterator it = m_pZMBallSprites.begin(); it != m_pZMBallSprites.end();)
	{
		ZMBallSprite* sprite = *it;

		if (*it && sprite->getDeleteTag())
		{
			Sprite* sp = Sprite::create();
			sp->setPosition(sprite->getPosition());
			Sequence* se = Sequence::create(AnimotePack::getInstance()->getAnimateByName(FIREANI), RemoveSelf::create(true), NULL);
			sp->runAction(se);
			this->addChild(sp, 100);
			sprite->runAction(RemoveSelf::create(true));

			it = m_pZMBallSprites.erase(it);
		}
		else
		{
			it++;
		}

	}


	if (m_pZMBallSprites.size() == 0)
	{
		m_sprFires[0][0]->setPosition(m_ptComplete);
		m_bIsGameComplete = true;

		return;
	}

	calcBallKind();

	updatePowerIndex(false);
}

void GameScene::removeBallsFromChainFrom(int nFromIndex, int nToIndex)
{
	int location = nFromIndex;
	int lenth = nToIndex - nFromIndex + 1;

	if (m_nPowerIndex > nToIndex)
		m_nPowerIndex -=lenth;
	else if (m_nPowerIndex >= nFromIndex && m_nPowerIndex <= nToIndex)
	{
		m_nPowerIndex = nToIndex + 1 -lenth;
		if (m_pZMBallSprites.size() - 1 <= nToIndex)
			m_nPowerIndex--;
	}

	ZMBallSprite* sprBall = m_pZMBallSprites[0];
	int oneNum = sprBall->getNextTargetPosIndex();

	m_ptComplete = sprBall->getPosition();
	m_nCompletePosIndex = oneNum;


	int addscore= 10 * lenth*lenth;
	log("add score %d", addscore);
	if (m_scorePro)
	{
		//触发得分道具
		addscore = addscore*UPSCORERATE;
		log("add score %d", addscore);
	}
	m_nScore += addscore;
	
	menuLayer->addSore(addscore);

}

bool GameScene::isMyBallFrontOfBall(Point ptMyBall, Point ptOther, int nIndex)
{
	if (m_parseOperation->getLocationCount() <= nIndex + 2)
		return false;

	Point ptFirst = m_parseOperation->getPosFromIndex(nIndex);
	Point ptSecond = m_parseOperation->getPosFromIndex(nIndex + 1);
	Point ptThird = m_parseOperation->getPosFromIndex(nIndex + 2);

	//    float nCheckValue = (ptFirst.x-ptSecond.x)*(ptThird.y-ptFirst.x)-(ptFirst.x-ptThird.x)*(ptSecond.y-ptFirst.y);
	//    if (nCheckValue > -0.01 && nCheckValue < 0.01)
	if (isLine(ptFirst, ptSecond, ptThird))
	{
		float distMy = sqrt((ptFirst.x - ptMyBall.x)*(ptFirst.x - ptMyBall.x) + (ptFirst.y - ptMyBall.y)*(ptFirst.y - ptMyBall.y));
		float distOther = sqrt((ptFirst.x - ptOther.x)*(ptFirst.x - ptOther.x) + (ptFirst.y - ptOther.y)*(ptFirst.y - ptOther.y));
		if (distOther > distMy)
			return false;
		else
			return true;
	}

	float cx = getCXOfCircleEquation(ptFirst, ptSecond, ptThird);
	float cy = getCYOfCircleEquation(ptFirst, ptSecond, ptThird);
	float r = getROfCircleEquation(ptFirst, ptSecond, ptThird);

	float alpha1 = acosfnear((ptFirst.x - cx) / r);
	float alpha2 = asinfnear((ptFirst.y - cy) / r);
	float firstAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	firstAlpha = (firstAlpha > 0) ? firstAlpha : firstAlpha + 2 * PI;

	alpha1 = acosfnear((ptSecond.x - cx) / r);
	alpha2 = asinfnear((ptSecond.y - cy) / r);
	float secondAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	secondAlpha = (secondAlpha > 0) ? secondAlpha : secondAlpha + 2 * PI;

	alpha1 = acosfnear((ptThird.x - cx) / r);
	alpha2 = asinfnear((ptThird.y - cy) / r);
	float thirdAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	thirdAlpha = (thirdAlpha > 0) ? thirdAlpha : thirdAlpha + 2 * PI;

	if (secondAlpha > firstAlpha && secondAlpha > thirdAlpha)
	{
		if (firstAlpha > PI)
			thirdAlpha += 2 * PI;
		else
			firstAlpha += 2 * PI;
	}
	else if (secondAlpha < firstAlpha && secondAlpha < thirdAlpha)
	{
		if (firstAlpha > PI)
		{
			secondAlpha += 2 * PI;
			thirdAlpha += 2 * PI;
		}
		else
		{
			firstAlpha += 2 * PI;
			secondAlpha += 2 * PI;
		}
	}

	float fArrow;
	if (firstAlpha > secondAlpha && secondAlpha > thirdAlpha)
	{
		fArrow = -1.0f;
	}
	else
	{
		fArrow = 1.0f;
	}

	alpha1 = acosfnear((ptMyBall.x - cx) / r);
	alpha2 = asinfnear((ptMyBall.y - cy) / r);
	float myAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	myAlpha = (myAlpha > 0) ? myAlpha : myAlpha + 2 * PI;
	myAlpha = (firstAlpha < thirdAlpha && firstAlpha > 2 * PI && myAlpha < 2 * PI) ? myAlpha + 2 * PI : myAlpha;
	myAlpha = (firstAlpha > thirdAlpha && thirdAlpha > 2 * PI && myAlpha < 2 * PI) ? myAlpha + 2 * PI : myAlpha;

	if (/*fArrow*fOffset < 0 && */firstAlpha > secondAlpha && firstAlpha > thirdAlpha && myAlpha < thirdAlpha && firstAlpha - myAlpha > PI)
	{
		myAlpha += 2 * PI;
	}

	else if (/*fArrow*fOffset > 0 && */thirdAlpha > secondAlpha && thirdAlpha > firstAlpha && myAlpha < firstAlpha && thirdAlpha - myAlpha > PI)
	{
		myAlpha += 2 * PI;
	}

	alpha1 = acosfnear((ptOther.x - cx) / r);
	alpha2 = asinfnear((ptOther.y - cy) / r);
	float otherAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	otherAlpha = (otherAlpha > 0) ? otherAlpha : otherAlpha + 2 * PI;
	otherAlpha = (firstAlpha < thirdAlpha && firstAlpha > 2 * PI && otherAlpha < 2 * PI) ? otherAlpha + 2 * PI : otherAlpha;
	otherAlpha = (firstAlpha > thirdAlpha && thirdAlpha > 2 * PI && otherAlpha < 2 * PI) ? otherAlpha + 2 * PI : otherAlpha;

	if (/*fArrow*fOffset < 0 && */firstAlpha > secondAlpha && firstAlpha > thirdAlpha && otherAlpha < thirdAlpha && firstAlpha - otherAlpha > PI)
	{
		otherAlpha += 2 * PI;
	}

	else if (/*fArrow*fOffset > 0 && */thirdAlpha > secondAlpha && thirdAlpha > firstAlpha && otherAlpha < firstAlpha && thirdAlpha - otherAlpha > PI)
	{
		otherAlpha += 2 * PI;
	}

	if ((secondAlpha > firstAlpha && myAlpha < otherAlpha) ||
		(secondAlpha < firstAlpha && myAlpha > otherAlpha))
	{
		return false;
	}

	return true;
}

void GameScene::checkAddingFlyingBallToChain()
{
	if (!m_sprFlyingBall)
	{
		log("m_sprFlyingBall is Null");
		return;
	}
		

	int nCount = m_pZMBallSprites.size();
	Point ptFlying = m_sprFlyingBall->getPosition();
	Rect ballRect = m_sprFlyingBall->getBallSize();
	
	////get hit ball Index
	bool bIsCrashing = false;
	for (int i = 0; i < nCount; i++)
	{
		ZMBallSprite* sprBall = m_pZMBallSprites[i];
		
		if (!sprBall || sprBall->getDeleteTag()) continue;
		int numFirst = sprBall->getNextTargetPosIndex();
		if (numFirst >= m_parseOperation->getLocationCount() - 1) continue;


		Point ptChain = sprBall->getPosition();
		float fDistance = sqrt((ptFlying.x - ptChain.x)*(ptFlying.x - ptChain.x) + (ptFlying.y - ptChain.y)*(ptFlying.y - ptChain.y));
		
		//碰撞发生
		if (fDistance < ballRect.size.width)
		{
			Point ptFirst = m_parseOperation->getPosFromIndex(numFirst);
			Point ptSecond = m_parseOperation->getPosFromIndex(numFirst + 1);
			Point ptThird = m_parseOperation->getPosFromIndex(numFirst + 2);

			if (!isLine(ptFirst, ptSecond, ptThird))
			{
				float cx = getCXOfCircleEquation(ptFirst, ptSecond, ptThird);
				float cy = getCYOfCircleEquation(ptFirst, ptSecond, ptThird);
				float r = getROfCircleEquation(ptFirst, ptSecond, ptThird);
				float x = intersectionEquationXOfLineAndCircle(ptFlying, m_parseOperation->getShooterPos(), cx, cy, r);
				float y = intersectionEquationYOfLineAndCircle(ptFlying, m_parseOperation->getShooterPos(), cx, cy, r);
				Point newPt;
				newPt.x = x;
				newPt.y = y;

				if (newPt.x != 0 && newPt.y != 0)
				{
					newPt = getNextPosFromCircle(newPt, numFirst, 0);
					if (newPt.x != 0 && newPt.y != 0)
					{
						ptFlying = newPt;
						m_sprFlyingBall->setPosition(newPt);
					}
					else
					{
						ptFlying = ptChain;
						m_sprFlyingBall->setPosition(ptChain);
					}
				}
				else
				{
					ptFlying = ptChain;
					m_sprFlyingBall->setPosition(ptChain);
				}
			}
			else
			{
				// y=ax+b y=cx+d
				float a = getAOfLineEquation(ptFirst, ptThird);
				float b = getBOfLineEquation(ptFirst, ptThird);
				float c = getAOfLineEquation(ptFlying, m_parseOperation->getShooterPos());
				float d = getBOfLineEquation(ptFlying, m_parseOperation->getShooterPos());

				float x = 0, y = 0;

				if (a == 0 && b == 0)
				{
					x = ptFirst.x;
					y = c*x + d;
				}
				else if (c == 0 && d == 0)
				{
					x = ptFlying.x;
					y = a*x + b;
				}
				else if (a != c)
				{
					x = (d - b) / (a - c);
					y = a*x + b;
				}


				Point newPt; newPt.x = x; newPt.y = y;
				if (newPt.x != 0 && newPt.y != 0)
				{
					float distorg = sqrtf((ptFirst.x - ptThird.x)*(ptFirst.x - ptThird.x) + (ptFirst.y - ptThird.y)*(ptFirst.y - ptThird.y));
					float dist1 = sqrtf((ptFirst.x - newPt.x)*(ptFirst.x - newPt.x) + (ptFirst.y - newPt.y)*(ptFirst.y - newPt.y));
					float dist2 = sqrtf((newPt.x - ptThird.x)*(newPt.x - ptThird.x) + (newPt.y - ptThird.y)*(newPt.y - ptThird.y));

					if (dist1 > distorg || dist2 > distorg)
					{
						ptFlying = ptChain;
						m_sprFlyingBall->setPosition(ptChain);
					}
					else
					{
						ptFlying = newPt;
						m_sprFlyingBall->setPosition(newPt);
					}
				}
				else
				{
					ptFlying = ptChain;
					m_sprFlyingBall->setPosition(ptChain);
				}

			}

			bIsCrashing = true;
			break;
		}

	}


	int nMyIndex = 0;
	if (bIsCrashing)
	{
		//hit

		float fMinDistance = 10000.0f;
		int nInsertIndex = 0;

		for (int i = 0; i < nCount; i++)
		{
			ZMBallSprite* sprBall = m_pZMBallSprites[i];

			Point ptChain = sprBall->getPosition();
			float fDistance = sqrt((ptFlying.x - ptChain.x)*(ptFlying.x - ptChain.x) + (ptFlying.y - ptChain.y)*(ptFlying.y - ptChain.y));
			if (fDistance < ballRect.size.width && fDistance < fMinDistance)
			{
				nInsertIndex = i;
				fMinDistance = fDistance;
			}
		}

		ZMBallSprite* sprBall = m_pZMBallSprites[nInsertIndex];

		if (sprBall->getProType() == EN_PRO_CHANGECOLOR)
		{
			sprBall->changleToPro(EN_PRO_NONE);
			int	nFromIndex = (nInsertIndex - CHANGLECOLORNUMS <= 0) ? 0 : (nInsertIndex - CHANGLECOLORNUMS);
			int	nToIndex = (nInsertIndex + CHANGLECOLORNUMS >= m_pZMBallSprites.size() - 1) ? (m_pZMBallSprites.size() - 1) : (nInsertIndex + CHANGLECOLORNUMS);

			for (int i = nFromIndex; i <= nToIndex; i++)
			{
				m_pZMBallSprites[i]->changleType(sprBall->getBallType());
			}
			

			m_ui->removeChild(m_sprFlyingBall, true);
			m_sprFlyingBall = NULL;
			m_bHasFlyingBall = false;
			return;
		}

		int numBallPosIndex = sprBall->getNextTargetPosIndex();
		bool isFront = isMyBallFrontOfBall(ptFlying, sprBall->getPosition(), numBallPosIndex);

		if (!isFront)
			nMyIndex = nInsertIndex + 1;
		else
			nMyIndex = nInsertIndex;

		int prevN;
		if (nMyIndex == getFirstIndexOfChain(nInsertIndex))
		{
			prevN = sprBall->getNextTargetPosIndex();
		}
		else
		{
			prevN = m_pZMBallSprites[nMyIndex - 1]->getNextTargetPosIndex();
		}
		

		m_pZMBallSprites.insert(m_pZMBallSprites.begin() + nMyIndex, m_sprFlyingBall);
		m_sprFlyingBall->setNextTargetPosIndex(prevN);
		m_sprFlyingBall->setLocalZOrder(m_parseOperation->getZorderByLocalPosIndex(prevN));

		m_bHasFlyingBall = false;
		setFastSortChain(nMyIndex);
	}

}

void GameScene::BallFlyingAnimation(float ft)
{
	if (!m_sprFlyingBall) return;
	Point pt = m_sprFlyingBall->getPosition();
	pt.x += m_FlyingSpeed.width*ft;
	pt.y += m_FlyingSpeed.height*ft;

	if (pt.x < 0 || pt.x > m_winSize.width || pt.y < 0 || pt.y > m_winSize.height)
	{
		m_bHasFlyingBall = false;
		m_sprFlyingBall->removeFromParent();
		m_sprFlyingBall = NULL;
		return;
	}

	m_sprFlyingBall->setPosition(pt);
}

void GameScene::procFiring()
{
	
	/*m_nFireIndex++;
	if (m_nFireIndex >= DEF_Fire_Count)
	{
	for (int i = m_nFireFrom; i <= m_nFireTo; i++)
	{
	m_sprFires[i - m_nFireFrom][m_nFireIndex - 1]->setVisible(false);
	}
	m_bIsFiring = false;
	m_nFireIndex = 0;
	removeBallsFromChainFrom(m_nFireFrom, m_nFireTo);
	}*/
}

int GameScene::checkBackChainToPowerBall()
{
	if (m_nPowerIndex <= 0 || m_nPowerIndex >m_pZMBallSprites.size()-1)
		return -1;

	int prev = m_pZMBallSprites[m_nPowerIndex - 1]->getBallType();
	int power = m_pZMBallSprites[m_nPowerIndex]->getBallType();

	if (prev == power)
	{
		return getFirstIndexOfChain(m_nPowerIndex - 1);
	}

	return -1;
}

void GameScene::procFastSort()
{
	int nFirstIndex = checkBackChainToPowerBall();

	if (nFirstIndex >= 0 && m_bIsFastSortingChain == false)
	{
		BallAnimationFrom(nFirstIndex, m_nPowerIndex - 1, false, 0);
	}

	if (m_bIsFastSortingChain)
	{
		bool bIsNormal = true;
		if (m_nFastSortTo != 0 && m_nFastSortTo < m_pZMBallSprites.size() - 1)
		{
			if (getDistanceBetweenBalls(m_nFastSortTo - 1, m_nFastSortTo) > getDistanceBetweenBalls(m_nFastSortTo - 1, m_nFastSortTo + 1))
			{
				BallAnimationFrom(m_nFastSortTo, m_nFastSortTo, true, 0);
				bIsNormal = false;
			}
			if (getDistanceBetweenBalls(m_nFastSortTo, m_nFastSortTo + 1) > getDistanceBetweenBalls(m_nFastSortTo - 1, m_nFastSortTo + 1))
			{
				BallAnimationFrom(m_nFastSortTo, m_nFastSortTo, false, 0);
				bIsNormal = false;
			}
		}

		if (bIsNormal)
		{
			Rect ballRect = Rect(0,0,BOXRECT,BOXRECT);
			float fDist1 = 0, fDist2 = 0;
			if (m_nFastSortTo == 0)
				fDist1 = ballRect.size.width;
			else if (getDistanceBetweenBalls(m_nFastSortTo - 1, m_nFastSortTo) > ballRect.size.width + DEF_DistCalc_Offset)
			{
				fDist1 = ballRect.size.width;
			}
			else
				fDist1 = getDistanceBetweenBalls(m_nFastSortTo - 1, m_nFastSortTo);

			if (m_nFastSortTo >= m_pZMBallSprites.size() - 1)
				fDist2 = ballRect.size.width;
			else if (getDistanceBetweenBalls(m_nFastSortTo + 1, m_nFastSortTo) > ballRect.size.width + DEF_DistCalc_Offset)
			{
				fDist2 = ballRect.size.width;
			}
			else
				fDist2 = getDistanceBetweenBalls(m_nFastSortTo + 1, m_nFastSortTo);

			m_nFastSortFrom = getFirstIndexOfChain(m_nFastSortTo);

			if (absf(fDist1 - ballRect.size.width) <= DEF_DistCalc_Offset && absf(fDist2 - ballRect.size.width) <= DEF_DistCalc_Offset)
			{
				FitBall(m_nFastSortTo);
				m_bIsFastSortingChain = false;
			}
			else
			{
				if (absf(fDist1 - ballRect.size.width) > DEF_DistCalc_Offset && m_nFastSortFrom < m_nFastSortTo)
					BallAnimationFrom(m_nFastSortFrom, m_nFastSortTo - 1, true, 0);
				else if (absf(fDist2 - ballRect.size.width) > DEF_DistCalc_Offset)
					BallAnimationFrom(m_nFastSortFrom, m_nFastSortTo, true, 0);
			}
		}


		if (m_bIsFastSortingChain == false)
		{
			onHit(m_nFastSortTo);
		}
	}
}

void GameScene::flashScore()
{
	
}

float GameScene::getDistanceToTrace(Point ptFlying, int nIndex)
{
	int nMyIndex = nIndex;
	float fDistance;
	if (nMyIndex % 2 == 1)
		nMyIndex--;

	Point ptFirst = m_parseOperation->getPosFromIndex(nMyIndex);
	Point ptSecond = m_parseOperation->getPosFromIndex(nMyIndex + 1);
	Point ptThird = m_parseOperation->getPosFromIndex(nMyIndex + 2);

	if (isLine(ptFirst,ptSecond,ptThird))    // if it is line, ...
	{   // y = ax+b
		float a = getAOfLineEquation(ptFirst,ptThird);
		float b = getBOfLineEquation(ptFirst,ptThird);
		float x1 = ptFlying.x;
		float y1 = a*x1 + b;

		if (a == 0 && b == 0)
			fDistance = ptFlying.x - ptFirst.x;
		else
			fDistance = y1 - ptFlying.y;
	}
	else // if it is circle, ...
	{
		float cx = getCXOfCircleEquation(ptFirst,ptSecond,ptThird);
		float cy = getCYOfCircleEquation(ptFirst,ptSecond,ptThird);
		float r = getROfCircleEquation(ptFirst,ptSecond,ptThird);
		float x1 = ptFlying.x;
		float y1 = sqrt(absf(r*r - (x1 - cx)*(x1 - cx))) + cy;
		float y2 = cy - sqrt(absf(r*r - (x1 - cx)*(x1 - cx)));

		float fDistance1 =absf(y1 - ptFlying.y);
		float fDistance2 =absf(y2 - ptFlying.y);

		fDistance = (fDistance1 > fDistance2) ? fDistance2 : fDistance1;
	}

	if (fDistance < 0)
		fDistance = -fDistance;
	return fDistance;
}

bool GameScene::isCrashingTwoBalls(Point ptFirst, Point ptSecond, int nBallWidth)
{
	float fDist = (ptFirst.x - ptSecond.x)*(ptFirst.x - ptSecond.x) + (ptFirst.y - ptSecond.y)*(ptFirst.y - ptSecond.y);
	fDist = sqrt(fDist);

	if (fDist < nBallWidth)
		return true;

	return false;
}

void GameScene::calcBallKind()
{
	m_nCountOfBallKind = 0;
	m_BallKind.clear();

	bool bIsExist = false;
	for (ZMBallSprite* var :m_pZMBallSprites)
	{
		bIsExist = false;
		for (int varint : m_BallKind)
		{
			if (varint == var->getBallType())
			{
				bIsExist = true;
			}
		}
	
		if (!bIsExist)
		{
			m_BallKind.push_back(var->getBallType());
			m_nCountOfBallKind++;
		}

	}

}

void GameScene::playSecondHit(float dt)
{
	/*if (gbSoundEnable)
		[[SimpleAudioEngine sharedEngine] playEffect:@"blank.mp3"];*/
	this->scheduleOnce(schedule_selector(GameScene::playThirdHit), 0.1f);
}

void GameScene::playThirdHit(float dt)
{
	/*if (gbSoundEnable)
		[[SimpleAudioEngine sharedEngine] playEffect:@"blank.mp3"];*/
	this->scheduleOnce(schedule_selector(GameScene::playFourthHit), 0.1f);
}
void GameScene::playFourthHit(float dt)
{
	/*if (gbSoundEnable)
		[[SimpleAudioEngine sharedEngine] playEffect:@"blank.mp3"];*/
}

void GameScene::BallAnimation()
{
	int ncount = m_pZMBallSprites.size();

	if (m_nBackPower < 0)
		m_nBackPower = 0;
	if (m_nBackPower == 0)
	{
		if (m_nIsBackPro)
			BallAnimationFrom(0, ncount - 1, false, 0);
		else
		{
			BallAnimationFrom(m_nPowerIndex, ncount - 1, true, 0);
		}
			
	}
	else
	{
		BallAnimationBackPower(getFirstIndexOfChain(ncount - 1), ncount - 1, false);
		m_nBackPower -= 2;
		if (m_nBackPower < 0)
			m_nBackPower = 0;
	}
}

void GameScene::procBallAnimation()
{
	if (m_parseOperation->getLocationCount() >= 1)
	{
		if (!m_nIsStop)
			BallAnimation();
	}
}

void GameScene::onHit(int shootedIndex)
{
	int nFirstIndexSameColor = getFirstIndexSameColorOfChain(shootedIndex);
	int nLastIndexSameColor = getLastIndexSameColorOfChain(shootedIndex);

	if (nLastIndexSameColor - nFirstIndexSameColor >= 2)    // more than 3
	{
		int nFromIndex = nFirstIndexSameColor;
		int nToIndex = nLastIndexSameColor;

		for (int i = nFirstIndexSameColor; i <= nLastIndexSameColor; i++)
		{
			ZMBallSprite* sprBall = m_pZMBallSprites[i];
			switch (sprBall->getProType()) {
			case EN_PRO_BACK:
				m_ccUpdateBombBackDelay = DEF_BombBack_Delay;
				m_nIsBackPro = true;
				break;
			case EN_PRO_BOMB:
			{
				Armature* ar = Armature::create("bomb1");
				ar->setPosition(m_pZMBallSprites[shootedIndex]->getPosition());
				ar->getAnimation()->play("bomb1");
				this->addChild(ar,10);

				nFromIndex = (nFromIndex - 2 <= 0)? 0 : (nFromIndex - 2);
				nToIndex =(nToIndex + 2 >= m_pZMBallSprites.size() - 1) ? (m_pZMBallSprites.size() - 1) : (nToIndex + 2);

			}
				break;
			case EN_PRO_STOP:
				m_nIsStop = true;
				m_ccUpdateBombStopDelay = DEF_BombStop_Delay;
				break;
			case EN_PRO_CHANGECOLOR:
			{
				/*nFromIndex = (nFromIndex - 2 <= 0) ? 0 : (nFromIndex - 2);
				nToIndex = (nToIndex + 2 >= m_pZMBallSprites.size() - 1) ? (m_pZMBallSprites.size() - 1) : (nToIndex + 2);

				for (int i = nFromIndex; i <= nToIndex; i++)
				{
				m_pZMBallSprites[i]->changleType(sprBall->getBallType());
				}
				sprBall->setProType(EN_PRO_NONE);*/

				return;
			}
				break;
			case EN_RPO_GETSCORE:
			{
				m_ccUpdateScoreUpTime = UPSCORELASTTIME;
				m_nIsBackPro = true;
			}
				break;
			case EN_PRO_GO:
			{
				m_ccGoheadlastTime = GPHEADLASTTIME;
				m_isGoPro = true;
			}
			default:
				break;
			}
		}

		//setDeleteFlag
		for (int i = nFromIndex; i <= nToIndex; i++)
		{
			ZMBallSprite* sprBall = m_pZMBallSprites[i];
			sprBall->setDeleteTag(true);
		}

		removeBallsFromChainFrom(nFromIndex, nToIndex);
	}
}


void GameScene::BallAnimationBackPower(int nFromIndex, int nToIndex, bool bIsRightArrow)
{
	float fMul = 1;
	if (bIsRightArrow == false)
		fMul = -1;

	int ncount = m_pZMBallSprites.size();
	if (nFromIndex > ncount)
		return;
	if (nToIndex >= ncount)
		nToIndex = ncount - 1;

	for (int i = nFromIndex; i <= nToIndex; i++)
	{
		ZMBallSprite* oneBallSprite = m_pZMBallSprites[i];
		Point ptCurrent = oneBallSprite->getPosition();

		if (fMul < 0)
			m_fRemainedDist = -m_nBackPower;
		else
			m_fRemainedDist = m_nBackPower;

		while (fabs(m_fRemainedDist))
		{
			int numBallPosIndex = oneBallSprite->getNextTargetPosIndex();
			ptCurrent = getNextPos(ptCurrent, numBallPosIndex, m_fRemainedDist);

			if (bIsRightArrow && m_parseOperation->getLocationCount() <= numBallPosIndex + 2)
				return;

			Point ptTarget = m_parseOperation->getPosFromIndex(numBallPosIndex + 2);
			if (!bIsRightArrow)
				ptTarget = m_parseOperation->getPosFromIndex(numBallPosIndex);

			if (fabs(m_fRemainedDist))
			{
				int intValue = numBallPosIndex + fMul * 2;
				if (intValue < 0)
					intValue = 0;
				m_pZMBallSprites[i]->setNextTargetPosIndex(intValue);
				m_pZMBallSprites[i]->setLocalZOrder(m_parseOperation->getZorderByLocalPosIndex(intValue));
			}
		}

		oneBallSprite->setPosition(ptCurrent);
	}
}

void GameScene::BallAnimationFrom(int nFromIndex, int nToIndex, bool bIsRightArrow, float foffset)
{
	float fMul = 1;
	if (bIsRightArrow == false)
		fMul = -1;

	if (foffset != 0)
	{
		if (fMul*foffset > 0)
			fMul = 1;
		else
			fMul = -1;
	}
	

	int ncount = m_pZMBallSprites.size();
	if (nFromIndex > ncount)
	{
		log("nFromIndex > ncount");
		return;
	}
		
	if (nToIndex >= ncount)
		nToIndex = ncount - 1;

	if (m_pZMBallSprites.size() == 0)
	{
		m_bIsGameComplete = true;
		return;
	}
	else
	{
		int firstBallIndex = m_pZMBallSprites[0]->getNextTargetPosIndex();
		if (firstBallIndex >= /*m_parseOperation->getLocationCount() / 3*/5)
		{
			m_bFirstSlide = false;
		}
	}

	

	ZMBallSprite* oneBallSprite = m_pZMBallSprites[0];
	for (int i = nFromIndex; i <= nToIndex; i++)
	{
		oneBallSprite = m_pZMBallSprites[i];
		if (oneBallSprite->getDeleteTag())
		{
			log("this ball is aleard delete");
			continue;
		}
		Point ptCurrent = oneBallSprite->getPosition();

		if (foffset != 0)
		{
			m_fRemainedDist = foffset;
		}
		else
		{
			//速度计算公式： （0.5 + （（（当前关卡 +1）/5）*关卡增加比例））；
			m_fRemainedDist = (m_DEF_Ball_Offset +((m_nCurrntStage +1)/5)*m_DEF_SpeedUpRateByLevel);

			if (m_nIsBackPro)
				m_fRemainedDist = -m_fRemainedDist* 2;
			else if (fMul < 0)
				m_fRemainedDist = fMul*m_fRemainedDist * 5;
			else if (m_bFirstSlide)
				m_fRemainedDist = fMul*m_fRemainedDist * 5;
			else if (m_bIsFastSortingChain)
				m_fRemainedDist = fMul*m_fRemainedDist * 5;
			else if (m_isGoPro)
				m_fRemainedDist = fMul*m_fRemainedDist * 1.5;
			else
			{
				m_fRemainedDist = fMul*m_fRemainedDist;
			}
		}


		while (fabs(m_fRemainedDist) >= 0.01f)
		{
			int numBallPosIndex = oneBallSprite->getNextTargetPosIndex();
	
			Point ptOld = ptCurrent;
			ptCurrent = getNextPos(ptCurrent, numBallPosIndex, m_fRemainedDist);

			if (ptCurrent.x == 0 && ptCurrent.y == 0)
			{
				oneBallSprite->setDeleteTag(true);
				m_bIsGameOver = true;
				return;
			}

			if (fabs(m_fRemainedDist) >= 0.01f)
			{
				int intValue = numBallPosIndex + fMul * 2;
				if (intValue < 0)
					intValue = 0;
				oneBallSprite->setNextTargetPosIndex(intValue);
				if (i == 0)
				{
					log("zorderPos_0000==============%d", m_parseOperation->getZorderByLocalPosIndex(intValue));
				}
				else
				{
					//log("zorder%d", m_parseOperation->getZorderByLocalPosIndex(intValue));
				}
				
				oneBallSprite->setLocalZOrder(m_parseOperation->getZorderByLocalPosIndex(intValue));
			}
		}

		if (ptCurrent != Vec2(0.0f,0.0f))
		{
			oneBallSprite->setPosition(ptCurrent);
		}		
	}
}



int GameScene::getLastIndexOfChain(int nFirstIndex)
{
	Rect ballRect = Rect(0,0,BOXRECT,BOXRECT);
	int nCount = m_pZMBallSprites.size();
	int nLastIndex = nFirstIndex;
	int nPrevIndex = nFirstIndex;
	while (nLastIndex < nCount)
	{
		ZMBallSprite* sprBall1 =m_pZMBallSprites[nLastIndex];
		ZMBallSprite* sprBall2 = m_pZMBallSprites[nPrevIndex];
		Point pt1 = sprBall1->getPosition();
		Point pt2 = sprBall2->getPosition();
		float dist = sqrt((pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y));
		if (dist > ballRect.size.width + DEF_DistCalc_Offset * 2)
			break;

		if (nLastIndex != nPrevIndex)
			nPrevIndex++;

		nLastIndex++;
	}
	nLastIndex--;

	return nLastIndex;
}

int GameScene::getFirstIndexOfChain(int nLastIndex)
{
	if (nLastIndex >=  m_pZMBallSprites.size())
	{
		nLastIndex = m_pZMBallSprites.size() - 1;
	}

	Rect ballRect = Rect(0, 0, BOXRECT, BOXRECT);
	int nFirstIndex = nLastIndex;
	int nNextIndex = nLastIndex;
	while (nFirstIndex >= 0)
	{
		ZMBallSprite* sprBall1 = m_pZMBallSprites[nFirstIndex];
		ZMBallSprite* sprBall2 = m_pZMBallSprites[nNextIndex];
		Point pt1 = sprBall1->getPosition();
		Point pt2 = sprBall2->getPosition();
		float dist = sqrt((pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y));
		if (dist > ballRect.size.width + DEF_DistCalc_Offset)
			break;

		if (nFirstIndex != nNextIndex)
			nNextIndex--;

		nFirstIndex--;
	}
	nFirstIndex++;

	return nFirstIndex;
}

void GameScene::checkGameOver()
{
	if (m_pZMBallSprites.size() == 0)
	{
		m_bIsGameComplete = true;
		return;
	}

	if (m_pZMBallSprites.size()>0)
	{
		if (m_pZMBallSprites[0]->getNextTargetPosIndex()+2 >= m_parseOperation->getLocationCount())
		{
			m_bIsGameOver = true;
			return;
		}
	}
	
}


void GameScene::loadBackgroundImg()
{
	Size size = Director::getInstance()->getWinSize();
	m_winSize = size;

	m_ui = dynamic_cast<Layout*>(SP_GUIReader->widgetFromJsonFile(m_parseOperation->getTrackFileNameJson().c_str()));
	m_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ui->setPosition(VisibleRect::center());
	this->addChild(m_ui, 2);

	Sprite *backSprite = Sprite::create(m_parseOperation->getBGName());
	
	backSprite->setPosition(Vec2(size.width / 2, size.height / 2));
	addChild(backSprite, 0);

	for (int i = 0; i < DEF_FiringBall_Count; i++)
	{
		for (int j = 0; j < DEF_Fire_Count; j++)
		{
			string strFileName = "fire" + UGameTool::converInt2String(j + 1) + ".png";
			m_sprFires[i][j] = Sprite::create(strFileName);
			m_sprFires[i][j]->setVisible(false);
			addChild(m_sprFires[i][j], 5);
		}
	}


	menuLayer = MenuLayer::create();
	menuLayer->initLevel(m_parseOperation->getLevel());
	this->addChild(menuLayer,60);
	
}

void GameScene::onPlayPause(EventCustom* event)
{
	Value* _value = (Value*)event->getUserData();
	m_gamePause = _value->asBool();

	if (m_gamePause)
	{
		this->pauseSchedulerAndActions();
	}
	else
	{
		this->resumeSchedulerAndActions();
	}
}

void GameScene::onPlayOn(Ref* sender)
{

}
void GameScene::onEndGame(Ref* sender)
{

}
void GameScene::onMoreGames(Ref* sender)
{

}

void GameScene::onContinue(Ref* sender)
{

}

void GameScene::onNewGames(EventCustom* event)
{
	if (m_pZMBallSprites.size() != 0)
	{
		for (vector<ZMBallSprite*>::iterator it = m_pZMBallSprites.begin(); it != m_pZMBallSprites.end();)
		{
			ZMBallSprite* sp = *(it);
			sp->runAction(RemoveSelf::create(true));
			it = m_pZMBallSprites.erase(it);
		}
	}

	if (m_sprShootBall)
	{
		m_sprShootBall->runAction(RemoveSelf::create(true));
		m_sprShootBall = nullptr;
	}

	initVariables();
	initEngine();


	m_nScore = 0;
	menuLayer->setCurrFenShu(0);
	menuLayer->setNowShowScore(0);
	menuLayer->addSore(0);

	this->schedule(schedule_selector(GameScene::updateBallRuning), 0.01f);
}

void GameScene::onLink(Ref* sender)
{

}

void GameScene::setContinue()
{

}
void GameScene::restartGame()
{
	int level = PlayerManager::instance()->getCurrentStage();
	Size size = Director::getInstance()->getWinSize();

	string strLevelBg = "";
	if (level < 9)
	{
		if (size.width == 568 || size.height == 568)
			strLevelBg = "stage000" + UGameTool::converInt2String(level + 1) + "_4inch.png";
		else
			strLevelBg = "stage000"+UGameTool::converFloatString(level+1)+".png";
	}
	else if (level < 99)
	{
		if (size.width == 568 || size.height == 568)
			strLevelBg = "stage00" + UGameTool::converInt2String(level + 1) + "_4inch.png";
		else
			strLevelBg = "stage00" + UGameTool::converFloatString(level + 1) + ".png";
	}

	unscheduleUpdate();

}

void GameScene::actionShooter()
{
	
}


void GameScene::GameCompleteProc()
{

	for (int i = 0; i < 20; i++)
	{
		ParticleSystemQuad* myTestParticle = ParticleFireworks::create();
		Sprite* sprParticle;
		sprParticle = Sprite::create("particle_star.png");
		myTestParticle->setPosition(Vec2(m_winSize.width / 20 * i + m_winSize.width / 20, m_winSize.height / 10 * 9));
		Color4F startColor, startColorVar, endColor, endColorVar;
		startColor.r = 1.0f;
		startColor.g = 1.0f;
		startColor.b = 1.0f;
		startColor.a = 1.0f;

		startColorVar.r = 0.0f;
		startColorVar.g = 0.0f;
		startColorVar.b = 0.0f;
		startColorVar.a = 0.0f;

		endColor.r = 1.0f;
		endColor.g = 1.0f;
		endColor.b = 1.0f;
		endColor.a = 1.0f;

		endColorVar.r = 0.0f;
		endColorVar.g = 0.0f;
		endColorVar.b = 0.0f;
		endColorVar.a = 0.0f;

		myTestParticle->setStartColor(startColor);
		myTestParticle->setStartColorVar(startColorVar);
		myTestParticle->setEndColor(endColor);
		myTestParticle->setEndColorVar(endColorVar);

		myTestParticle->setDuration(0.2);
	    myTestParticle->setLife(1.0f);
		myTestParticle->setLifeVar(1.0f);

		myTestParticle->setSpeed(80.f);
		myTestParticle->setSpeedVar(20.f);
		myTestParticle->setTexture(sprParticle->getTexture());
		myTestParticle->setAutoRemoveOnFinish(true);
		addChild(myTestParticle,100);
	}

	m_bOverLayerExist = true;
	PlayerManager::instance()->setLevelScore(m_nCurrntStage, m_nScore);
	if (m_nCurrntStage < LEVEL_COUNT - 1)
	{
		PlayerManager::instance()->setLevelIsUnlockByLevel(m_nCurrntStage + 1);
	}
	
	ValueMap value;
	value["score"] = m_nScore;
	value["time"] = "08::00";
	value["win"] = true;

	if (m_sprShootBall)
	{
		m_sprShootBall->runAction(RemoveSelf::create(true));
		m_sprShootBall = nullptr;
	}

	this->unschedule(schedule_selector(GameScene::updateBallRuning));
	GameOverLayer* gamover = GameOverLayer::create();
	gamover->addGameInf(value);
	this->addChild(gamover, 100);
}

void GameScene::rotateShooter(Point touchPos)
{
	Point ptShooter = m_sprShooter->getPosition();
	Rect rectShooter = m_sprShooter->getBoundingBox();
	Point ptTemp = ptShooter; ptTemp.x = ptTemp.x + rectShooter.size.width;

	float angle = getAngleBetweenTowLines(ptShooter,touchPos,ptShooter,ptTemp);
	angle = angle * 180 / PI; 
	angle = -angle;

	m_sprShooter->runAction(Sequence::create(RotateTo::create(0.1f, angle), CallFunc::create([=](){
		ImageView* shotball0 = SEEK_WIDGET_BY_NAME(ImageView, "shootball_0", m_ui);
		Point pt = shotball0->getParent()->convertToWorldSpace(shotball0->getPosition());
		m_parseOperation->setShooterPos(pt);
		if (!m_sprShooter) return;
		m_sprShootBall->setPosition(pt);
	}),nullptr));
}


void GameScene::setFlyingBall(Point location)
{
	m_sprFlyingBall = m_sprShootBall;
	m_nFlyingBallColor = m_sprFlyingBall->getBallType();
	m_bHasFlyingBall = true;
	addShooteBall();

	//set flying ball speed 
	////m_FlyingSpeed.width
	////m_FlyingSpeed.height
	Point ptBall = m_sprFlyingBall->getPosition();
	float nLargeLine = sqrt((ptBall.x - location.x)*(ptBall.x - location.x) + (ptBall.y - location.y)*(ptBall.y - location.y));
	float nDifX = location.x - ptBall.x;
	float nDifY = location.y - ptBall.y;
	float rX, rY;
	rX = (DEF_Ball_FlyingSpeed*fabs(nDifX)) / nLargeLine;
	rY = (DEF_Ball_FlyingSpeed*fabs(nDifY)) / nLargeLine;
	m_FlyingSpeed.width = nDifX > 0? rX:-rX;
	m_FlyingSpeed.height = nDifY > 0? rY : -rY;

}

bool GameScene::onTouchBegan(Touch *touch, Event *unused_event)
{
	if (m_gamePause || m_bIsGameOver || m_bIsGameDone || m_bHasFlyingBall)
	{
		return false;
	}


	Point location = touch->getLocationInView();
	location = Director::getInstance()->convertToGL(location);
	rotateShooter(location);

	return true;
}

void GameScene::onTouchMoved(Touch *touch, Event *unused_event)
{
	Point location = touch->getLocationInView();
	location = Director::getInstance()->convertToGL(location);
	rotateShooter(location);

}

void GameScene::onTouchEnded(Touch *touch, Event *unused_event)
{
	Point location = touch->getLocationInView();
	location = Director::getInstance()->convertToGL(location);

	if (!m_nIsBackPro && !m_bHasFlyingBall && !m_bIsFastSortingChain && checkBackChainToPowerBall() == -1 && !m_bFirstSlide)
	{
		log("GameScene::onTouchEnded(flying)");
		makeShootBall();
		m_sprShootBall->stopAllActions();
		setFlyingBall(location);
	}
}

void GameScene::updateBallRuning(float delta)
{
	if (m_bIsGameDone || m_gamePause)
		return;

	m_gameLastTime = m_gameLastTime + delta;
	m_ccUpdateBallTime = m_ccUpdateBallTime + delta;
	m_ccUpdateFlyingTime = m_ccUpdateFlyingTime + delta;
	m_ccUpdateFastSortTime = m_ccUpdateFastSortTime + delta;
	m_ccUpdateBombShowTime = m_ccUpdateBombShowTime + delta;	
	int time = m_gameLastTime * 100;
	
	
	if (m_bIsGameOver || m_bIsGameComplete)
	{
		procGameFinish();
		return;
	}

	
	checkGameOver();
	updatePowerIndex(true);

	deleteBallsByDeleteTag();

	if (m_ccUpdateBallTime > DEF_BallAni_Interval)
	{
		procBallAnimation();
		m_ccUpdateBallTime = 0;
	}

	if (m_ccUpdateFastSortTime > DEF_FastSortChain_Interval)
	{
		procFastSort();
		m_ccUpdateFastSortTime = 0;
	}

	if (m_ccUpdateFlyingTime > DEF_Ball_FlyingInterval)
	{
		if (m_bHasFlyingBall)
		{
			BallFlyingAnimation(DEF_Ball_FlyingInterval);
			checkAddingFlyingBallToChain();
		}

		m_ccUpdateFlyingTime = 0;
	}

	//stop pro
	if (m_ccUpdateBombStopDelay <= 0)
	{
		if (m_nIsStop == true)
		{
			m_ccUpdateBombStopDelay = 0;
			m_nIsStop = false;
		}
	}
	else
	{
		m_ccUpdateBombStopDelay -= delta;
	}

	//back pro
	if (m_ccUpdateBombBackDelay <= 0)
	{
		if (m_nIsBackPro == true)
		{
			m_ccUpdateBombBackDelay = 0;
			m_nIsBackPro = false;
		}
	}
	else
	{
		m_ccUpdateBombBackDelay -= delta;
	}


	//addScore
	if (m_ccUpdateScoreUpTime <= 0)
	{
		if (m_scorePro == true)
		{
			m_ccUpdateScoreUpTime = 0;
			m_scorePro = false;
		}
	}
	else
	{
		m_ccUpdateScoreUpTime -= delta;
	}


	//gohead pro
	if (m_ccGoheadlastTime <= 0)
	{
		if (m_isGoPro == true)
		{
			m_ccGoheadlastTime = 0;
			m_isGoPro = false;
		}
	}
	else
	{
		m_ccGoheadlastTime -= delta;
	}

	
	if (m_ccUpdateBombShowTime > DEF_BombShow_Interval)
	{
		//BombProc();
		m_ccUpdateBombShowTime = 0;
	}
}
