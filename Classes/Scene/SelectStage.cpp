#include "SelectStage.h"
#include "MainMenu.h"
#include "PlayerManager.h"
#include "GameScene.h"
#include "publicFunc.h"
#include "RDTableView.h"
#include "VisibleRect.h"
#include "MapEditeScene.h"
#include "RDSceneManager.h"
#include "ResoureManager.h"


#define STAGE_NUM   30
#define PAGESELLS   10
#define MARGINX_RATE    0.1f
#define MARGINY_RATE    0.12f
#define ROW_NUM         2
#define COL_NUM         5

#define STAGE_MENU_RATEX        0.75f

using namespace std;
SelectStage::SelectStage()
{
	m_nPage = 1;
}

SelectStage::~SelectStage()
{

}

Scene* SelectStage::createScene(int stage)
{
	Scene* scene = Scene::create();
	SelectStage* layer = SelectStage::create(stage);
	scene->addChild(layer);
	return scene;
}

SelectStage* SelectStage::create(int stage)
{
	SelectStage* layer = new SelectStage;
	if (layer&&layer->init(stage))
	{
		layer->autorelease();
		return layer;
	}
	CC_SAFE_DELETE(layer);
	return NULL;
}

bool SelectStage::init(int stage)
{
	if (!Layer::init()) return false;

	m_ui = dynamic_cast<Layout*>(SP_GUIReader->widgetFromJsonFile("fight/ui/select_stage.ExportJson"));
	m_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ui->setPosition(VisibleRect::center());
	this->addChild(m_ui, 2);


	m_nPage = stage % PAGESELLS != 0 ? (stage / PAGESELLS + 1) : stage / PAGESELLS;


	//
	Button* button = SEEK_WIDGET_BY_NAME(Button, "Button_return", m_ui);
	button->setPositionX(VisibleRect::right().x - button->getContentSize().width*2.f);
	button->addTouchEventListener(CC_CALLBACK_2(SelectStage::buttonOver, this));

	button = SEEK_WIDGET_BY_NAME(Button, "Button_left", m_ui);
	button->setLocalZOrder(10);
	button->setPositionX(VisibleRect::left().x + button->getContentSize().width*0.6f);
	button->addTouchEventListener(CC_CALLBACK_2(SelectStage::buttonOver, this));

	button = SEEK_WIDGET_BY_NAME(Button, "Button_right", m_ui);
	button->setLocalZOrder(10);
	button->setPositionX(VisibleRect::right().x - button->getContentSize().width*0.6f);
	button->addTouchEventListener(CC_CALLBACK_2(SelectStage::buttonOver, this));

	int totalStage = PlayerManager::instance()->getTotalStage();
	m_tableCellNums = totalStage % PAGESELLS != 0 ? (totalStage / PAGESELLS + 1) : totalStage / PAGESELLS;

	m_pTablview = TableView::create(this, Size(450, 200));
	m_pTablview->setDirection(extension::ScrollView::Direction::HORIZONTAL);
	m_pTablview->setPosition(VisibleRect::leftBottom() + Vec2(VisibleRect::getVisibleRect().size.width*0.1f, VisibleRect::getVisibleRect().size.height*0.25f));
	m_pTablview->setDelegate(this);
	m_pTablview->setTouchEnabled(false);
	m_ui->addChild(m_pTablview,2);
	m_pTablview->reloadData();
	
	/*int j = 0;
	float x = VisibleRect::center().x;

	for (int i = 1; i <= m_tableCellNums; i++)
	{
	string str = "dot_" + UGameTool::converInt2String(i);
	ImageView* dot = SEEK_WIDGET_BY_NAME(ImageView, str, m_ui);
	if (i == m_nPage)
	{
	((ui::Scale9Sprite*)dot->getVirtualRenderer())->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("x_stagedot1.png"));
	}
	else
	{
	((ui::Scale9Sprite*)dot->getVirtualRenderer())->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("x_stagedot2.png"));
	}


	}*/

	TrunPage(m_nPage,0.0f);
	return true;
}  

LabelTTF* SelectStage::initLevelScore(int nIndex)
{
	Size screenSize = Director::getInstance()->getWinSize();
	float fFontSize = screenSize.height / 320.0 * 15;

	string strScore = UGameTool::converInt2String(PlayerManager::instance()->getLevelScore(nIndex));
	LabelTTF* levelscore = LabelTTF::create(strScore ,"Arial", fFontSize);
	levelscore->setColor(ccc3(255, 255, 255));
	
	return levelscore;
}

void SelectStage::selectUnlockAllLevels()
{

}
void SelectStage::onRemoveAds()
{

}
void SelectStage::onGetFreeSkip()
{
}
void SelectStage::onBack()
{
	
	Scene *scene = MainMenu::createScene();
	TransitionScene *ts = TransitionFade::create(1.0f, scene, Color3B::BLACK);
	Director::getInstance()->replaceScene(ts);
}
void SelectStage::onMoreGames()
{

}
void SelectStage::onNewGames()
{

}

void SelectStage::onLink()
{

}

void SelectStage::TurnPageLeft()
{
	if (m_nPage >= m_tableCellNums) return;
	m_nPage++;

	TrunPage(m_nPage,0.8f);
}

void SelectStage::TurnPageRight()
{
	if (m_nPage <= 1) return;
	m_nPage--;

	TrunPage(m_nPage,0.8f);
}

void SelectStage::onSelectStage(Ref* sender)
{
	int nTag = ((Node*)sender)->getTag();

	int chapter = nTag/CHAPTERSTAGEMAX+1;
	int level = nTag % CHAPTERSTAGEMAX;

	if (nTag%CHAPTERSTAGEMAX == 0)
	{
		chapter = nTag/ CHAPTERSTAGEMAX;
		level = CHAPTERSTAGEMAX;
	}


	log("selected level number %d", nTag);
	PlayerManager::instance()->setCurrentStage(level);
	PlayerManager::instance()->setSelectedCharacter(chapter);


	if (nTag <= CURRENTMAXSTAGE)
	{
		CCScene *scene = GameScene::createScene();
		CCTransitionScene *ts = CCTransitionFade::create(1.0f, scene, Color3B::BLACK);
		Director::getInstance()->replaceScene(ts);
	}
	else
	{
		CCScene *scene = MapEditeScene::createScene();
		CCTransitionScene *ts = CCTransitionFade::create(1.0f, scene, Color3B::BLACK);
		Director::getInstance()->replaceScene(ts);
	}
	
}

void SelectStage::TrunPage(int page,float time)
{
	if (m_nPage == 1)
	{
		Button* button = SEEK_WIDGET_BY_NAME(Button, "Button_left", m_ui);
		button->setBright(true);
		button->setHighlighted(true);
		button = SEEK_WIDGET_BY_NAME(Button, "Button_right", m_ui);
		button->setBright(false);
	}
	else if (m_nPage == m_tableCellNums)
	{
		Button* button = SEEK_WIDGET_BY_NAME(Button, "Button_left", m_ui);
		button->setBright(false);
		button = SEEK_WIDGET_BY_NAME(Button, "Button_right", m_ui);
		button->setBright(true);
	}
	else
	{
		Button* button = SEEK_WIDGET_BY_NAME(Button, "Button_left", m_ui);
		button->setBright(true);
		button = SEEK_WIDGET_BY_NAME(Button, "Button_right", m_ui);
		button->setBright(true);
	}
	m_pTablview->setContentOffsetInDuration(Vec2(-450 * (m_nPage - 1), 0), time);
}

cocos2d::Size SelectStage::tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
	return Size(450, 200);
}

cocos2d::extension::TableViewCell* SelectStage::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
	TableViewCell *cell = table->dequeueCell();
	if (!cell) {
		cell = new TableViewCell();
		cell->autorelease();
		freshCell(table, cell, idx);
	}
	else
	{
		cell->removeAllChildren();
		freshCell(table, cell, idx);
	}


	return cell;

}

ssize_t SelectStage::numberOfCellsInTableView(cocos2d::extension::TableView *table)
{

	return m_tableCellNums;
}


void SelectStage::freshCell(TableView *table, TableViewCell* cell, int index)
{
	Size size = tableCellSizeForIndex(table, index);
	int nums = index*PAGESELLS;
	
	int stage = 0+nums;
	for (int i = 0; i < ROW_NUM; i++)
	{
		float y = size.height - size.height*0.25f*(i*2+1);
		for (int j = 0; j < COL_NUM; j++)
		{
			stage++;
			if (stage > STAGE_NUM) return;

			ValueMap map = ResoureManager::instance()->getStageDataByStage(stage);
			bool isEidit = map["isEidit"].asBool();

			float x = size.width * 0.1 * (j*2+1);

			bool isUnlock = PlayerManager::instance()->getLevelisUnlock(stage);
			string str ="";
			if (isUnlock || isEidit)
			{
				str = "x_stageunlockback.png";
			}
			else
			{
				str = "x_stagelockback.png";
			}
			ImageView* sp = ImageView::create(str, Widget::TextureResType::PLIST);
			sp->addTouchEventListener(CC_CALLBACK_2(SelectStage::touchStage,this));
			sp->setTag(stage);
			sp->setAnchorPoint(Vec2(0.5f, 0.5f));
			sp->setPosition(Vec2(x, y));
			cell->addChild(sp);

			
			LabelTTF* level = LabelTTF::create(UGameTool::converInt2String(stage), "Airial", 45);
			level->setAnchorPoint(Vec2(0.5, 0.5));
			level->setPosition(Vec2(sp->getContentSize().width*0.5, sp->getContentSize().height*0.6f));
			level->setColor(ccc3(255, 255, 0));
			sp->addChild(level);


			if (!isUnlock && !isEidit)
			{
				sp->setTouchEnabled(false);
				Sprite* lock = Sprite::createWithSpriteFrameName("x_stageLock.png");
				lock->setPosition(Vec2(sp->getContentSize().width*0.1f, sp->getContentSize().height*0.85f));
				sp->addChild(lock);
			}
			else
			{
				sp->setTouchEnabled(true);
				LabelTTF* levelscore = initLevelScore(stage);
				levelscore->setAnchorPoint(Vec2(0.5, 0.5));
				levelscore->setPosition(Vec2(sp->getContentSize().width*0.5f, sp->getContentSize().height*0.2f));
				levelscore->setColor(ccc3(255, 255, 255));
				sp->addChild(levelscore);
			}

		}
	}


}

void SelectStage::buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type)
{
	Node* node = (Node*)sender;
	if (type != Widget::TouchEventType::ENDED) return;

	if (strcmp("Button_return", node->getName().c_str()) == 0)
	{
		RDSceneManager::instance()->RunTargetScene(EN_SCENE_MAINMENU);
	}
	else if (strcmp("Button_left", node->getName().c_str()) == 0)
	{
		TurnPageLeft();
	}
	else if (strcmp("Button_right", node->getName().c_str()) == 0)
	{
		TurnPageRight();
	}
}

void SelectStage::touchStage(cocos2d::Ref *sender, Widget::TouchEventType type)
{
	if (Widget::TouchEventType::ENDED != type) return;
	ImageView* image = (ImageView*)sender;

	int nTag = image->getTag();
	int chapter = nTag / CHAPTERSTAGEMAX + 1;
	int level = nTag % CHAPTERSTAGEMAX;

	if (nTag%CHAPTERSTAGEMAX == 0)
	{
		chapter = nTag / CHAPTERSTAGEMAX;
		level = CHAPTERSTAGEMAX;
	}

	ValueMap map = ResoureManager::instance()->getStageDataByStage(nTag);
	bool isEidit = map["isEidit"].asBool();


	log("selected level number %d", nTag);
	PlayerManager::instance()->setCurrentStage(level);
	PlayerManager::instance()->setSelectedCharacter(chapter);
	PlayerManager::instance()->setCurrentTotalStage(nTag);


	if (!isEidit)
	{
		RDSceneManager::instance()->RunTargetScene(EN_SCENE_FIGHT);
	}
	else
	{
		RDSceneManager::instance()->RunTargetScene(EN_SCENE_MAPEDIT);
	}
}