//
//  PlayerData.h
//  UGameMarMonst
//
//  Created by linminglu on 14-6-3.
//
//

#ifndef __ZHUMAFIGHT__RDCSVDataManageData__
#define __ZHUMAFIGHT__RDCSVDataManageData__

#include <iostream>
#include "cocos2d.h"
#include "CSingleton.h"
#include "Header.h"

using namespace std;
USING_NS_CC;
class RDCSVDataManage : public Singleton < RDCSVDataManage >
{
public:

	~RDCSVDataManage();

	ValueMap getValuMapByCSV(string fileName);
private:
	CC_SYNTHESIZE(int, m_nCurrentStage, CurrentStage);
	CC_SYNTHESIZE(int, m_nSelectedCharacter, SelectedCharacter);
	CC_SYNTHESIZE(int, m_nCurrentTotalStage, CurrentTotalStage);
private:
	RDCSVDataManage();
	SINGLETON_FRIENDS;
};

#endif /*__ZHUMAFIGHT__RDCSVDataManageData__*/
