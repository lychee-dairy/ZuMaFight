#include "RDCSVDataManage.h"
#include "RDCSVRead.h"
#include "publicFunc.h"

RDCSVDataManage::RDCSVDataManage()
{


}

RDCSVDataManage::~RDCSVDataManage()
{

}

ValueMap RDCSVDataManage::getValuMapByCSV(string fileName)
{
	ValueMap _vector;

	RDCSVRead *csvFile = new RDCSVRead();
	csvFile->openFile(fileName.c_str());
	log("cols====%d", csvFile->getCols());
	log("rows====%d", csvFile->getRows());
	if (csvFile->getRows() <= 1)
	{
		return _vector;
	}

	
	for (int i = 1; i < csvFile->getRows(); i++) {
		
		ValueMap _valueCols;
		_valueCols.clear();
		for (int j = 1; j < csvFile->getCols(); j++) {
			string keys = csvFile->getData(0, j);
			string content = csvFile->getData(i, j);
			keys = UGameTool::cleanCharInString(keys, '\r');
			_valueCols[keys] = UGameTool::cleanCharInString(content, '\r');
			log("keys[%s]==%s", keys.c_str(), _valueCols[keys].asString().c_str());
		}
		string keys = csvFile->getData(i, 0);
		_vector[keys] = _valueCols;
	}

	if (csvFile) delete(csvFile);

	return _vector;
}