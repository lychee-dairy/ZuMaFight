﻿
#include"AnimotePack.h"
#include "Header.h"

AnimotePack::AnimotePack()
{

}

AnimotePack::~AnimotePack()
{
	AnimationCache::destroyInstance();
	SpriteFrameCache::getInstance()->removeUnusedSpriteFrames();
	Director::getInstance()->getTextureCache()->removeUnusedTextures();
}

AnimotePack* AnimotePack::instance = NULL ;
AnimotePack* AnimotePack::getInstance()
{
	if (instance == NULL)
	{
		instance = new AnimotePack();
	}
	return instance ;
}

Animate* AnimotePack::AddAnimate(char* picName, float delay, int frames, char* animotionName, TextureResType _type /* = TextureResType::LOCAL */)
{
	//加载帧的数组
	Vector<SpriteFrame*> plistArrary ;
	for(int i = 0 ; i < frames ; i++)
	{
		char c_str1[8];
		std::string picStr(picName);
		sprintf(c_str1,"%d",i+1);
		picStr += c_str1;
		picStr += ".png";
		
		SpriteFrame* frame = nullptr;
		if (_type == TextureResType::PLIST)
		{
			frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(picStr.c_str());
		}
		else
		{
			
			Sprite* sp = Sprite::create(picStr);
			frame = SpriteFrame::createWithTexture(sp->getTexture(),sp->getTextureRect());
		}
		
		plistArrary.pushBack(frame);
	}

	Animation* plistAnimation = Animation::createWithSpriteFrames(plistArrary, delay);
	AnimationCache::getInstance()->addAnimation(plistAnimation, animotionName);
	Animate* plistAnimate = Animate::create(AnimationCache::getInstance()->getAnimation(animotionName));

	return plistAnimate;
}

Animate* AnimotePack::getAnimateByName(char* animotionName)
{
	Animate* plistAnimate = Animate::create(AnimationCache::getInstance()->getAnimation(animotionName));
	return plistAnimate;
}

void AnimotePack::addPlist(string plistName)
{
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plistName);
}

void AnimotePack::showShake(Node *node, CallFunc* fun /* = nullptr */)
{
	MoveBy* by1 = MoveBy::create(0.1f, Vec2(0,10));
	MoveBy* by2 = MoveBy::create(0.1f, Vec2(-10, 0));
	Sequence* up = Sequence::create(by1, by1->clone()->reverse(), by1->clone()->reverse(), by1->clone(), nullptr);
	Sequence* lr = Sequence::create(by2, by2->clone()->reverse(), by2->clone()->reverse(), by2->clone(), nullptr);
	Spawn* spa = Spawn::create(up, lr, nullptr);
	node->runAction(Sequence::create(spa,fun, nullptr));
}

void AnimotePack::showEaseScallOut(Node *node, CallFunc* fun )
{
	node->setScale(1.0f);
	node->runAction(Sequence::create(EaseBackInOut::create(ScaleTo::create(0.4f, 0.0f)), fun, nullptr));
}

void AnimotePack::showEaseScallBack(Node *node, CallFunc* fun /* = nullptr */)
{
	node->setScale(0.0f);
	node->runAction(Sequence::create(EaseBackOut::create(ScaleTo::create(0.4f, 1.0f)), fun, nullptr));
}

void AnimotePack::showScallBack(Node* node, CallFunc* fun /* = nullptr */)
{
	node->setScale(0.0f);
	node->runAction(Sequence::create(ScaleTo::create(0.2f, 1.0f), fun, nullptr));
}

void AnimotePack::showScallOut(Node* node, CallFunc* fun /* = nullptr */)
{
	node->setScale(1.0f);
	node->runAction(Sequence::create(ScaleTo::create(0.2f, 0.0f) , fun , nullptr));
}

void AnimotePack::showEaseScallOutAndFadeIn(Node* node, CallFunc* fun)
{
	if (node == nullptr){
		return;
	}
	node->setOpacity(0);
	node->setScale(0.3f);
	node->runAction(Sequence::create(DelayTime::create(0.2f),Spawn::create(FadeIn::create(0.4f),
		EaseBackOut::create(ScaleTo::create(0.4f, 1.0f)),nullptr), fun, nullptr));
}
void AnimotePack::showEaseScallOutAndFadeOut(Node* node, CallFunc* fun )
{
	node->runAction(Sequence::create(Spawn::create(FadeOut::create(0.1f),
		EaseBackIn::create(ScaleTo::create(0.5f, 0.01f)), nullptr), fun, nullptr));
}
void AnimotePack::setUiButtonSpriteNormal(Button *pButton)
{
}


