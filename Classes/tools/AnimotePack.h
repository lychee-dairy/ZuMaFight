﻿
/************************************************************************/
/* 
  @文件名：          AnimotePack.h
  @Copyright(c):    江西滕王科技有限公司技术开发部
  @作者：            熊忠华
  @日期：            2013-9-4
  @描述：            利用 TexturePackerGUI导出的Plist文件 来制作一些简单匀速的帧动画 的一个单例
  @版本：            1.0
*/
/************************************************************************/

#ifndef _AnimotePacker_H
#define _AnimotePacker_H

/**********头文件定义************/
#include "cocos2d.h"
#include "Header.h"
USING_NS_CC ;
using namespace std ;
/*********类定义***************/
using namespace cocos2d;
using namespace ui;

class AnimotePack : public Ref
{
public:
	AnimotePack();
	~AnimotePack();

/************************************************************************/
/* 
   @作者：   熊忠华
   @功能：   获得一个这个单例的唯一实例
   @参数：   无
   @返回值：  AnimotePack* 
*/
/************************************************************************/
    static AnimotePack* getInstance();
	
/************************************************************************/
/* 
   @作者：   熊忠华
   @功能：   返回一个相应plist文件下的动画CCAnimate的对象
   @参数：   
		 picName   : 该动画的帧图片的名字相同部分 比如 a1_1.png a1_2.png 的共同部分是 a1_
         delay     : 就是每两帧之间的间隔时间 
		 frames    : 帧数
		 animotionName :帧动画名
   @返回值：  CCAnimote* x'zh
*/
/************************************************************************/
	Animate* AddAnimate(char* picName, float delay, int frames, char* animotionName, TextureResType _type = TextureResType::LOCAL);
	Animate* getAnimateByName(char* animotionName);

	void addPlist(string plistName);


	void showScallOut(Node* node, CallFunc* fun = nullptr);
	void showScallBack(Node* node, CallFunc* fun = nullptr);
	void showEaseScallOut(Node *node, CallFunc* fun = nullptr);
	void showEaseScallBack(Node *node, CallFunc* fun = nullptr);
	//shake
	void showShake(Node *node, CallFunc* fun = nullptr);
	
	//layer  fade in and fade out;
	void showEaseScallOutAndFadeIn(Node* node, CallFunc* fun = nullptr);
	void showEaseScallOutAndFadeOut(Node* node, CallFunc* fun = nullptr);
	void setUiButtonSpriteNormal(Button *pButton);
private:
	static AnimotePack* instance ; //单例

    std::vector<char*> plists ;

};
#endif /*_AnimotePacker_H*/