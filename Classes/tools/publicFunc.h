﻿//
//  publicFunc.h
//  sanxiao
//
//  Created by linminglu on 14-4-9.
//
//

#ifndef __sanxiao__publicFunc__
#define __sanxiao__publicFunc__

#include <iostream>
#include <random>
#include <string>
#include <vector>
#include <sstream>
#include <map>
#include <unordered_map>
using namespace std;
namespace UGameTool {
     int randint(int smalldata, int big);      //rand某个范围内的数字
     void shuffle(unsigned char *arr, int len); //随机排序数组
    string converInt2String(int data);         //数字转字符串
	string converFloatString(const float f, const int prec = 2);
    float round(float doValue, int nPrecision);  //精确到某一位
    bool string2int(const char*d,int&r);
    bool isHavedata(const int *arraydata,size_t size,int value);
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
    std::vector<std::string> split(const std::string &s, char delim);
	std::vector<int > splitToIntArray(std::string &s, char delim );
	std::string cleanCharInString(std::string str , char delim);

    long long getCurrentTime();
	bool checkIsRightEmailAddress(std::string email_address);
	void  ensure_dir_exist(std::string path);
	bool isHave(string filePath);

    string getWordWithFile(const string &file ,const char *str);
    string getWordByWordKey(const char *str);
	std::string ReplaceAllSubstring(const std::string &ori_str, const std::string &sub_str, const std::string &new_sub_str);
	string getTimeBySecond(int ft);

    template<typename  T>
    void clearVecor(vector<T *> &vcVector)
    {
        for (typename vector<T *>::iterator it=vcVector.begin() ; it!=vcVector.end(); it++) {
            T *tempValue=*it;
			if (tempValue)
			{
				delete tempValue;
				tempValue = nullptr;
			}
          
        }
        vcVector.erase(vcVector.begin(),vcVector.end());
    }
    
    
    template<typename KeyType,typename  T>
    void clearMap(std::unordered_map<KeyType, T *> &mapValue)
    {
        for (typename std::unordered_map<KeyType, T *>::iterator it=mapValue.begin() ; it!=mapValue.end(); it++) {
            T *tempValue=it->second;
            delete tempValue;
            tempValue=nullptr;
        }
        mapValue.erase(mapValue.begin(),mapValue.end());
    }
    template<class T>
    void covertStringSplitToTArray(T*p ,int size,string str,char delim)
    {
        std::stringstream ss(str);
        std::string item;
        int temp=0;
        while (std::getline(ss, item, delim)) {
            stringstream sstemp;
            T a;
            sstemp << item;
            sstemp >> a;
            if (temp<size) {
                p[temp]=a;
				temp++;
            }
            else{
                printf("data error \n");
                break;
            }
        }
    }
}

#endif /* defined(__sanxiao__publicFunc__) */
