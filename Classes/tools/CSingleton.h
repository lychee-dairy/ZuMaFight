﻿//
//  CSingleton.h
//  en.cube
//
//  Created by linminglu on 13-5-23.
//
//

#ifndef __en_cube__CSingleton__
#define __en_cube__CSingleton__

#include <memory>
#include <iostream>
#include "cocos2d.h"
using namespace std;

#include <functional>
#include <stack>
#include <memory>
#include <assert.h>
#if !defined(_MSC_VER) || _MSC_VER >= 1700
#include <mutex>
#endif

class AtExit
{
public:
    typedef std::function<void ()>  ExitCallback;
    AtExit()
    {
        assert(!*instance());
        if(!*instance())
            *instance() = this;
    }
    
    ~AtExit()
    {
        printf("delete ");
        *instance() = nullptr;
        while(!callbacks_.empty())
        {
            (callbacks_.top())();
            callbacks_.pop();
        }
    }
    
    static void Add(ExitCallback exit_callback)
    {
        //assert(*instance());
        if(*instance())
            (*instance())->callbacks_.push(exit_callback);
    }
    
private:
    std::stack<ExitCallback>    callbacks_;
    
    static AtExit** instance()
    {
        static AtExit* the_instance = nullptr;
        return &the_instance;
    }
};

template <typename Type, bool release_at_exit = true>
class Singleton : public cocos2d::Ref
{
public:
    friend class std::unique_ptr<Type>;
    static Type* instance()
    {
        if (instance_.get() == nullptr)
        {
            std::lock_guard<std::mutex> lock(singleton_mutex_);
            if (instance_.get() == nullptr)
            {
                instance_.reset(new Type());
                if(release_at_exit)
                    AtExit::Add(Singleton<Type,release_at_exit>::releaseSelf);
            }
        }
        return instance_.get();
    }
    
    static void releaseSelf()
    {
        std::lock_guard<std::mutex> lock(singleton_mutex_);
        if(instance_.get())
            instance_.reset();
    }
    
    static bool is_null()
    {
        return instance_.get() == nullptr;
    }
    
protected:
    typedef class Singleton<Type>       BaseSingletonType;
    typedef std::default_delete<Type>   DefaultDeleteType;
    
    Singleton() {}
	virtual ~Singleton() {
		std::string sDeleteName = "delete";
		printf("%s ", sDeleteName.c_str());
	}
    
private:
    static std::mutex                   singleton_mutex_;
    static std::unique_ptr<Type>        instance_;
    
    Singleton( const Singleton& );
    const Singleton& operator=( const Singleton& );
};

template <typename Type, bool release_at_exit>
std::unique_ptr<Type> Singleton<Type,release_at_exit>::instance_;

template <typename Type, bool release_at_exit>
std::mutex Singleton<Type,release_at_exit>::singleton_mutex_;

#define SINGLETON_FRIENDS       \
friend BaseSingletonType;   \
friend DefaultDeleteType;

#endif /* defined(__en_cube__CSingleton__) */
