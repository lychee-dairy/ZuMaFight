#include "RDTableView.h"

RDTableView::RDTableView()
{
	m_nBgString.clear();
	m_nIsInitOK = false;
	m_event = nullptr;
}

RDTableView::~RDTableView()
{
	m_nBgString.clear();
}

RDTableView* RDTableView::create()
{
	return RDTableView::create(nullptr, Size::ZERO);
}

RDTableView* RDTableView::create(TableViewDataSource* dataSource, Size size)
{
	return RDTableView::create(dataSource, size, nullptr);
}

RDTableView* RDTableView::create(TableViewDataSource* dataSource, Size size, Node *container)
{
	RDTableView *table = new RDTableView();
	table->initWithViewSize(size, container);
	table->autorelease();
	table->setDataSource(dataSource);
	
	return table;
}



/////
void RDTableView::moveTableCellByCellIndex(int cellIndex)
{
	int maxNums = _dataSource->numberOfCellsInTableView(this);
	if (cellIndex < 0) cellIndex = 0;
	if (cellIndex > maxNums - 1) cellIndex = maxNums - 1;

	float distance = 0;
	for (int i = 0; i < cellIndex; i++)
	{
		distance  +=  _dataSource->tableCellSizeForIndex(this,i).height;
	}

	setContentOffset(Vec2(0, -distance), false);
	updateCellPostion();

}

void RDTableView::moveTableCellByPercent(float percent)
{
	int maxNums = _dataSource->numberOfCellsInTableView(this);
	
	float distance = getContainSize().height*percent;

	setContentOffset(Vec2(0,-distance), false);
	updateCellPostion();

}

void RDTableView::setScollingControlCallback(ScorllingEvent event)
{
	m_event = event;
}

void RDTableView::initMoveCellToByIndex(int index)
{
	Point pos = _container->getPosition();
	moveTableCellByCellIndex(index);
	m_nIsInitOK = true;
	scrollViewDidScroll(this);
}

void RDTableView::initMoveCellToByPercent(float percent)
{
	moveTableCellByPercent(percent);
	m_nIsInitOK = true;
	scrollViewDidScroll(this);
}

bool RDTableView::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	TableView::onTouchBegan(pTouch, pEvent);
	if (m_event)
	{
		m_event(true);
	}
	
	return true;
}
void RDTableView::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	TableView::onTouchMoved(pTouch, pEvent);
}

void RDTableView::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	TableView::onTouchEnded(pTouch, pEvent);
	if (m_event)
	{
		m_event(false);
	}
}

void RDTableView::onTouchCancelled(Touch *pTouch, Event *pEvent)
{
	TableView::onTouchCancelled(pTouch, pEvent);
}

void RDTableView::scrollViewDidScroll(ScrollView* view)
{
	long countOfItems = _dataSource->numberOfCellsInTableView(this);
	if (0 == countOfItems)
	{
		return;
	}

	if (_isUsedCellsDirty)
	{
		_isUsedCellsDirty = false;
		std::sort(_cellsUsed.begin(), _cellsUsed.end(), [](TableViewCell *a, TableViewCell *b) -> bool{
			return a->getIdx() < b->getIdx();
		});
	}

	if (_tableViewDelegate != nullptr) {
		_tableViewDelegate->scrollViewDidScroll(this);
	}

	ssize_t startIdx = 0, endIdx = 0, idx = 0, maxIdx = 0;
	Vec2 offset = this->getContentOffset() * -1;
	maxIdx = MAX(countOfItems - 1, 0);

	if (_vordering == VerticalFillOrder::TOP_DOWN)
	{
		offset.y = offset.y + _viewSize.height / this->getContainer()->getScaleY();
	}
	startIdx = this->_indexFromOffset(offset);
	if (startIdx == CC_INVALID_INDEX)
	{
		startIdx = countOfItems - 1;
	}

	if (_vordering == VerticalFillOrder::TOP_DOWN)
	{
		offset.y -= _viewSize.height / this->getContainer()->getScaleY();
	}
	else
	{
		offset.y += _viewSize.height / this->getContainer()->getScaleY();
	}
	offset.x += _viewSize.width / this->getContainer()->getScaleX();

	endIdx = this->_indexFromOffset(offset);
	if (endIdx == CC_INVALID_INDEX)
	{
		endIdx = countOfItems - 1;
	}

#if 0 // For Testing.
	Ref* pObj;
	int i = 0;
	CCARRAY_FOREACH(_cellsUsed, pObj)
	{
		TableViewCell* pCell = static_cast<TableViewCell*>(pObj);
		log("cells Used index %d, value = %d", i, pCell->getIdx());
		i++;
	}
	log("---------------------------------------");
	i = 0;
	CCARRAY_FOREACH(_cellsFreed, pObj)
	{
		TableViewCell* pCell = static_cast<TableViewCell*>(pObj);
		log("cells freed index %d, value = %d", i, pCell->getIdx());
		i++;
	}
	log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
#endif

	if (!_cellsUsed.empty())
	{
		auto cell = _cellsUsed.at(0);
		idx = cell->getIdx();

		while (idx < startIdx)
		{
			this->_moveCellOutOfSight(cell);
			if (!_cellsUsed.empty())
			{
				cell = _cellsUsed.at(0);
				idx = cell->getIdx();
			}
			else
			{
				break;
			}
		}
	}
	if (!_cellsUsed.empty())
	{
		auto cell = _cellsUsed.back();
		idx = cell->getIdx();

		while (idx <= maxIdx && idx > endIdx)
		{
			this->_moveCellOutOfSight(cell);
			if (!_cellsUsed.empty())
			{
				cell = _cellsUsed.back();
				idx = cell->getIdx();
			}
			else
			{
				break;
			}
		}
	}

	for (long i = startIdx; i <= endIdx; i++)
	{
		if (_indices->find(i) != _indices->end() || !m_nIsInitOK)
		{
			continue;
		}
		this->updateCellAtIndex(i);
	}
}

void RDTableView::updateCellPostion()
{
	if (_dataSource)
	{
		_updateCellPositions();
		_updateContentSize();
	}
}

void RDTableView::loadResourceByIndex(int index, vector<string>& nResource)
{
	if (nResource.size() == 0) return;

	for (string str : nResource)
	{
		bool hret = true;
		
		if (m_nBgString.size() == 0)
		{

		}
		else
		{
			for (string str1 : m_nBgString)
			{
				if (strcmp(str.c_str(), str1.c_str()) == 0)
				{
					hret = false;
				}
			}
		}
		
		
		if (hret)
		{
			m_nBgString.push_back(str);
			log("load res=============%s", str.c_str());
			SpriteFrameCache::getInstance()->addSpriteFramesWithFile(str);
		}

	}
}

Size RDTableView::getContainSize()
{
	int maxNums = _dataSource->numberOfCellsInTableView(this);

	float width = 0;
	float height = 0;
	for (int i = 0; i < maxNums-1; i++)
	{
		width += _dataSource->tableCellSizeForIndex(this, i).width;
		height += _dataSource->tableCellSizeForIndex(this, i).height;
	}

	return Size(width,height);
}

void RDTableView::addResourceByVec(int index,vector<string>& nResource)
{
	/*if (m_nResourecIndex.find(index) != m_nResourecIndex.end())
	{

	}
	else
	{
	m_nResourecIndex.insert(index,nResource);
	}*/
}
