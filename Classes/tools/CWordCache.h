//
//  CWordCache.h
//  UGameMarMonst
//
//  Created by linminglu on 14-5-6.
//
//

#ifndef __UGameMarMonst__CWordCache__
#define __UGameMarMonst__CWordCache__

#include <iostream>
//游戏的整个字典列表


#include "cocos2d.h"

using namespace cocos2d;

class CWordCache : Ref
{
public:
	CWordCache();
	~CWordCache();
    
	static CWordCache* getInstance(void);
    static void purgesharedWordCache(void);
	void addWord(__Dictionary*, const char * fileName);
	void removeWordByName(const char* fileName);
	__Dictionary * WordByName(const char* fileName);
	bool init();
private:
	__Dictionary* m_pWord;
	static CWordCache *s_pSharedWordCache;
};


#endif /* defined(__UGameMarMonst__CWordCache__) */
