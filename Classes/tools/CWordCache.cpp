//
//  CWordCache.cpp
//  UGameMarMonst
//
//  Created by linminglu on 14-5-6.
//
//

#include "CWordCache.h"

CWordCache* CWordCache::s_pSharedWordCache = NULL;

CWordCache::CWordCache()
: m_pWord(NULL)
{
}

CWordCache::~CWordCache()
{
    //	if(s_pSharedWordCache) {
    //		delete s_pSharedWordCache;
    CC_SAFE_RELEASE(m_pWord);
    //	}
}

CWordCache* CWordCache::getInstance(void)
{
	if (! s_pSharedWordCache)
	{
		s_pSharedWordCache = new CWordCache();
		s_pSharedWordCache->init();
	}
    
	return s_pSharedWordCache;
}
void CWordCache::purgesharedWordCache(void)
{
    CC_SAFE_RELEASE_NULL(s_pSharedWordCache);
}

bool CWordCache::init()
{
	m_pWord = new __Dictionary();
	return true;
}

void CWordCache::addWord(__Dictionary* obj, const char * fileName)
{
	m_pWord->setObject(obj, std::string(fileName));
}

void CWordCache::removeWordByName(const char* fileName)
{
	if (! fileName)
	{
		return;
	}
    
	m_pWord->removeObjectForKey(std::string(fileName));
}

__Dictionary* CWordCache::WordByName(const char* fileName)
{
	return (__Dictionary*)m_pWord->objectForKey(std::string(fileName));
}