﻿//
//  publicFunc.cpp
//  sanxiao
//
//  Created by linminglu on 14-4-9.
//
//

#include "publicFunc.h"
#include <algorithm>
#include <sstream>
#include <vector>
#include <math.h>
#include "CWordCache.h"
#include "UGameToolConfig.h"
#include "CReadWriteFile.h"
#include <regex>

#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
#include <sys/stat.h>
#include <dirent.h>

#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include <time.h>
#include <sys/timeb.h>
#include <time.h>
#if !defined(_WINSOCK2API_) && !defined(_WINSOCKAPI_)
struct timeval
{
	long tv_sec;
	long tv_usec;
};
#endif
//static int gettimeofday(struct timeval* tv, void *data)
//{
//	union {
//		long long ns100;
//		FILETIME ft;
//	} now;
//	GetSystemTimeAsFileTime(&now.ft);
//	tv->tv_usec = (long)((now.ns100 / 10LL) % 1000000LL);
//	tv->tv_sec = (long)((now.ns100 - 116444736000000000LL) / 10000000LL);
//
//	return (0);
//}
#else
#include <sys/time.h>
#endif




using namespace std;
namespace UGameTool {
    int randint(int smalldata, int big){

		/*std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> dis(smalldata, big);
		int value = dis(gen);*/
		int value = rand() % (big - smalldata + 1) + smalldata;
        return value;
    }
     bool isHavedata(const int *arraydata,size_t sizet,int value)
    {
        for (int i=0; i<sizet; i++) {
            if (value==arraydata[i]) {
                return true;
            }
        }
        return false;
    }
    float round(float doValue, int nPrecision)
    {
        static const float doBase = 10.0;
        float doComplete5, doComplete5i;
        
        doComplete5 = doValue * pow(doBase, (float) (nPrecision + 1));
        if(doValue < 0.0)
            doComplete5 -= 5.0;
        else
            doComplete5 += 5.0;
        doComplete5 /= doBase;
        modff(doComplete5, &doComplete5i);
        
        return doComplete5i / pow(doBase, (float) nPrecision);
    }
    
    /**********************************/
    const char * valueForKey(const char *key, __Dictionary* dict)
	{
		if (dict)
		{
			__String* pString = (__String*)dict->objectForKey(std::string(key));
			return pString ? pString->_string.c_str() : "";
		}
		return "";
	}
    string&   replace_all(string&   str,const   string&   old_value,const   string&   new_value)
    {
        while(true)   {
            string::size_type   pos(0);
            if(   (pos=str.find(old_value))!=string::npos   )
                str.replace(pos,old_value.length(),new_value);
            else   break;
        }
        return   str;
    }
    string&   replace_all_distinct(string&   str,const   string&   old_value,const   string&   new_value)
    {
        for(string::size_type   pos(0);   pos!=string::npos;   pos+=new_value.length())   {
            if(   (pos=str.find(old_value,pos))!=string::npos   )
                str.replace(pos,old_value.length(),new_value);
            else   break;
        }   
        return   str;   
    }
	std::string ReplaceAllSubstring(const std::string &ori_str, const std::string &sub_str, const std::string &new_sub_str)
	{
		std::string temp(ori_str);
		size_t pos(0);
		size_t len_of_sub_str(sub_str.length());
		size_t len_of_new_sub_str(new_sub_str.length());
		while (1) {
			pos = temp.find(sub_str, pos);
			if (pos == std::string::npos) {
				break;
			}
			else {
				temp.replace(pos, len_of_sub_str, new_sub_str);
				pos += len_of_new_sub_str;
			}
		}
		return temp;
	}

	std::string getTimeBySecond(int ft)
	{
		int minute = ft / 60.f;
		string min = "";
		if (minute < 10)
		{
			min = "0" + converInt2String(minute);
		}
		else
		{
			min = converInt2String(minute);
		}
		int second = ft % 60;
		string sec = "";
		if (second < 10)
		{
			sec = "0" + UGameTool::converInt2String(second);
		}
		else
		{
			sec = converInt2String(second);
		}

		return min + ":" + sec;
	}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define STR_2_WSTR(str, wstr, len) \
	WCHAR wstr[len]; \
	memset(wstr, 0, sizeof(wstr)); \
	MultiByteToWideChar(CP_ACP, 0, str, strlen(str) + 1, wstr, sizeof(wstr) / sizeof(wstr[0]));
#endif

	bool isHave(string filePath)
	{
		/*log("%s", (filePath).c_str());
		if (access((filePath).c_str(), 0) != -1)
		{
		return true;
		}
		else
		{
		return false;
		}*/
		return false;
	}

	void  ensure_dir_exist(std::string path)
	{
		int start = 1;
		int i = 0;

		while ((i = path.find_first_of('/', start)) != std::string::npos)
		{
			std::string sub = path.substr(0, i);
			std::string write_path = cocos2d::FileUtils::getInstance()->getWritablePath();
			write_path += sub;
			const char *dir_path = write_path.c_str();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			STR_2_WSTR(dir_path, wpath, 128);
			CreateDirectory(wpath, nullptr);
#else
			DIR *dir = opendir(dir_path);

			if (dir == NULL)
			{
				mkdir(dir_path, 0755);
			}
			else
			{
				closedir(dir);
			}
#endif

			start = i + 1;
		}
	}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#undef STR_2_WSTR
#endif

	bool checkIsRightEmailAddress(string email_address)
	{


			string user_name, domain_name;

			regex pattern("([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)");
			if (regex_match(email_address, pattern))
			{
				cout << "您输入的电子邮件地址合法" << endl;

				// 截取第一组
				user_name = regex_replace(email_address, pattern, string("$1"));

				// 截取第二组
				domain_name = regex_replace(email_address, pattern, string("$2"));

				cout << "用户名：" << user_name << endl;
				cout << "域名：" << domain_name << endl;
				cout << endl;
			}
			else
			{
				cout << "您输入的电子邮件地址不合法" << endl << endl;
				return 0;
			}
			return 1;

	}
    //生成文字
    string getWordWithFile(const string& file, const char* str) {
        //        file = WordPath + file;
        //先寻找缓冲
        __Dictionary* data = CWordCache::getInstance()->WordByName(file.c_str());
        if(!data) {
            std::string m_sPlistFile;
            if (CReadWriteFile::isHaveFile(file)) {
                m_sPlistFile=CCFileUtils::getInstance()->getWritablePath()+g_wordDirPath+file;
            }
            else
            {
            string fileName = g_wordDirPath + file;
            m_sPlistFile = CCFileUtils::getInstance()->fullPathForFilename(fileName.c_str());
            }
			log("world path = %s",m_sPlistFile.c_str());
            data = __Dictionary::createWithContentsOfFile(m_sPlistFile.c_str());
            //添加缓冲
           CWordCache::getInstance()->addWord(data, file.c_str());
          CCLOG("djkahjk");
        }
        
        string word = string(valueForKey(str, data));
        return replace_all(word,"\\n","\n");
    }
    

    /**
     该源文件的主入口方法，传入一个数组，能够对数组进行打乱
     **/
    //将一个数组打乱的方法，使得每个元素和数组其他随机元素交换的方法实现
    void shuffle(unsigned char  *arr, int len){
        for(int i=0; i<len; ++i){
            //和后面的随机交换就可以了
            std::swap(arr[i], arr[randint(i,len-1)]);
        }
    }

    bool string2int(const char*d,int&r)
    {
        for(r=0;*d>='0'&&*d<='9';r=r*10+*(d++)-'0');
        return *d==0;
    }

    long long getCurrentTime()
    {
       time_t rawtime;
        time( &rawtime );
        return rawtime;
    }
    string converInt2String(int data)
    {
        stringstream ss;
        ss<<data;
        return ss.str();
    }

	string converFloatString(const float f, const int prec)
	{
		stringstream ss;
		ss.precision(prec);
		ss << f;
		return ss.str();
	}

	std::vector<int > splitToIntArray ( std::string &s, char delim )
	{
		std::vector<int>result;
		if (s.empty()) return result;
		std::string strs = cleanCharInString(s, '\"');
		/*s.erase ( remove_if ( s.begin ( ), s.end ( ),
			bind2nd ( equal_to<char> ( ), '\"' ) ) );*/
		std::stringstream ss(strs);
		std::string item;
		while (std::getline( ss, item, delim )) {
			if (!item.empty())
			{
				int valuetemp;
				valuetemp = atoi(item.c_str());
				//string2int ( item.c_str ( ), valuetemp );
				result.push_back ( valuetemp );
			}
			
		}
		return result;
	}
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }
    
    
    std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, elems);
        return elems;
    }
    /**
     * @brief convert string to upper case through toupper, which transform a char into upper case
     *
     * @param s
     *
     * @return the upper case result string
     */
    string strtoupper(string s)
    {
        string t = s;
        int i = -1;
        while(t[i++])
        {
            t[i] = toupper(t[i]);
        }
        return t;
    }
    
    /**
     * @brief convert string to lower case throught transform method, also can use transform method directly
     *
     * @param s
     *
     * @return the lower case result saved still in s
     */
    string& strtolower(string& s)
    {
        transform(s.begin(),s.end(),s.begin(),::tolower);
        return s;
    }
    
    /**
     * @brief convert string to lower case through tolower, which transform a char into lower case
     *
     * @param s
     *
     * @return the lower case result string
     */
    string strtolower(string s)
    {
        string t = s;
        int i = -1;
        while(t[i++])
        {
            t[i] = tolower(t[i]);
        }
        return t;
    }

	std::string cleanCharInString(std::string str, char delim)
	{
			 string::iterator   it;

  		  for (it =str.begin(); it != str.end();)
  		  {
     		   if ( *it == delim)
      			{
      		      it=str.erase(it);
       			}
			   else
			   {
				   it++;
			   }
   		 }

 			  return str;
	}

}
