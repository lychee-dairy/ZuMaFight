﻿//
//  CReadWriteFile.h
//  UGameMarMonst
//
//  Created by linminglu on 14-5-10.
//
//

#ifndef __UGameMarMonst__CReadWriteFile__
#define __UGameMarMonst__CReadWriteFile__

#include <iostream>
#include <string>
using namespace std;

class  CReadWriteFile
{
public:
    /** 存储结构的好处是 可以 为未来扩展成员*/
    static bool isHaveFile(string pFileName);
    
    /** 储存内容到文件 */
    static bool saveFile(string pFileName,void*tempGamesState, size_t structsizeof);
	static bool  createFolder(const char *path);
    
};

#endif /* defined(__UGameMarMonst__CReadWriteFile__) */
