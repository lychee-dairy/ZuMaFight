/****************************************************************************
Copyright (c) 2014 
AUTHOR: rainDreamer


THE SOFTWARE.
****************************************************************************/

#ifndef __RDTABLEVIEW_H__
#define __RDTABLEVIEW_H__
#include "extensions/cocos-ext.h"
#include "publicFunc.h"
#include "Header.h"
#include "VisibleRect.h"

class RDTableView:public cocos2d::extension::TableView
{
public:
	typedef std::function<void(bool)> ScorllingEvent;
public:
	RDTableView();
	~RDTableView();

	static RDTableView* create();
	static RDTableView* create(TableViewDataSource* dataSource, Size size);
	static RDTableView* create(TableViewDataSource* dataSource, Size size, Node *container);
	
	////init
	void initMoveCellToByIndex(int index);
	void initMoveCellToByPercent(float percent);
	void setScollingControlCallback(ScorllingEvent event);

	////didScroll
	virtual void scrollViewDidScroll(ScrollView* view);
	/*virtual void scrollViewDidZoom(ScrollView* view)  override {}*/
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent) override;
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent) override;
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent) override;
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent) override;

	////
	void updateCellPostion();

	////
	void moveTableCellByPercent(float percent);
	void moveTableCellByCellIndex(int cellIndex);

	//resource
	void addResourceByVec(int index, vector<string>& nResource);
	void loadResourceByIndex(int index, vector<string>& nResource);


	Size getContainSize();
private:
	//Map<int, vector<string>> m_nResourecIndex; //cellResource

	bool m_nIsInitOK;
	vector<string> m_nBgString;

	ScorllingEvent m_event;
};

#endif