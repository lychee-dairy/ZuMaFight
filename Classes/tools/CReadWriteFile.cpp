﻿//
//  CReadWriteFile.cpp
//  UGameMarMonst
//
//  Created by linminglu on 14-5-10.
//
//

#include "CReadWriteFile.h"
#include "cocos2d.h"
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
#include <io.h>
#else
#include <unistd.h>
#endif

#if CC_TARGET_PLATFORM!=CC_PLATFORM_WIN32
#include <dirent.h>
#include <sys/stat.h>
#endif


using namespace std;
using namespace cocos2d;
/** 存储结构的好处是 可以 为未来扩展成员*/
bool  CReadWriteFile::isHaveFile(string pFileName)
{
    
    log("%s",(FileUtils::getInstance()->getWritablePath()+pFileName).c_str());
	if(access((FileUtils::getInstance()->getWritablePath()+pFileName).c_str(),0)!=-1)
	{
        return true;
    }
    else
    {
        return false;
        
    }
    
}

/** 储存内容到文件 */
bool CReadWriteFile::saveFile(string pFileName,void*tempGamesState, size_t structsizeof)
{
    FILE *fp=fopen((CCFileUtils::getInstance()->getWritablePath()+pFileName).c_str(),"wb");
	if(fp)
	{
        //加密下字段
        if(fwrite(tempGamesState,structsizeof,1,fp)==1)
        {
            fclose(fp);
            return true;
        }
        fclose(fp);
        return false;
        
	}
	else
	{
		return false;
        
	}
}

bool CReadWriteFile::createFolder(const char *path)
{
#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)

	int ret = mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO);

	if (ret != 0 && (errno != EEXIST))

	{

		return false;

	}

	return true;

#else

	BOOL ret = CreateDirectoryA(path, NULL);

	if (!ret && ERROR_ALREADY_EXISTS != GetLastError())

	{

		return false;

	}

	return true;

#endif
}
