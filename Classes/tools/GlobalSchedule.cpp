/////////////////////
// GlobalSchedule.cpp
/////////////////////

#include "GlobalSchedule.h"

#define SCHEDULE Director::getInstance()->getScheduler()

#define HOUR 12
#define MIN  10
#define SEC  0
#define LASTTIME 60

GlobalSchedule* GlobalSchedule::m_pSchedule = NULL;

GlobalSchedule::GlobalSchedule(float fInterval, float fDelay) {
 log("GlobalSchedule()");

  CCAssert(!m_pSchedule, "以定义，不能重复定义");
  
  //CCDirector::sharedDirector()->setScheduler()
  SCHEDULE->scheduleSelector(schedule_selector(GlobalSchedule::globalUpdate), this, fInterval, kRepeatForever, fDelay, false);
   
  m_pSchedule = this;
  start_ = false;
  
  lastTime = 0;
}

GlobalSchedule::~GlobalSchedule() {
	log("GlobalSchedule().~()");

  SCHEDULE->unscheduleSelector(schedule_selector(GlobalSchedule::globalUpdate), this);
}

void  GlobalSchedule::globalUpdate(float ft) {
  // 这里写全局定时器的逻辑处理代码
  //log("global update:%d",);

  //显示时间
  unsigned long long timestamp = time(NULL);
  struct tm *ptm = localtime((time_t*)&timestamp);
  /*char tmp[100] = { 0 };
  memset(tmp, 0x0, 100);
  strftime(tmp, sizeof(tmp), "%Y-%m-%d %H:%M:%S", ptm);*/
  
  int hour = ptm->tm_hour;
  int min = ptm->tm_min;
  int sec = ptm->tm_sec;

  if (hour == HOUR && min == MIN && sec == SEC)
  {
	  log("%d:%d:%d", hour,min,sec);
	  int rand = UGameTool::randint(0, 100);

	  if (rand > 50)
	  {
		  RDPlatManager::instance()->showAdmobAds(EN_ADS_BANER, true);
	  }
	  else
	  {
		  RDPlatManager::instance()->showAdmobAds(EN_ADS_INSTER, true);
	  }
	  lastTime = time(NULL);
  }

  if (lastTime != 0 )
  {
	  if (time(NULL) - lastTime > LASTTIME)
	  {
		  RDPlatManager::instance()->showAdmobAds(EN_ADS_BANER, false);
		  lastTime = 0;
	  }
  }

}

GlobalSchedule* GlobalSchedule::getInstance()
{
  if (!m_pSchedule)
  {
    return NULL;
  }
  return m_pSchedule;
}

void GlobalSchedule::start(float fInterval, float fDelay) {
  new GlobalSchedule(fInterval, fDelay);
}

void GlobalSchedule::stop() {
  log("GlobalSchedule().clean()");

  CCAssert(m_pSchedule, "未定义");
  CC_SAFE_DELETE(m_pSchedule);
}

void GlobalSchedule::pause() {
  log("GlobalSchedule().pause()");
 
  CCAssert(m_pSchedule, "为定义");
  SCHEDULE->pauseTarget(m_pSchedule);
}

void GlobalSchedule::resume() {
  log("GlobalSchedule().resume()");

  CCAssert(m_pSchedule, " 未定义");
  SCHEDULE->resumeTarget(m_pSchedule);
}