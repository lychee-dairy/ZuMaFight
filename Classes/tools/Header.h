//
//  Header.h
//  UGameMarMonst
//
//  Created by linminglu on 14-5-31.
//
//

#ifndef _SLIMEHIT_Header_
#define _SLIMEHIT_Header_
#include "cocos2d.h"
#if CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID
#include "extensions/cocos-ext.h"
#else
#include "extensions/cocos-ext.h"
#endif
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "RDPlatManager.h"
#include "VisibleRect.h"
#include "AnimotePack.h"
#include "publicFunc.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

#define PI 3.1415926f

#define SP_GUIReader GUIReader::getInstance()

#define SEEK_WIDGET_BY_NAME(T,S,P) (dynamic_cast<T*>(Helper::seekWidgetByName(P, S)))

#define  CHAPTERMAX 3  //最大章节数
#define  CHAPTERSTAGEMAX 8 //最大关卡数

#define CURRENTMAXSTAGE 1

#define Transparency 80

#define BOXRECT 21.6f

#define CHANGLECOLORNUMS 3

//event
#define PAUSEGAME "pauseGame"
#define REPLAYGAME "replaygame"
#define SAVEDATA "savedata"
#define ADDBALLTAG "addballtag"

enum RealityFrameRate
{
	EN_FRATERATE_NONE,
	EN_FRATERATE_3To2,
	EN_FRATERATE_16To9,
};

enum EN_ZM_TYPE
{
	EN_ZM_NONE = 0,
	EN_ZM_Ball1,
	EN_ZM_Ball2,
	EN_ZM_Ball3,
	EN_ZM_Ball4,
	EN_ZM_Ball5,
	EN_ZM_Ball6,
};

enum EN_PRO_TYPE
{
	EN_PRO_NONE = 0,
	EN_PRO_STOP,
	EN_PRO_BOMB,
	EN_PRO_BACK,
	EN_PRO_GO,
	EN_PRO_CHANGECOLOR,
	EN_RPO_GETSCORE,
};

enum EN_SCENE_TYPE
{
	EN_SCENE_NONE,
	EN_SCENE_MAINMENU,
	EN_SCENE_SELECTSTAGE,
	EN_SCENE_FIGHT,
	EN_SCENE_MAPEDIT,
};

#endif /*_SLIMEHIT_Header_*/
