#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "MainMenu.h"
#include "MapEditeScene.h"
#include "ResoureManager.h"
#include "RDSceneManager.h"
#include "GlobalSchedule.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("ZhumaFight");
		//glview->setFrameSize(960, 640);
		glview->setFrameSize(1136, 640);
        director->setOpenGLView(glview);
    }

	glview->setDesignResolutionSize(568, 320, ResolutionPolicy::NO_BORDER);


	//搜索目录列表
	std::vector<std::string> searchPaths;
	searchPaths = FileUtils::getInstance()->getSearchPaths();

	////其他资源
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0]+ "ipad/");
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone/"); 
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone/character1");
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone/character2");
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone/character3");
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone2x/");         
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone2x/character1");
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone2x/character2");
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "iphone2x/character3");
	searchPaths.push_back(FileUtils::getInstance()->getSearchPaths()[0] + "maps/");

	//cocos2d 配置
	cocos2d::FileUtils::getInstance()->setSearchPaths(searchPaths); //添加搜索目录


    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);


	ResoureManager::instance();
    // create a scene. it's an autorelease object
	RDSceneManager::instance()->RunTargetScene(EN_SCENE_MAINMENU);

	// 每 1 秒间隔运行，延迟 3 秒启动
	//GlobalSchedule::start(1.f, 3.0f);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
