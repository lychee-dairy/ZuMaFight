#include "LevelData.h"
#include "publicFunc.h"
#include "ResoureManager.h"

//const float m_nDeafaultWidth = 960;
//const float m_nDeafaultHeight = 640;

LevelData::LevelData()
{
	srand(unsigned(time(NULL)));
}

LevelData::~LevelData()
{

}

void LevelData::initWithXML(int stage, Size winSize)
{
	m_nLevel = stage;
	m_nrollTo = 0;
	m_nColorCount = 0;
	m_nlPosCount = 0;
	m_winSize = winSize;
	initStageInf(stage);

	m_levelData =FileUtils::getInstance()->getSearchPaths()[0]+m_nTrackXML;
	log("load xml %s",m_levelData.c_str());

	if (ResoureManager::instance()->fileIsExit(m_nTrackXML))
	{
		initLevelData();
	}
}

int LevelData::getLevel()
{

	return m_nLevel;
}

int LevelData::getRollTo()
{
	return m_nrollTo;
}

Point LevelData::getShooterPos()
{
	return m_ptShooterPos;
}

void LevelData::setShooterPos(Point shootPos)
{
	m_ptShooterPos = shootPos;
}

int LevelData::getColorCount()
{
	return m_nColorCount;
}

int LevelData::getColorFromIndex(int nIndex)
{
	log("index%d",nIndex);
	if (nIndex < 0)
		return -1;

	if (nIndex >= m_nColorCount)
		return -1;

	return m_colorArray[nIndex];
}

int LevelData::getLocationCount()
{
	return m_locationArray.size();
	//return m_nlPosCount;
}

Point LevelData::getPosFromIndex(int nIndex)
{
	Point pt; pt.x = 0; pt.y = 0;
	if (nIndex < 0)
		return pt;

	if (nIndex >= getLocationCount())
		return pt;

	return m_locationArray[nIndex];
}

Point LevelData::getStartLineFrom()
{

	return m_ptStartLineFrom;
}

Point LevelData::getStartLineTo()
{
	return m_ptStartLineTo;
}

/////////////////////////////////////////////////

////EDITMAP USE

void LevelData::addPoint(Point pos)
{
	m_locationArray.push_back(pos);
}

void LevelData::removePoint(int FromeIndex, int toIndex)
{
	if (FromeIndex == 0) {
		m_locationArray.clear();
		return;
	}
	if (m_locationArray.size() < toIndex)
	{
		int i = 0;
		i++;
		return;
	}
	m_locationArray.erase(m_locationArray.begin()+FromeIndex, m_locationArray.begin() + toIndex);
}


void LevelData::addTagVect(int tag)
{
	m_TagArry.push_back(tag);
}

int LevelData::getZorderByLocalPosIndex(int localPosIndex)
{
	if (localPosIndex > 0)
	{
		if (localPosIndex%2 == 0)
		{
			int index = localPosIndex / 2;
			return m_TagArry[index - 1];
		}
		else
		{
			return m_TagArry[0];
		}
	}
	else
	{
		return m_TagArry[0];
	}
}

void LevelData::removeTag()
{
	if (m_TagArry.size() == 0) return;

	m_TagArry.pop_back();
}

bool LevelData::isExitFile(string filePath)
{
	FILE *fp = fopen(filePath.c_str(), "r");
	bool bRet = false;

	if (fp)
	{
		bRet = true;
		fclose(fp);
	}

	return bRet;
}

bool LevelData::createXMLFile(string rootName, string filePath)
{
	bool bRet = false;
	tinyxml2::XMLDocument *pDoc = new tinyxml2::XMLDocument();
	if (nullptr == pDoc)
	{
		return false;
	}
	tinyxml2::XMLDeclaration *pDeclaration = pDoc->NewDeclaration(nullptr);
	if (nullptr == pDeclaration)
	{
		return false;
	}
	pDoc->LinkEndChild(pDeclaration);
	tinyxml2::XMLElement *pRootEle = pDoc->NewElement(rootName.c_str());
	if (nullptr == pRootEle)
	{
		return false;
	}
	pDoc->LinkEndChild(pRootEle);
	bRet = tinyxml2::XML_SUCCESS == pDoc->SaveFile(filePath.c_str());

	if (pDoc)
	{
		delete pDoc;
	}

	return true;
}

bool LevelData::savaLevelData(int level)
{
	string str = "level-" + UGameTool::converInt2String(level) + ".xml";
	string fileName = ResoureManager::instance()->getFilePathth(str);
	if (!isExitFile(fileName) && !createXMLFile("info", fileName))
	{
		return false;
	}

	//save data
	tinyxml2::XMLElement* rootNode;
	tinyxml2::XMLDocument* doc;
	getXMLNodeForKey(fileName.c_str(), &rootNode, &doc);
	if (!rootNode) return false;

	string dataInf = "";
	
	////color
	//dataInf = "";
	//for (int i = 0; i < getAppearBallCounts(); i++)
	//{
	//	if (i == (getAppearBallCounts()-1))
	//	{
	//		dataInf += UGameTool::converInt2String(UGameTool::randint(1, getAppearBallTypes()));
	//	}
	//	else
	//	{
	//		dataInf += UGameTool::converInt2String(UGameTool::randint(1, getAppearBallTypes())) + ",";
	//	}
	//	
	//}
	//rootNode->SetAttribute("colorArr", dataInf.c_str());

	Point pos1 = getPosFromIndex(0);
	Point pos2 = getPosFromIndex(1);

	Point shooterPos1 = Vec2(640, 320);
	Point shooterPos2 = Vec2(640, 320);
	if (abs(pos1.x - pos2.x) >= abs(pos1.y - pos2.y))
	{
		shooterPos1.y = shooterPos2.y = pos1.y;
		if (pos1.x - pos2.x >0)
		{
			shooterPos1.x = 10000;
			shooterPos2.x = pos1.x + 10;
		}
		else
		{
			shooterPos1.x = -10000;
			shooterPos2.x = pos1.x - 10;
		}
		
	}
	else
	{
		shooterPos1.x = shooterPos2.x = pos1.x;
		if (pos1.y - pos2.y > 0)
		{
			shooterPos1.y = 10000;
			shooterPos2.y = pos1.y + 10;
		}
		else
		{
			shooterPos1.y = -10000;
			shooterPos2.y = pos1.y - 10;
		}

	}

	//startline
	dataInf = "";
	dataInf += UGameTool::converInt2String(shooterPos1.x) + " ";
	dataInf += UGameTool::converInt2String(shooterPos1.y) + " ";
	dataInf += UGameTool::converInt2String(shooterPos2.x) + " ";
	dataInf += UGameTool::converInt2String(shooterPos2.y);
	rootNode->SetAttribute("startline", dataInf.c_str());
	 
	//control pos
	dataInf += " ";
	for (int i = 0; i < getLocationCount(); i++)
	{
		Point pos = getPosFromIndex(i);
		if (i == getLocationCount()-1)
		{
			dataInf += UGameTool::converInt2String(pos.x) + " ";
			dataInf += UGameTool::converInt2String(pos.y);
		}
		else
		{
			dataInf += UGameTool::converInt2String(pos.x) + " ";
			dataInf += UGameTool::converInt2String(pos.y) + " ";
		}
		
	}

	rootNode->SetAttribute("data",dataInf.c_str());
	
	//control pos
	dataInf = "";
	dataInf += "5,";
	for (int i = 0; i < m_TagArry.size(); i++)
	{
		Point pos = getPosFromIndex(i);
		if (i == m_TagArry.size() - 1)
		{
			int tag = m_TagArry[i];
			dataInf += UGameTool::converInt2String(tag);
		}
		else
		{
			int tag = m_TagArry[i];
			dataInf += UGameTool::converInt2String(tag) + ",";
		}
	}

	rootNode->SetAttribute("Zorder", dataInf.c_str());

	tinyxml2::XML_SUCCESS == doc->SaveFile(fileName.c_str());

	if (doc)
	{
		delete doc;
	}

	return true;
}

/////////////////////////////////////////////////


void LevelData::initStageInf(int level)
{
	ValueMap map = ResoureManager::instance()->getStageDataByStage(level);

	m_nBGName = map["BGFile"].asString();
	m_nTrackXML = map["trackXmlFile"].asString();
	m_nTrackFileJson = map["trackJsonFile"].asString();
	m_nAppearBallTypes = map["appearBallTypes"].asInt();
	m_nAppearBallCounts = map["appearBallCount"].asInt();
	m_nIsEdit = map["isEidit"].asBool();
	m_nStartGamePosIndex = map["startPosIndex"].asInt(); //游戏正式开始的位置索引

	m_nBombAppearTimes = map["AppearTimes_1"].asInt(); //炸弹出现的次数
	m_nStopAppearTimes = map["AppearTimes_2"].asInt(); //暂停道具出现的次数
	m_nBackAppearTimes = map["AppearTimes_3"].asInt(); //后退道具出现的次数
	m_nGoAppearTimes = map["AppearTimes_4"].asInt(); //前进炸弹出现的次数
	m_nChangeColorAppearTimes = map["AppearTimes_5"].asInt(); //改变颜色出现的次数
	m_nGetScoreAppearTimes = map["AppearTimes_6"].asInt(); //得分道具出现的次数


	//ball nums
	m_colorArray.clear();
	m_nColorCount = getAppearBallCounts();
	for (int i = 0; i < m_nColorCount; i++)
	{
		m_colorArray.push_back(UGameTool::randint(1, getAppearBallTypes()));
	}
}


void LevelData::initLevelData()
{
	
	log("initlevel");
	tinyxml2::XMLElement* rootNode;
	tinyxml2::XMLDocument* doc;
	getXMLNodeForKey(m_levelData.c_str(), &rootNode, &doc);

	if (!rootNode) return;
		

	// level info
	m_nDeafaultWidth = 568;
	m_nDeafaultHeight = 320;

	float fRate = m_winSize.height / 320;
	float fOffsetX = (m_winSize.width - 568 * fRate) / 2;

	// colorArray info
	/*string strcolorArr = getStringByKeys("colorArr", rootNode);
	log("color arr %s" , strcolorArr.c_str());
	std::vector<int> colorArray = UGameTool::splitToIntArray(strcolorArr, ',');
	m_nColorCount = colorArray.size();
	for (int i = 0; i < m_nColorCount; i++)
	{
	m_colorArray.push_back(colorArray[i]);
	}*/

	// startline
	string strStartLineArr = getStringByKeys("startline", rootNode);
	std::vector<int> lineArray = UGameTool::splitToIntArray(strStartLineArr, ' ');
   if (lineArray.size() == 4)
	{
		float strOnePos = lineArray[0];
		m_ptStartLineFrom.x = strOnePos * fRate + fOffsetX;
		strOnePos = lineArray[1];
		m_ptStartLineFrom.y = strOnePos*fRate;
		
		strOnePos = lineArray[2];
		m_ptStartLineTo.x = strOnePos * fRate + fOffsetX;
		strOnePos = lineArray[3];
		m_ptStartLineTo.y = strOnePos*fRate;
		
	}

	// location info
	string strDataArr = getStringByKeys("data", rootNode);
	std::vector<int> dataArray = UGameTool::splitToIntArray(strDataArr, ' ');
	m_nlPosCount = dataArray.size();
	float x = 0;
	float y = 0;
	for (int i = 0; i < m_nlPosCount; i++)
	{
		int strOnePos = dataArray[i];
		if (i % 2 == 0)
		{
			x = strOnePos * fRate + fOffsetX;
			//m_locationArray[i / 2].x = strOnePos * fRate + fOffsetX;
		}
		else
		{
			y = strOnePos;
			m_locationArray.push_back(Point(x,y));
			//m_locationArray[i / 2].y = (m_nDeafaultHeight - strOnePos)*fRate;
		}
	}

	m_nlPosCount /= 2;



	string strZorderArr = getStringByKeys("Zorder", rootNode);
	std::vector<int> zorder = UGameTool::splitToIntArray(strZorderArr, ',');
	int tagSize = zorder.size();
	for (int i = 0; i < tagSize; i++)
	{
		int zorders = zorder[i];
		m_TagArry.push_back(zorders);
	}

	CC_SAFE_DELETE(doc);
}

string LevelData::getStringByKeys(string keys, tinyxml2::XMLElement* rootNode)
{
	const char* text = rootNode->Attribute(keys.c_str());
	return text;
}

tinyxml2::XMLElement* LevelData::getXMLNodeForKey(const char* pKey, tinyxml2::XMLElement** rootNode, tinyxml2::XMLDocument **doc)
{
	tinyxml2::XMLDocument* xmlDoc = new tinyxml2::XMLDocument();
	*doc = xmlDoc;

	std::string xmlBuffer = FileUtils::getInstance()->getStringFromFile(pKey);

	if (xmlBuffer.empty())
	{
		CCLOG("can not read xml file");
		return nullptr;
	}
	xmlDoc->Parse(xmlBuffer.c_str(), xmlBuffer.size());

	//get root node
	*rootNode = xmlDoc->RootElement();

	return *rootNode;
}


