//
//  PlayerData.cpp
//  UGameMarMonst
//
//  Created by linminglu on 14-6-3.
//
//

#include "PlayerManager.h"
#include "publicFunc.h"
#include "VisibleRect.h"
#include "SimpleAudioEngine.h"
#include "ResoureManager.h"

#define  UNLOCK "unclok_"
#define  LEVELSCORE "level_score_"


using namespace CocosDenshion;

PlayerManager::PlayerManager() {
	init();
}

PlayerManager::~PlayerManager() {

}

void PlayerManager::init()
{
	//player data init
	if (!UserDefault::getInstance()->getBoolForKey("isExit"))
	{
		UserDefault::getInstance()->setBoolForKey("isExit", true);
		//lock data
		setLevelIsUnlockByLevel(1);

		//score
		setLevelScore(1,0);
	}
	

	m_nSelectedCharacter = 0;
	m_nCurrentStage = 0;
	m_nCurrentTotalStage = 0;

}


bool PlayerManager::getLevelisUnlock(int nLevel)
{
	string levelunlock = UNLOCK + UGameTool::converFloatString(nLevel);
	bool lock = UserDefault::getInstance()->getBoolForKey(levelunlock.c_str());
	
	return lock;
}

void PlayerManager::setLevelIsUnlockByLevel(int nLevel)
{
	string levelunlock = UNLOCK + UGameTool::converFloatString(nLevel);
	if (!getLevelisUnlock(nLevel))
	{
		UserDefault::getInstance()->setBoolForKey(levelunlock.c_str(),true);
		UserDefault::getInstance()->flush();
	}
}

void PlayerManager::setLevelScore(int nLevel, int nScore)
{
	string levelscore = LEVELSCORE + UGameTool::converFloatString(nLevel);
	if (nScore > getLevelScore(nLevel))
	{
		UserDefault::getInstance()->setIntegerForKey(levelscore.c_str(), nScore);
		UserDefault::getInstance()->flush();
	}
}

string PlayerManager::getGameInfByName(string str)
{
	ValueMap map = ResoureManager::instance()->getGameInf();
	string data = map[str].asString();
	return data;
}

int PlayerManager::getEditMaxStage()
{
	ValueMap map = ResoureManager::instance()->getGameInf();
	int data = map["EditMaxStage"].asInt();
	return data;
}

float PlayerManager::defaultSpeedUpByLevel()
{
	ValueMap map = ResoureManager::instance()->getGameInf();
	int data = map["SpeedUpRateByLevel"].asInt();
	return data/100.f;
}

float PlayerManager::defaultBallOffect()
{
	ValueMap map = ResoureManager::instance()->getGameInf();
	int data = map["ball_offect"].asInt();
	return data/100.f;
}

int PlayerManager::getTotalStage()
{
	ValueMap map = ResoureManager::instance()->getGameInf();
	int data = map["TotalStage"].asInt();
	return data;
}

int PlayerManager::getLevelScore(int nLevel)
{
	string levelscore = LEVELSCORE + UGameTool::converFloatString(nLevel);
	int score = UserDefault::getInstance()->getIntegerForKey(levelscore.c_str());
	return score;
}
