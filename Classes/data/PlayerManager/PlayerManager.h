//
//  PlayerData.h
//  UGameMarMonst
//
//  Created by linminglu on 14-6-3.
//
//

#ifndef __ZHUMAFIGHT__PlayerManagerData__
#define __ZHUMAFIGHT__PlayerManagerData__

#include <iostream>
#include "cocos2d.h"
#include "CSingleton.h"
#include "Header.h"

using namespace std;
USING_NS_CC;
class PlayerManager : public Singleton <PlayerManager>
{
public:
	
	~PlayerManager();

	//level data
	bool getLevelisUnlock(int nLevel);
	void setLevelIsUnlockByLevel(int nLevel);

	//level score
	int getLevelScore(int nLevel);
	void setLevelScore(int nLevel, int nScore);


	string getGameInfByName(string str);
	int getTotalStage();
	int getEditMaxStage();
	float defaultSpeedUpByLevel();
	float defaultBallOffect();
private:
	CC_SYNTHESIZE(int, m_nCurrentStage, CurrentStage);
	CC_SYNTHESIZE(int, m_nSelectedCharacter, SelectedCharacter);
	CC_SYNTHESIZE(int, m_nCurrentTotalStage, CurrentTotalStage);


private:
	PlayerManager();
	
	void init();
	SINGLETON_FRIENDS;
};

#endif /*__ZHUMAFIGHT__PlayerManagerData__*/
