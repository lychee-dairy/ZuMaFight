#include "ResoureManager.h"
#include "RDCSVDataManage.h"

ResoureManager::ResoureManager()
{
	init();
}

ResoureManager::~ResoureManager()
{

}

void ResoureManager::init()
{
	loadStageData("StageData.csv");
	loadGameInf("gameInf.csv");
}

ValueMap ResoureManager::getStageDataByStage(int level)
{
	if (m_nStageData.find(UGameTool::converInt2String(level)) != m_nStageData.end())
	{
		return m_nStageData.find(UGameTool::converInt2String(level))->second.asValueMap();
	}
	
	ValueMap map;
	return map;
}

bool ResoureManager::fileIsExit(string filePath)
{
	std::string TPath = cocos2d::FileUtils::getInstance()->fullPathForFilename(filePath);
	bool TIsHave = cocos2d::FileUtils::getInstance()->isFileExist(TPath);
	log("IsHaveFile %s : %i", TPath.c_str(), TIsHave);
	return TIsHave;
}

string ResoureManager::getFilePathth(string fileName)
{
	string filebase = "";
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	filebase = "e:/";
#else
	filebase = CCFileUtils::sharedFileUtils()->getWritablePath().c_str();
#endif
	filebase += fileName;

	return filebase;
}

void ResoureManager::loadStageData(string str)
{
	m_nStageData = RDCSVDataManage::instance()->getValuMapByCSV(str);
}

void ResoureManager::loadGameInf(string str)
{
	ValueMap value = RDCSVDataManage::instance()->getValuMapByCSV(str);
	m_nGameInf = value["1"].asValueMap();
}