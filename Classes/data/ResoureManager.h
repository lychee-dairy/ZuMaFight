//
//  PlayerData.h
//  UGameMarMonst
//
//  Created by linminglu on 14-6-3.
//
//

#ifndef __ZHUMAFIGHT__ResoureManagerData__
#define __ZHUMAFIGHT__ResoureManagerData__

#include <iostream>
#include "cocos2d.h"
#include "CSingleton.h"
#include "Header.h"

using namespace std;
USING_NS_CC;
class ResoureManager : public Singleton < ResoureManager >
{
public:

	~ResoureManager();

	ValueMap getStageDataByStage(int level);


	bool fileIsExit(string filePath);
	string getFilePathth(string fileName);
private:
	ResoureManager();

	void init();
	void loadStageData(string str);
	void loadGameInf(string str);

	ValueMap m_nStageData;

	CC_SYNTHESIZE(ValueMap, m_nGameInf, GameInf);
	SINGLETON_FRIENDS;
};

#endif /*__ZHUMAFIGHT__ResoureManagerData__*/
