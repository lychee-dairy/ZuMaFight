﻿//
//  CWJson.h
//  GFCocos2dX
//
//  Created by 文智旭 on 14-2-17.
//
//

#ifndef __GFCocos2dX__CWJson__
#define __GFCocos2dX__CWJson__

#include "Header.h"

class Json : public cocos2d::Ref
{
private:

public:
	Json(void);
	~Json(void);

	static Json *GetInstance(void);
#define CWIJson Json::GetInstance()

public:
	std::string DataEncode(cocos2d::Ref *TData);                            //把值转换成json格式
	cocos2d::Ref *DataDecode(const std::string &TData);                     //把json格式的数据解析成字典或数组；

private:
	cocos2d::__Dictionary *DataParseToDic(const rapidjson::Value &Value);   //解析json
	cocos2d::__Array *DataParseToAr(const rapidjson::Value &Value);         //解析json
	cocos2d::Ref *DataParseToObject(const rapidjson::Value &Value);         //解析json

private:
	std::shared_ptr<rapidjson::Value> ArParseJSON(cocos2d::__Array *TAr, rapidjson::Document::AllocatorType& allocator);             //把数组转换成json格式数据；
	std::shared_ptr<rapidjson::Value> DicParseJSON(cocos2d::__Dictionary *TDic, rapidjson::Document::AllocatorType& allocator);      //把字典转换成json格式数据；
	std::shared_ptr<rapidjson::Value> ValueParseJSON(cocos2d::Ref *TValue, rapidjson::Document::AllocatorType& allocator);           //把值转换成json格式数据；
};
#endif /* defined(__GFCocos2dX__CWJson__) */
