
#ifndef _ZHUMAFIGHT_LEVELDATA_H_
#define _ZHUMAFIGHT_LEVELDATA_H_

#include "cocos2d.h"
#include "tinyxml2/tinyxml2.h"
#include "Header.h"
USING_NS_CC;
using namespace std;

class LevelData
{
public:
	LevelData();
	~LevelData();

	void initWithXML(int stage, Size winSize);
	int getLevel();
	int getRollTo();
	Point getShooterPos();
	void  setShooterPos(Point shootPos);
	int getColorCount();
	int getColorFromIndex(int nIndex);
	int getLocationCount();
	Point getPosFromIndex(int nIndex);
	Point getStartLineFrom();
	Point getStartLineTo();

	////
	void addPoint(Point pos);
	void removePoint(int FromeIndex , int toIndex);

	int getZorderByLocalPosIndex(int localPosIndex);
	void addTagVect(int tag);
	void removeTag();


	bool savaLevelData(int level);
	bool isExitFile(string filePath);
	bool createXMLFile(string rootName , string filePath);


private:
	tinyxml2::XMLElement* getXMLNodeForKey(const char* pKey, tinyxml2::XMLElement** rootNode, tinyxml2::XMLDocument **doc);
	void initLevelData();
	void initStageInf(int level);

	string getStringByKeys(string keys, tinyxml2::XMLElement* rootNode);
private:

	CC_SYNTHESIZE(string, m_nBGName, BGName);
	CC_SYNTHESIZE(string, m_nTrackFileJson, TrackFileNameJson);
	CC_SYNTHESIZE(string, m_nTrackXML, TrackXML);
	CC_SYNTHESIZE(int, m_nAppearBallTypes, AppearBallTypes);
	CC_SYNTHESIZE(int, m_nAppearBallCounts, AppearBallCounts);
	CC_SYNTHESIZE(bool, m_nIsEdit, IsEdit);
	CC_SYNTHESIZE(int, m_nStartGamePosIndex, StartGamePosIndex);

	CC_SYNTHESIZE(int, m_nBombAppearTimes, BombAppearTimes);
	CC_SYNTHESIZE(int, m_nStopAppearTimes, StopAppearTimes);
	CC_SYNTHESIZE(int, m_nBackAppearTimes, BackAppearTimes);
	CC_SYNTHESIZE(int, m_nGoAppearTimes,  GoAppearTimes);
	CC_SYNTHESIZE(int, m_nChangeColorAppearTimes, ChangeColorAppearTimes);
	CC_SYNTHESIZE(int, m_nGetScoreAppearTimes, GetScoreAppearTimes);

	
	int         m_nLevel;
	int         m_nrollTo;
	Point     m_ptShooterPos;
	vector<int>         m_colorArray;
	int         m_nColorCount;
	vector<Point>     m_locationArray;
	vector<int>		  m_TagArry;
	int         m_nlPosCount;
	Point     m_ptStartLineFrom;
	Point     m_ptStartLineTo;
	Size      m_winSize;

	string      m_levelData;


	int  m_nDeafaultWidth;
	int  m_nDeafaultHeight;

};

#endif	_ZHUMAFIGHT_LEVELDATA_H_
