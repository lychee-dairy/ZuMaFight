﻿//
//  CWJson.cpp
//  GFCocos2dX
//
//  Created by 文智旭 on 14-2-17.
//
//

#include "CWJson.h"
//json
#include "../../cocos2d/external/json/document.h"
#include "../../cocos2d/external/json/filestream.h"
#include "../../cocos2d/external/json/prettywriter.h"
#include "../../cocos2d/external/json/stringbuffer.h"

Json::Json(void)
{
    
}

Json::~Json(void)
{
    
}

Json *Json::GetInstance(void)
{
	static Json *JSON_ = NULL;
    
	if (JSON_ == NULL)
	{
		JSON_ = new Json();
	}
    
	return JSON_;
}

std::string Json::DataEncode(cocos2d::Ref *TData)
{
	rapidjson::Document document;
	document.SetObject();
	rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
    
	std::shared_ptr<rapidjson::Value> TValue;
    
	if (dynamic_cast<cocos2d::__Dictionary *>(TData))
	{
		TValue = this->DicParseJSON((cocos2d::__Dictionary *)TData, allocator);
	}
	else if (dynamic_cast<cocos2d::__Array *>(TData))
	{
		TValue = this->ArParseJSON((cocos2d::__Array *)TData, allocator);
	}
	else
	{
		TValue = this->ValueParseJSON(TData, allocator);
	}
    
    rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	(*TValue).Accept(writer);
    
	return buffer.GetString();
}

cocos2d::Ref *Json::DataDecode(const std::string &TData)
{
	//把数据解析层json对象值
	rapidjson::Document doc;
	doc.Parse<0>(TData.c_str());
    
	if (doc.HasParseError())
	{
		return NULL;
	}
    
	rapidjson::Value &Value = doc;
	switch (Value.GetType())
	{
        case rapidjson::kArrayType:
        {
            return this->DataParseToAr(Value);
        }
            break;
        case rapidjson::kObjectType:
        {
            return this->DataParseToDic(Value);
        }
            break;
        default:
        {
            return this->DataParseToObject(Value);
        }
            break;
	}
    
	return nullptr;
}

#pragma mark -
cocos2d::__Dictionary *Json::DataParseToDic(const rapidjson::Value &Value)
{
    cocos2d::__Dictionary *TData = cocos2d::__Dictionary::create();
    
	//获取键值
	for (rapidjson::Value::ConstMemberIterator TMember = Value.MemberonBegin(); TMember != Value.MemberonEnd(); ++TMember)
	{
		const rapidjson::Value &name = TMember->name;
		const rapidjson::Value &val = TMember->value;
        
		cocos2d::Ref *TChildData = NULL;            //相应数据
        
		//判断相应数据类型，转换数据
		switch (val.GetType())
		{
            case rapidjson::kArrayType:
            {
                TChildData = this->DataParseToAr(val);
            }
                break;
            case rapidjson::kObjectType:
            {
                TChildData = this->DataParseToDic(val);
            }
                break;
            default:
            {
                TChildData = this->DataParseToObject(val);
            }
                break;
		}
        
		TData->setObject(TChildData, name.GetString());
	}
    
	return TData;
}

cocos2d::__Array *Json::DataParseToAr(const rapidjson::Value &Value)
{
    cocos2d::__Array *TData = cocos2d::__Array::create();
    
	//获取键值
	for (unsigned int i = 0; i<Value.Size(); ++i)
	{
		const rapidjson::Value &val = Value[i];
		cocos2d::Ref *TChildData = NULL;            //相应数据
        
		//判断相应数据类型，转换数据
		switch (val.GetType())
		{
            case rapidjson::kArrayType:
            {
                TChildData = this->DataParseToAr(val);
            }
                break;
            case rapidjson::kObjectType:
            {
                TChildData = this->DataParseToDic(val);
            }
                break;
            default:
            {
                TChildData = this->DataParseToObject(val);
            }
                break;
		}
        
		TData->addObject(TChildData);
	}
    
	return TData;
}

cocos2d::Ref *Json::DataParseToObject(const rapidjson::Value &Value)
{
	cocos2d::Ref *TData = NULL;
	switch (Value.GetType())
	{
        case rapidjson::kNumberType:
        {
            if (Value.IsInt())
            {
                TData = __String::createWithFormat("%i",Value.GetInt());
            }
            else if (Value.IsUint())
            {
                TData = __String::createWithFormat("%i",Value.GetUint());
            }
            else if (Value.IsInt64())
            {
                TData = __String::createWithFormat("%lli",Value.GetInt64());
            }
            else if (Value.IsUint64())
            {
                TData = __String::createWithFormat("%lli",Value.GetUint64());
            }
            else if (Value.IsBool())
            {
                TData = __String::createWithFormat("%i",Value.GetBool());
            }
            else if (Value.IsDouble())
            {
                TData = __String::createWithFormat("%lf",Value.GetDouble());
            }
            else
            {
                CCLOG("Json Value kNumberType 不再支持队列！");
            }
        }
            break;
        case rapidjson::kStringType:
        {
            TData = __String::createWithFormat("%s",Value.GetString());
        }
            break;
        case rapidjson::kTrueType:
        case rapidjson::kFalseType:
        {
            TData = __String::createWithFormat("%i",(int)Value.GetBool());
        }
            break;
        default:
        {
            TData = __String::createWithFormat("");
            CCLOG("JSON Value is Error!");
        }
            break;
	}
    
	return TData;
}

#pragma mark -
std::shared_ptr<rapidjson::Value> Json::ArParseJSON(cocos2d::__Array *TAr, rapidjson::Document::AllocatorType& allocator)
{
	std::shared_ptr<rapidjson::Value> TValue(new rapidjson::Value);
	(*TValue).SetArray();
    
	cocos2d::Ref *TObj = nullptr;
	USING_NS_CC;
	CCARRAY_FOREACH(TAr, TObj)
	{
		std::shared_ptr<rapidjson::Value> TValueData;
		if (dynamic_cast<cocos2d::__Dictionary *>(TObj))
		{
			TValueData = DicParseJSON((cocos2d::__Dictionary *)TObj, allocator);
		}
		else if (dynamic_cast<cocos2d::__Array *>(TObj))
		{
			TValueData = ArParseJSON((cocos2d::__Array *)TObj, allocator);
		}
		else
		{
			TValueData = ValueParseJSON(TObj, allocator);
		}
        
		(*TValue).PushBack((*TValueData), allocator);
	}
    
	return TValue;
}

char *CopyChar(const char *TData, long int TSize)
{
    //注意需要自己释放
    char *TValue = new char[TSize + 1];
    memset(TValue, 0, TSize + 1);
    memmove(TValue, TData, TSize);
    return TValue;
}

std::shared_ptr<rapidjson::Value> Json::DicParseJSON(cocos2d::__Dictionary *TDic, rapidjson::Document::AllocatorType& allocator)
{
	std::shared_ptr<rapidjson::Value> TValue(new rapidjson::Value);
	(*TValue).SetObject();
    
	cocos2d::DictElement *TElement = NULL;
    
	//循环读取复制每一个元素
	cocos2d::CCDICT_FOREACH(TDic, TElement)
	{
		std::shared_ptr<rapidjson::Value> TValueData;
		const std::string TKey = TElement->getStrKey();
		cocos2d::Ref *TObj = TElement->getObject();
        
		if (dynamic_cast<cocos2d::__Dictionary *>(TObj))
		{
			TValueData = this->DicParseJSON((cocos2d::__Dictionary *)TObj, allocator);
		}
		else if (dynamic_cast<cocos2d::__Array *>(TObj))
		{
			TValueData = this->ArParseJSON((cocos2d::__Array *)TObj, allocator);
		}
		else
		{
			TValueData = this->ValueParseJSON(TObj, allocator);
		}
        
		char *TCopyData = CopyChar(TKey.c_str(), TKey.size());
		(*TValue).AddMember(TCopyData, *TValueData, allocator);
        
	}
    
	return TValue;
}

std::shared_ptr<rapidjson::Value> Json::ValueParseJSON(cocos2d::Ref *TData, rapidjson::Document::AllocatorType& allocator)
{
	if (dynamic_cast<cocos2d::__String *>(TData))
	{
		std::string TCh = ((cocos2d::__String *)TData)->_string;
		char *TCopyData = CopyChar(TCh.c_str(), TCh.size());
		return std::shared_ptr<rapidjson::Value>(new rapidjson::Value(TCopyData));
	}
	else if (dynamic_cast<cocos2d::__Integer *>(TData))
	{
		return std::shared_ptr<rapidjson::Value>(new rapidjson::Value(((cocos2d::__Integer *)TData)->getValue()));
	}
	else if (dynamic_cast<cocos2d::__Double *>(TData))
	{
		return std::shared_ptr<rapidjson::Value>(new rapidjson::Value(((cocos2d::__Double *)TData)->getValue()));
	}
	else if (dynamic_cast<cocos2d::__Float *>(TData))
	{
		return std::shared_ptr<rapidjson::Value>(new rapidjson::Value(((cocos2d::__Float *)TData)->getValue()));
	}
	else
	{
		CCLOG("暂不支持该类型转换！");
		return NULL;
	}
}