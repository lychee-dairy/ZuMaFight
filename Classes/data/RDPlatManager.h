//
//  PlayerData.h
//  UGameMarMonst
//
//  Created by linminglu on 14-6-3.
//
//

#ifndef __ZHUMAFIGHT__RDPlatManagerData__
#define __ZHUMAFIGHT__RDPlatManagerData__

#include <iostream>
#include "cocos2d.h"
#include "CSingleton.h"
#include "Header.h"

enum ADTYPE
{
	EN_ADS_NONE,
	EN_ADS_BANER,
	EN_ADS_INSTER,
};

class RDPlatManager : public Singleton < RDPlatManager >
{
public:

	~RDPlatManager();

	//adomb ads
	void showAdmobAds(ADTYPE _type,bool _visual);


private:
	void Common(cocos2d::__Dictionary *_value, int _CommondID); //connect to android 

private:
	RDPlatManager();


	void init();
	SINGLETON_FRIENDS;
};

#endif /*__ZHUMAFIGHT__RDPlatManagerData__*/
