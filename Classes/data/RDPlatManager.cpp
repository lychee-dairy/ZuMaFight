#include "RDPlatManager.h"
#include "CWJson.h"
#include "PlayerManager.h"

#if CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID
#    include "hellocpp/main.h"
#elif CC_TARGET_PLATFORM==CC_PLATFORM_IOS

#endif


#define RDPLAT_ADMOB 1001

RDPlatManager::RDPlatManager()
{

}

RDPlatManager::~RDPlatManager()
{


}


void RDPlatManager::init()
{


}

void RDPlatManager::showAdmobAds(ADTYPE _type, bool _visual)
{

	bool  hRet = false;

	//��ʾʱ��
	unsigned long long timestamp = time(NULL);
	struct tm *ptm = localtime((time_t*)&timestamp);
	int yaer = 1900+ptm->tm_year;
	int mon = ptm->tm_mon+1;
	int day = ptm->tm_mday;

	log("time data ====== %d:%d:%d", yaer, mon, day);

	int setYear = atoi(PlayerManager::instance()->getGameInfByName("ban_year").c_str());
	int setMon = atoi(PlayerManager::instance()->getGameInfByName("ban_month").c_str());
	int setDay = atoi(PlayerManager::instance()->getGameInfByName("ban_day").c_str());


	if (yaer > setYear)
	{
		hRet = true;
	}
	else if (yaer == setYear)
	{
		if (mon > setMon)
		{
			hRet = true;
		}
		else if (mon == setMon)
		{
			if (day >= setDay)
			{
				hRet = true;
			}
		}
	}


	if (hRet)
	{
		log("show ads success ===================");
		__Dictionary* dic = __Dictionary::create();
		dic->setObject(__Integer::create(_type), "adsType");
		if (_visual)
		{
			dic->setObject(__Integer::create(1), "visual");
		}
		else
		{
			dic->setObject(__Integer::create(0), "visual");
		}

		this->Common(dic, RDPLAT_ADMOB);
	}

}

void RDPlatManager::Common(cocos2d::__Dictionary*_value, int _CommondID)
{
#if CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID
	log("ANDROID Common==============");
	_value->setObject(__Integer::create(_CommondID), "commondID");
	string TValueData = CWIJson->DataEncode(_value);
	log("ANDROID Common %s",TValueData.c_str());
	JNI_JaveCommon(TValueData.c_str());
#elif CC_TARGET_PLATFORM==CC_PLATFORM_IOS
	log("IOS Common");

#else
	log("Win32 Common");
#endif	
}