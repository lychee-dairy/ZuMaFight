#include "MapEditeScene.h"
#include "LevelData.h"
#include "MathLayer.h"
#include "VisibleRect.h"
#include "PlayerManager.h"
#include "publicFunc.h"
#include "MenuLayer.h"
#include "RDSceneManager.h"
#include "EditBoxLayer.h"



MapEditeScene::MapEditeScene()
{
	m_LevelMapPoint.clear();
	m_ballVevtor.clear();
	m_nSelectStage = 1;
	m_touch = true;
	m_isDraw = false;
	m_editBox = nullptr;

	Director::getInstance()->getEventDispatcher()->addCustomEventListener(SAVEDATA, CC_CALLBACK_1(MapEditeScene::saveLevelData, this));
	Director::getInstance()->getEventDispatcher()->addCustomEventListener(ADDBALLTAG, CC_CALLBACK_1(MapEditeScene::setBallTag, this));
}

MapEditeScene::~MapEditeScene()
{
	CC_SAFE_DELETE(m_nlevelData);
	Director::getInstance()->getEventDispatcher()->removeEventListener(listener);
	//delete
	Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(SAVEDATA);
	Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(ADDBALLTAG);
}

Scene* MapEditeScene::createScene()
{
    auto scene = Scene::create();
    auto layer = MapEditeScene::create();
    scene->addChild(layer);
    return scene;
}

// on "init" you need to initialize your instance
bool MapEditeScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !MathLayer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	m_nSelectStage =PlayerManager::instance()->getCurrentTotalStage();

	m_nlevelData = new LevelData();
	m_nlevelData->initWithXML(m_nSelectStage, Size(568, 320));
    
	
	addUI();
	//addTips();
	m_pMenulayer->addSore(m_nlevelData->getLocationCount());



	ball = Sprite::createWithSpriteFrameName("ball1.png");
	ball->setPosition(m_nlevelData->getPosFromIndex(2));
	ball->setScale(0.6f);
	this->addChild(ball,201);
	m_nCurrentPosIndex = 0;

	if (m_nlevelData->getLocationCount() > 0 )
	{
		for (int i = 0; i < m_nlevelData->getLocationCount();i++)
		{
			Sprite* sp = Sprite::createWithSpriteFrameName("ball1.png");
			sp->setPosition(m_nlevelData->getPosFromIndex(i));
			sp->setScale(0.6f);
			this->addChild(sp, 200);
			m_ballVevtor.push_back(sp);
		}
	}


	this->schedule(schedule_selector(MapEditeScene::ballRuning), 0.05f);


	setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
	setTouchEnabled(true);


	//keypad
	//ע�Ჶ׽����
	listener = EventListenerKeyboard::create();
	listener->onKeyReleased = CC_CALLBACK_2(MapEditeScene::onKeyReleased, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);


    return true;
}

void MapEditeScene::menuCloseCallback(Ref* pSender)
{
	Node* node = (Node*)pSender;
	string name = node->getName();
	if (name == "save")
	{
		int lelve = PlayerManager::instance()->getCurrentTotalStage();
		if (m_nlevelData->getLocationCount() >=3)
		{
			if (m_nlevelData->savaLevelData(lelve))
			{
				LayerColor* layer = (LayerColor*)this->getChildByName("dialog");
				Menu* menu = (Menu*)layer->getChildByName("buttonmenu");
				((Node*)menu->getChildByName("cancel"))->setVisible(false);
				((Node*)menu->getChildByName("save"))->setVisible(false);

				Label* label1 = Label::create("Save success : \n e:/level-"+UGameTool::converInt2String(lelve)+".xml", "Arial", 15);
				label1->setPosition(Vec2(layer->getContentSize().width*0.5f,layer->getContentSize().height*0.8f));
				label1->setColor(ccc3(0, 0, 255));
				layer->addChild(label1);
			}
		}
	}
	else if (name == "menu")
	{
		RDSceneManager::instance()->RunTargetScene(EN_SCENE_MAINMENU);
	}
	else if (name == "cancel")
	{
		m_touch = true;
		this->removeChildByName("dialog");
	}
}


void MapEditeScene::ballRuning(float ft)
{
	if (m_nlevelData->getLocationCount() >= 3)
	{
		Point nextPos = getNextPos(ball->getPosition(), m_nCurrentPosIndex, 5);
		if (nextPos.x == ball->getPositionX() && nextPos.y == ball->getPositionY())
		{
			m_nCurrentPosIndex += 2;
			if (m_nCurrentPosIndex > m_nlevelData->getLocationCount())
			{
				m_nCurrentPosIndex = 2;
				ball->setPosition(m_nlevelData->getPosFromIndex(2));
				nextPos = getNextPos(ball->getPosition(), m_nCurrentPosIndex, 5);
			}
		}
		ball->setPosition(nextPos);
	}

}  


Point MapEditeScene::getNextPos(Point ptCurrent, int nIndex, float foffset)
{
	// check if it is circle or line...
	int nFirstIndex;
	if (nIndex % 2 == 0)
	{
		nFirstIndex = nIndex;
	}
	else
	{
		nFirstIndex = nIndex - 1;
	}

	Point zeroPt; zeroPt.x = 0; zeroPt.y = 0;

	if (foffset > 0 && m_nlevelData->getLocationCount() <= nFirstIndex + 2)
		return zeroPt;

	Point ptFirst = m_nlevelData->getPosFromIndex(nFirstIndex);
	Point ptSecond = m_nlevelData->getPosFromIndex(nFirstIndex + 1);
	Point ptThird = m_nlevelData->getPosFromIndex(nFirstIndex + 2);
	if (isLine(ptFirst, ptSecond, ptThird))  // it is a line
	{
		if (foffset > 0)
			return getNextPosFromLine(ptCurrent, m_nlevelData->getPosFromIndex(nIndex + 2), foffset);
		else
			return  getNextPosFromLine(ptCurrent, m_nlevelData->getPosFromIndex(nIndex), -foffset);
	}
	return getNextPosFromCircle(ptCurrent, nFirstIndex, foffset);
}

Point MapEditeScene::getNextPosFromLine(Point ptCurrent, Point ptTarget, float foffset)
{
	float nLargeLine = sqrt((ptCurrent.x - ptTarget.x)*(ptCurrent.x - ptTarget.x) + (ptCurrent.y - ptTarget.y)*(ptCurrent.y - ptTarget.y));
	float nDifX = ptTarget.x - ptCurrent.x;
	float nDifY = ptTarget.y - ptCurrent.y;
	float rX, rY;

	if (nLargeLine == 0)
	{
		return ptCurrent;
	}

	rX = (foffset*nDifX) / nLargeLine;
	rY = (foffset*nDifY) / nLargeLine;

	float nNewLargeLine = sqrt(rX*rX + rY*rY);

	if (nNewLargeLine > nLargeLine)
	{
		//if (/*m_fRemainedDist < 0 &&*/ nNewLargeLine - nLargeLine > 0)
		//{
		//	m_fRemainedDist = nLargeLine - nNewLargeLine;
		//}
		//else
		//{
		//	m_fRemainedDist = nNewLargeLine - nLargeLine;
		//}
		return ptTarget;
	}

	//m_fRemainedDist = 0;
	ptCurrent.x += rX;
	ptCurrent.y += rY;

	return ptCurrent;
}

void MapEditeScene::addUI()
{
	Sprite* sp = Sprite::create(m_nlevelData->getBGName());
	sp->setAnchorPoint(Vec2(0.5f, 0.5f));
	sp->setPosition(VisibleRect::center());
	this->addChild(sp);

	
	Layout* m_ui = dynamic_cast<Layout*>(SP_GUIReader->widgetFromJsonFile(m_nlevelData->getTrackFileNameJson().c_str()));
	m_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ui->setPosition(VisibleRect::center());
	this->addChild(m_ui, 2);

	m_pMenulayer = MenuLayer::create();
	m_pMenulayer->setEdit(true);
	this->addChild(m_pMenulayer, 60);
}


Point MapEditeScene::getNextPosFromCircle(Point ptCurrent, int nFirstIndex, float foffset)
{
	//int nums = m_LevelMapPoint.size
	Point zeroPt; zeroPt.x = 0; zeroPt.y = 0;
	if (foffset >= 0 && m_nlevelData->getLocationCount() <= (nFirstIndex + 2))
		return zeroPt;


	Point ptFirst = m_nlevelData->getPosFromIndex(nFirstIndex);
	Point ptSecond = m_nlevelData->getPosFromIndex(nFirstIndex + 1);
	Point ptThird = m_nlevelData->getPosFromIndex(nFirstIndex + 2);

	//    float nCheckValue = (ptFirst.x-ptSecond.x)*(ptThird.y-ptFirst.y)-(ptFirst.x-ptThird.x)*(ptSecond.y-ptFirst.y);
	//    if (nCheckValue > -0.01 && nCheckValue < 0.01)
	if (isLine(ptFirst, ptSecond, ptThird))
	{
		Point zeroPt; zeroPt.x = 0; zeroPt.y = 0;
		return zeroPt;
	}

	float cx = getCXOfCircleEquation(ptFirst, ptSecond, ptThird);
	float cy = getCYOfCircleEquation(ptFirst, ptSecond, ptThird);
	float r = getROfCircleEquation(ptFirst, ptSecond, ptThird);

	// get the way ( right or left)
	float alpha1 = acosfnear((m_nlevelData->getPosFromIndex(nFirstIndex).x - cx) / r);
	float alpha2 = asinfnear((m_nlevelData->getPosFromIndex(nFirstIndex).y - cy) / r);

	float firstAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	firstAlpha = (firstAlpha > 0) ? firstAlpha : firstAlpha + 2 * PI;

	alpha1 = acosfnear((m_nlevelData->getPosFromIndex(nFirstIndex + 1).x - cx) / r);
	alpha2 = asinfnear((m_nlevelData->getPosFromIndex(nFirstIndex + 1).y - cy) / r);
	float secondAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	secondAlpha = (secondAlpha > 0) ? secondAlpha : secondAlpha + 2 * PI;

	alpha1 = acosfnear((m_nlevelData->getPosFromIndex(nFirstIndex + 2).x - cx) / r);
	alpha2 = asinfnear((m_nlevelData->getPosFromIndex(nFirstIndex + 2).y - cy) / r);
	float thirdAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	thirdAlpha = (thirdAlpha > 0) ? thirdAlpha : thirdAlpha + 2 * PI;

	if (secondAlpha > firstAlpha && secondAlpha > thirdAlpha)
	{
		if (firstAlpha > PI)
			thirdAlpha += 2 * PI;
		else
			firstAlpha += 2 * PI;
	}
	else if (secondAlpha < firstAlpha && secondAlpha < thirdAlpha)
	{
		if (firstAlpha > PI)
		{
			secondAlpha += 2 * PI;
			thirdAlpha += 2 * PI;
		}
		else
		{
			firstAlpha += 2 * PI;
			secondAlpha += 2 * PI;
		}
	}

	float fArrow;
	if (firstAlpha > secondAlpha && secondAlpha > thirdAlpha)
	{
		fArrow = -1.0f;
	}
	else
	{
		fArrow = 1.0f;
	}

	// get the alpha of ptCurrent
	alpha1 = acosfnear((ptCurrent.x - cx) / r);
	alpha2 = asinfnear((ptCurrent.y - cy) / r);

	float orgAlpha = getAlphaFromArcSinCos(alpha1, alpha2);
	orgAlpha = (orgAlpha > 0) ? orgAlpha : orgAlpha + 2 * PI;
	orgAlpha = (firstAlpha < thirdAlpha && firstAlpha > 2 * PI && orgAlpha < 2 * PI) ? orgAlpha + 2 * PI : orgAlpha;
	orgAlpha = (firstAlpha > thirdAlpha && thirdAlpha > 2 * PI && orgAlpha < 2 * PI) ? orgAlpha + 2 * PI : orgAlpha;

	if (/*fArrow*fOffset < 0 && */firstAlpha > secondAlpha && firstAlpha > thirdAlpha && orgAlpha < thirdAlpha && firstAlpha - orgAlpha > PI)
	{
		orgAlpha += 2 * PI;
	}

	else if (/*fArrow*fOffset > 0 && */thirdAlpha > secondAlpha && thirdAlpha > firstAlpha && orgAlpha < firstAlpha && thirdAlpha - orgAlpha > PI)
	{
		orgAlpha += 2 * PI;
	}


	// get the next pos
	float alpha = orgAlpha + (foffset / r)*fArrow;
	while (alpha < 0)
	{
		alpha += 2 * PI;
	}
	Point ptNext;
	ptNext.x = cx + r*cos(alpha);
	ptNext.y = cy + r*sin(alpha);

	if (firstAlpha > secondAlpha)
	{
		if (orgAlpha > firstAlpha)
		{
			return ptFirst;
		}
		else if (orgAlpha < thirdAlpha)
		{
			return ptThird;
		}
	}
	else if (thirdAlpha > secondAlpha)
	{
		if (orgAlpha > thirdAlpha)
		{
			return ptThird;
		}
		else if (orgAlpha < firstAlpha)
		{
			return ptFirst;
		}
	}


	// check if the next pos is out of the last position
	/*    if ((orgAlpha > firstAlpha && orgAlpha < thirdAlpha) || (orgAlpha > thirdAlpha && orgAlpha < firstAlpha))
	{
	}
	else
	{
	m_fRemainedDist = 0;
	return ptNext;
	}
	*/
	if ((alpha >= firstAlpha && alpha <= thirdAlpha) || (alpha >= thirdAlpha && alpha <= firstAlpha))
	{
	}
	else
	{
		int nEndIndex = nFirstIndex + 2;
		if (foffset < 0)
			nEndIndex = nFirstIndex;
		ptNext = m_nlevelData->getPosFromIndex(nEndIndex);

		float fDist = (foffset > 0) ? thirdAlpha - orgAlpha : firstAlpha - orgAlpha;
		fDist = (fDist > 0) ? fDist : -fDist;
		fDist = (fDist > 2 * PI) ? fDist - 2 * PI : fDist;
		if (m_nlevelData > 0 && foffset - r*fDist < 0)
		{
			float fDist = (ptNext.x - ptCurrent.x)*(ptNext.x - ptCurrent.x) + (ptNext.y - ptCurrent.y)*(ptNext.y - ptCurrent.y);
			fDist = sqrtf(fDist);

			return ptNext;
		}

		//m_fRemainedDist = (foffset > 0) ? foffset - r*fDist : foffset + r*fDist;

		//        if (m_fRemainedDist < 0.01 && m_fRemainedDist > -0.01)
		//            m_fRemainedDist = 0;

		float fDist2 = (ptNext.x - ptCurrent.x)*(ptNext.x - ptCurrent.x) + (ptNext.y - ptCurrent.y)*(ptNext.y - ptCurrent.y);
		fDist2 = sqrtf(fDist2);

		return ptNext;
	}

	//m_fRemainedDist = 0;
	float fDist3 = (ptNext.x - ptCurrent.x)*(ptNext.x - ptCurrent.x) + (ptNext.y - ptCurrent.y)*(ptNext.y - ptCurrent.y);
	fDist3 = sqrtf(fDist3);

	return ptNext;
}

void MapEditeScene::addTips()
{
	LabelTTF* lblScoreShadowBg = LabelTTF::create("Hide and show HELP : Enter key 'H' ", "Marker Felt", 15);
	lblScoreShadowBg->setAnchorPoint(Vec2(1.0f, 1.0f));
	lblScoreShadowBg->setColor(ccc3(255, 255, 0));
	lblScoreShadowBg->setPosition(VisibleRect::rightTop());
	this->addChild(lblScoreShadowBg,10000);

	lblScoreShadowBg = LabelTTF::create("Save : Enter key 'S' ", "Marker Felt", 15);
	lblScoreShadowBg->setAnchorPoint(Vec2(1.0f, 1.0f));
	lblScoreShadowBg->setColor(ccc3(255,255, 0));
	lblScoreShadowBg->setPosition(VisibleRect::rightTop() - Vec2(0, 20));
	this->addChild(lblScoreShadowBg, 10000);

	lblScoreShadowBg = LabelTTF::create("back step: Enter key 'ESC' ", "Marker Felt", 15);
	lblScoreShadowBg->setAnchorPoint(Vec2(1.0f, 1.0f));
	lblScoreShadowBg->setColor(ccc3(255, 255, 0));
	lblScoreShadowBg->setPosition(VisibleRect::rightTop()-Vec2(0,40));
	this->addChild(lblScoreShadowBg, 10000);

}

void MapEditeScene::setBallTag(EventCustom* event)
{
	Value* tag = (Value*)event->getUserData();
	int ballTag = tag->asInt();
	m_nlevelData->addTagVect(ballTag);
	m_editBox = nullptr;
}

void MapEditeScene::saveLevelData(EventCustom* event)
{

	if (!this->getChildByName("dialog"))
	{
		m_touch = false;
		LayerColor* bakc = LayerColor::create(ccc4(128, 128, 128, 255), 200, 150);
		bakc->setPosition(Vec2(VisibleRect::getVisibleRect().size.width*0.3f, VisibleRect::getVisibleRect().size.height*0.3f));
		bakc->setName("dialog");
		this->addChild(bakc, 500);

		Label* label = Label::create("Sava", "Arial", 25);
		label->setColor(ccc3(255, 0, 0));
		MenuItemLabel* pRDTableView = MenuItemLabel::create(label, CC_CALLBACK_1(MapEditeScene::menuCloseCallback, this));
		pRDTableView->setName("save");
		pRDTableView->setPosition(Vec2(bakc->getContentSize().width*0.5f, bakc->getContentSize().height*0.7f));

		Label* label1 = Label::create("MainScene", "Arial", 25);
		label1->setColor(ccc3(255, 255, 0));
		MenuItemLabel* menus = MenuItemLabel::create(label1, CC_CALLBACK_1(MapEditeScene::menuCloseCallback, this));
		menus->setName("menu");
		menus->setPosition(Vec2(bakc->getContentSize().width*0.5f, bakc->getContentSize().height*0.5f));

		Label* label2 = Label::create("Cancel", "Arial", 25);
		label2->setColor(ccc3(0, 0, 0));
		MenuItemLabel* cancel = MenuItemLabel::create(label2, CC_CALLBACK_1(MapEditeScene::menuCloseCallback, this));
		cancel->setName("cancel");
		cancel->setPosition(Vec2(bakc->getContentSize().width*0.5f, bakc->getContentSize().height*0.3f));

		Menu* menu = Menu::create(menus, pRDTableView, cancel, nullptr);
		menu->setName("buttonmenu");
		menu->setPosition(Vec2(0.0f, 0.0f));
		bakc->addChild(menu);
	}
	
}

void MapEditeScene::backStep()
{
	if (m_editBox) return;

	int count = m_nlevelData->getLocationCount();
	if (count % 2 == 0 || count == 0) return;
	if (m_nlevelData->getLocationCount() > 3)
	{
		m_nlevelData->removePoint(m_nlevelData->getLocationCount() - 2, m_nlevelData->getLocationCount());

		if (m_ballVevtor.size() != 0)
		{
			int from = m_ballVevtor.size() - 2;
			int to = m_ballVevtor.size()-1;

			for (int i = from; i <= to; i++)
			{
				m_ballVevtor[i]->runAction(RemoveSelf::create(true));
			}
			m_ballVevtor.erase(m_ballVevtor.begin() + from, m_ballVevtor.begin() + to+1);
		}

		m_pMenulayer->addSore(-2);
		m_nlevelData->removeTag();
	}
	else
	{
		m_nlevelData->removePoint(0, m_nlevelData->getLocationCount());

		if (m_ballVevtor.size() != 0)
		{
			int from = 0;
			int to = m_ballVevtor.size() - 1;

			for (int i = m_ballVevtor.size() - 3; i <= m_ballVevtor.size() - 1; i++)
			{
				m_ballVevtor[i]->runAction(RemoveSelf::create(true));
			}
			m_ballVevtor.clear();
		}
		
		m_pMenulayer->addSore(-3);
		m_nlevelData->removeTag();
	}
	
	

}


bool MapEditeScene::onTouchBegan(Touch *touch, Event *unused_event)
{
	if (!m_touch) return false;

	Point touchPos = touch->getLocation();
	m_nlevelData->addPoint(touchPos);
	m_pMenulayer->addSore(1);

	Sprite* sp = Sprite::createWithSpriteFrameName("ball1.png");
	sp->setScale(0.6f);
	sp->setPosition(touchPos);
	this->addChild(sp,200);
	m_ballVevtor.push_back(sp);

	int nums = m_ballVevtor.size();
	if (nums > 1 && nums%2 != 0)
	{
		if (!m_editBox)
		{
			m_editBox = EditBoxLayer::create();
			m_editBox->setAnchorPoint(Vec2(0.5f,0.5f));
			m_editBox->setPosition(Vec2::ZERO);
			this->addChild(m_editBox,400);
		}
	}

	return true;
}
void MapEditeScene::onTouchMoved(Touch *touch, Event *unused_event)
{

}
void MapEditeScene::onTouchEnded(Touch *touch, Event *unused_event)
{

}
void MapEditeScene::onTouchCancelled(Touch *touch, Event *unused_event)
{


}

void MapEditeScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
	{
		backStep();
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_S)
	{
		//saveLevelData(NULL);
	}

}

void MapEditeScene::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	_customCommand.init(100);
	_customCommand.func = CC_CALLBACK_0(MapEditeScene::onDraw, this, transform, flags);
	renderer->addCommand(&_customCommand);
}

void MapEditeScene::onDraw(const Mat4 &transform, uint32_t flags)
{
	Director* director = Director::getInstance();
	director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
	director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);

	////draw
	CHECK_GL_ERROR_DEBUG();

	int nums = m_nlevelData->getLocationCount();
	if (nums >= 3)
	{

		m_isDraw = true;
		Point nextPos = m_nlevelData->getPosFromIndex(0);
		Point dotPos = m_nlevelData->getPosFromIndex(0);
		int currentIndex = 0;
		while (currentIndex <= nums - 3)
		{
			Point oldDotPos = dotPos;
			dotPos = getNextPos(dotPos, currentIndex, 2);
			if (dotPos == Vec2::ZERO)break;
			if (dotPos == oldDotPos || dotPos == nextPos)
			{
				currentIndex += 2;
				nextPos = m_nlevelData->getPosFromIndex(currentIndex);
			}

			Point ptFirst = m_nlevelData->getPosFromIndex(currentIndex);
			Point ptSecond = m_nlevelData->getPosFromIndex(currentIndex + 1);
			Point ptThird = m_nlevelData->getPosFromIndex(currentIndex + 2);
			if (isLine(ptFirst, ptSecond, ptThird))  // it is a line
			{
				DrawPrimitives::setDrawColor4B(255, 0, 0, 255);
			}
			else
			{
				DrawPrimitives::setDrawColor4B(255, 255, 0, 200);
			}
			DrawPrimitives::setPointSize(5);
			DrawPrimitives::drawPoint(dotPos);
		}
	}










	////DrawPrimitives::drawLine(start,pp);
	//int nums = m_nlevelData->getLocationCount();

	//for (int i = 0; i < nums; i++)
	//{
	//	if (i < (nums - 2) && i % 2 == 0)
	//	{
	//		Point firstPos = m_nlevelData->getPosFromIndex(i);
	//		Point secondPos = m_nlevelData->getPosFromIndex(i+1);
	//		Point thirdPos = m_nlevelData->getPosFromIndex(i+2);
	//		if (isLine(firstPos,secondPos,thirdPos))
	//		{
	//			DrawPrimitives::setPointSize(40);
	//			DrawPrimitives::setDrawColor4B(0, 0, 255, 200);
	//			DrawPrimitives::drawLine(firstPos, secondPos);
	//			DrawPrimitives::drawLine(secondPos, thirdPos);
	//		}
	//		else
	//		{
	//			
	//			/* Point currentPos = firstPos;
	//			 Point nextPos = getNextPosFromCircle(currentPos, i, 5);
	//			 while (currentPos != thirdPos)
	//			 {

	//			 Point pos = m_nlevelData->getPosFromIndex(i);
	//			 DrawPrimitives::setPointSize(5);
	//			 DrawPrimitives::setDrawColor4B(255, 0, 0, 200);
	//			 DrawPrimitives::drawPoint(nextPos);

	//			 currentPos = nextPos;
	//			 nextPos = getNextPosFromCircle(currentPos, i, 5);
	//			 }*/

	//		}
	//	
	//	}
	//	/*Point pos = m_nlevelData->getPosFromIndex(i);
	//	DrawPrimitives::setPointSize(15);
	//	DrawPrimitives::setDrawColor4B(255, 255, 0,255);
	//	DrawPrimitives::drawPoint(pos);*/
	//}

	

	//end draw
	director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}
