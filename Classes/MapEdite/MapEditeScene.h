#ifndef __MapEditeScene_H__
#define __MapEditeScene_H__

#include "cocos2d.h"
#include "MathLayer.h"
#include "Header.h"

class LevelData;
class MenuLayer;
class EditBoxLayer;

class MapEditeScene : public MathLayer
{
public:
  
	MapEditeScene();
	~MapEditeScene();

    static cocos2d::Scene* createScene();
    virtual bool init();  
    void menuCloseCallback(cocos2d::Ref* pSender);
    

    CREATE_FUNC(MapEditeScene);

	////touch
	virtual bool onTouchBegan(Touch *touch, Event *unused_event);
	virtual void onTouchMoved(Touch *touch, Event *unused_event);
	virtual void onTouchEnded(Touch *touch, Event *unused_event);
	virtual void onTouchCancelled(Touch *touch, Event *unused_event);

	//key pad
	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);


	//
	void ballRuning(float ft);
	virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override;

protected:
	void onDraw(const Mat4 &transform, uint32_t flags);
	CustomCommand _customCommand;

private:
	void addUI();

	Point getNextPosFromCircle(Point ptCurrent, int nFirstIndex, float foffset);
	Point getNextPos(Point ptCurrent, int nIndex, float foffset);
	Point getNextPosFromLine(Point ptCurrent, Point ptTarget, float foffset);

	// bask Step for the dot 
	void backStep();
	void saveLevelData(EventCustom* event);
	void setBallTag(EventCustom* event);

	void addTips();
private:
	MenuLayer* m_pMenulayer;
	EditBoxLayer* m_editBox;

	CC_SYNTHESIZE(int, m_nSelectStage, SelectStage);
	EventListenerKeyboard* listener;
	LevelData* m_nlevelData;
	Sprite* ball;
	int m_nCurrentPosIndex;

	vector<Point> m_LevelMapPoint;
	vector<Sprite*> m_ballVevtor;

	bool m_touch;


	bool m_isDraw;
};

#endif // __MapEditeScene_SCENE_H__
