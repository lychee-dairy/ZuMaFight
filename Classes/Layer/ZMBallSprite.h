/**
*@time : 2014 12 2
*@author : rainDreamer
*@des: a zhumaBallSprite
*/
#ifndef _ZM_ZMBallSprite_h
#define _ZM_ZMBallSprite_h

#include "Header.h"
class ZMBallSprite: public Node
{
public:
	ZMBallSprite();
	~ZMBallSprite();

	static ZMBallSprite* create(EN_ZM_TYPE nBallType,int totalTypes);

	bool init(EN_ZM_TYPE nBallType,int totalTypes=1);
	void changleType(EN_ZM_TYPE nBallType);

	void changleToPro(EN_PRO_TYPE nProType);

	void updateColorType(float ft);
	void openUpdateColorType();
private:
	Sprite* ball;

	int m_totalTypes; //当前关卡的种类数
	int m_indexTypes;

	//shuxin 
	CC_SYNTHESIZE(EN_ZM_TYPE, m_nBallType, BallType);
	CC_SYNTHESIZE(Rect, m_nBallSize, BallSize);
	CC_SYNTHESIZE(int, m_nNextTargetPosIndex, NextTargetPosIndex);

	CC_SYNTHESIZE(EN_PRO_TYPE, m_nProType, ProType);
	CC_SYNTHESIZE(bool, m_nDeleteTag, DeleteTag);
};

#endif /*_ZM_ZMBallSprite_h*/
