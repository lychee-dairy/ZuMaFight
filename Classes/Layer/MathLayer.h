
#ifndef _ZHUMAFIGHT_MathLayer_H_
#define _ZHUMAFIGHT_MathLayer_H_

#include "cocos2d.h"
USING_NS_CC;
using namespace std;

class MathLayer :public Layer
{
public:
	MathLayer();
	~MathLayer();

	CREATE_FUNC(MathLayer);

	virtual bool init();

protected:
	bool isLine(Point ptFirst, Point ptSecond , Point ptThird);
	float getParamOfCircleEquation(int nParamIndex ,Point ptFirst ,Point ptSecond , Point ptThird);
	float getCXOfCircleEquation(Point ptFirst,Point ptSecond,Point ptThird);
	float getCYOfCircleEquation(Point ptFirst, Point ptSecond , Point ptThird);
	float getROfCircleEquation(Point ptFirst,Point ptSecond , Point ptThird);
	float getParamOfLineEquation(int nParamIndex,Point ptFrom ,Point ptTo); // y=ax+b
	float getAOfLineEquation(Point ptFrom,Point ptTo);   // y=ax+b
	float getBOfLineEquation(Point ptFrom , Point ptTo);   // y=ax+b
	float intersectionEquationParamOfLineAndCircle(int nParam , Point ptFrom ,Point ptTo , int cx , int cy , int r);
	float intersectionEquationYOfLineAndCircle(Point ptFrom , Point ptTo , int cx , int cy , int r);
	float intersectionEquationXOfLineAndCircle(Point ptFrom , Point ptTo , int cx , int cy , int r);
	float getAlphaFromArcSinCos(float acosValue , float asinValue);
	float getAngleBetweenTowLines(Point ptFirstFrom , Point ptFirstTo , Point ptSecondFrom , Point ptSecondTo);
	float asinfnear(float sinValue);
	float acosfnear(float cosValue);
	float calcRadianFromThreePoints(Point ptFirst , Point ptSecond , Point ptThird);
	float absf(float fValue);

	Point getCircleControlPointByThreetPoint(Point ptFirst, Point ptSecond, Point ptThird);

	

private:

};
#endif _ZHUMAFIGHT_MathLayer_H_
