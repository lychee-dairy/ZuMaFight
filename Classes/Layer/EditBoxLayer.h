
#ifndef _ZHUMAFIGHT_EditBoxLayer_H_
#define _ZHUMAFIGHT_EditBoxLayer_H_

#include "cocos2d.h"
#include "Header.h"

class EditBoxLayer :public LayerColor, ui::EditBoxDelegate
{
public:
	EditBoxLayer();
	~EditBoxLayer();

	CREATE_FUNC(EditBoxLayer);

	virtual bool init();

	virtual bool onTouchBegan(Touch *touch, Event *unused_event);


	virtual void editBoxReturn(ui::EditBox* editBox);
private:
	void addUI();
	void buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type);
	void okCallback(Ref* pSender);
	
private:
	EventListenerTouchOneByOne* m_touch;
	Layout* m_ui;
	ui::EditBox* _editName;

	int m_Zorder;
};
#endif _ZHUMAFIGHT_EditBoxLayer_H_
