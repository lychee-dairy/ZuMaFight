#include "GameOverLayer.h"
#include "RDSceneManager.h"
#include "PlayerManager.h"

GameOverLayer::GameOverLayer()
{

}

GameOverLayer::~GameOverLayer()
{

}

bool GameOverLayer::init()
{
	if (!LayerColor::init()) return false;

	this->initWithColor(Color4B(0.f, 0.f, 0.f, Transparency));
	this->setContentSize(VisibleRect::getVisibleRect().size);

	m_touch = cocos2d::EventListenerTouchOneByOne::create();
	m_touch->onTouchBegan = CC_CALLBACK_2(GameOverLayer::onTouchBegan, this);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(m_touch, this);
	m_touch->setSwallowTouches(true);

	return true;
}


bool GameOverLayer::onTouchBegan(Touch *touch, Event *unused_event)
{

	return true;
}

void GameOverLayer::addUI()
{
	m_ui = dynamic_cast<Layout*>(SP_GUIReader->widgetFromJsonFile("fight/ui/gameOver.ExportJson"));
	m_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ui->setPosition(VisibleRect::center());
	this->addChild(m_ui, 2);

	Button* button = SEEK_WIDGET_BY_NAME(Button, "Button_replay", m_ui);
	button->addTouchEventListener(CC_CALLBACK_2(GameOverLayer::buttonOver, this));

	button = SEEK_WIDGET_BY_NAME(Button, "Button_cotinue", m_ui);  
	button->addTouchEventListener(CC_CALLBACK_2(GameOverLayer::buttonOver, this));

	TextBMFont* score = SEEK_WIDGET_BY_NAME(TextBMFont , "score_num" , m_ui);
	score->setString(UGameTool::converInt2String(m_gameInf["score"].asInt()));

	TextBMFont* time = SEEK_WIDGET_BY_NAME(TextBMFont, "time_num", m_ui);
	time->setString(m_gameInf["time"].asString());
}

void GameOverLayer::addGameInf(ValueMap& gameInf)
{
	m_gameInf = gameInf;
	addUI();
}

void GameOverLayer::buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type)
{
	Node* node = (Node*)sender;

	if (type == Widget::TouchEventType::ENDED)
	{
		if (strcmp("Button_replay", node->getName().c_str())==0)
		{
			this->runAction(RemoveSelf::create(true));
			Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(REPLAYGAME);
		}
		else if (strcmp("Button_cotinue", node->getName().c_str())==0)
		{
			AnimotePack::getInstance()->showEaseScallOutAndFadeOut(m_ui, CallFunc::create([this]()
			{
				this->runAction(RemoveSelf::create(true));
				RDSceneManager::instance()->RunTargetScene(EN_SCENE_SELECTSTAGE, PlayerManager::instance()->getCurrentTotalStage());
			}));
		}
	}
}