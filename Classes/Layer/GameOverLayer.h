
#ifndef _ZHUMAFIGHT_GameOverLayer_H_
#define _ZHUMAFIGHT_GameOverLayer_H_

#include "cocos2d.h"
#include "Header.h"

class GameOverLayer :public LayerColor
{
public:
	GameOverLayer();
	~GameOverLayer();

	CREATE_FUNC(GameOverLayer);

	virtual bool init();

	virtual bool onTouchBegan(Touch *touch, Event *unused_event);


	void addGameInf(ValueMap& gameInf);
private:
	void addUI();
	void buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type);

private:
	EventListenerTouchOneByOne* m_touch;
	Layout* m_ui;

	ValueMap m_gameInf;
};
#endif _ZHUMAFIGHT_GameOverLayer_H_
