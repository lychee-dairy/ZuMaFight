#include "EditBoxLayer.h"
#include "RDSceneManager.h"

EditBoxLayer::EditBoxLayer()
{

}

EditBoxLayer::~EditBoxLayer()
{

}

bool EditBoxLayer::init()
{
	if (!LayerColor::init()) return false;

	this->initWithColor(Color4B(0.f, 0.f, 0.f, Transparency));
	this->setContentSize(VisibleRect::getVisibleRect().size);

	m_touch = cocos2d::EventListenerTouchOneByOne::create();
	m_touch->onTouchBegan = CC_CALLBACK_2(EditBoxLayer::onTouchBegan, this);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(m_touch, this);
	m_touch->setSwallowTouches(true);

	addUI();
	return true;
}


bool EditBoxLayer::onTouchBegan(Touch *touch, Event *unused_event)
{

	return true;
}

void EditBoxLayer::addUI()
{
	LayerColor* bakc = LayerColor::create(ccc4(128, 128, 128, 255), 200, 100);
	bakc->setPosition(Vec2(VisibleRect::getVisibleRect().size.width*0.3f, VisibleRect::getVisibleRect().size.height*0.3f));
	bakc->setName("dialog");
	this->addChild(bakc, 10);

	Label* label = Label::create("Z:", "Arial", 25);
	label->setColor(ccc3(0, 0, 0));
	label->setPosition(Vec2(bakc->getContentSize().width*0.2f, bakc->getContentSize().height*0.7f));
	bakc->addChild(label);

	Label* label2 = Label::create("OK", "Arial", 25);
	label2->setColor(ccc3(255, 0, 0));
	MenuItemLabel* ok = MenuItemLabel::create(label2, CC_CALLBACK_1(EditBoxLayer::okCallback, this));
	ok->setName("ok");
	ok->setPosition(Vec2(bakc->getContentSize().width*0.5f, bakc->getContentSize().height*0.3f));

	Menu* menu = Menu::create(ok, nullptr);
	menu->setName("buttonmenu");
	menu->setPosition(Vec2(0.0f, 0.0f));
	bakc->addChild(menu);


	ui::Scale9Sprite* back = ui::Scale9Sprite::create("editBac.png");
	_editName = ui::EditBox::create(Size(100, 50), back);
	_editName->setAnchorPoint(Vec2(0.0f, 0.5f));
	_editName->setPosition(Vec2(bakc->getContentSize().width*0.3f, bakc->getContentSize().height*0.7f));
	_editName->setFontName("Paint Boy");
	_editName->setFontSize(25);
	_editName->setFontColor(Color3B::RED);
	_editName->setPlaceHolder("1");
	_editName->setPlaceholderFontColor(Color3B::RED);
	_editName->setMaxLength(8);
	_editName->setReturnType(ui::EditBox::KeyboardReturnType::DONE);
	_editName->setDelegate(this);
	bakc->addChild(_editName,10);
}

void EditBoxLayer::buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type)
{
	Node* node = (Node*)sender;

	if (type == Widget::TouchEventType::ENDED)
	{
		
	}
}

void EditBoxLayer::okCallback(Ref* pSender)
{
	Node* node = (Node*)pSender;
	if (strcmp("ok", node->getName().c_str()) == 0)
	{
		int zorder = atoi(_editName->getText());
		if (zorder == 0)
		{
			zorder = 5;
		}
		Value values = Value(zorder);
		Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(ADDBALLTAG, &values);
		this->runAction(RemoveSelf::create(true));
	}
}



void EditBoxLayer::editBoxReturn(ui::EditBox* editBox)
{
	string str = editBox->getText();
	m_Zorder = atoi(str.c_str());
}