#include "PauseLayer.h"
#include "RDSceneManager.h"

PauseLayer::PauseLayer()
{

}

PauseLayer::~PauseLayer()
{

}

bool PauseLayer::init()
{
	if (!LayerColor::init()) return false;

	this->initWithColor(Color4B(0.f, 0.f, 0.f, Transparency));
	this->setContentSize(VisibleRect::getVisibleRect().size);

	m_touch = cocos2d::EventListenerTouchOneByOne::create();
	m_touch->onTouchBegan = CC_CALLBACK_2(PauseLayer::onTouchBegan, this);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(m_touch, this);
	m_touch->setSwallowTouches(true);

	addUI();
	return true;
}


bool PauseLayer::onTouchBegan(Touch *touch, Event *unused_event)
{

	return true;
}

void PauseLayer::addUI()
{
	m_ui = dynamic_cast<Layout*>(SP_GUIReader->widgetFromJsonFile("fight/ui/pauseLayer.ExportJson"));
	m_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ui->setPosition(VisibleRect::center());
	this->addChild(m_ui, 2);

	Button* button = SEEK_WIDGET_BY_NAME(Button, "Button_exit", m_ui);
	button->addTouchEventListener(CC_CALLBACK_2(PauseLayer::buttonOver, this));

	button = SEEK_WIDGET_BY_NAME(Button, "Button_continute", m_ui);  
	button->addTouchEventListener(CC_CALLBACK_2(PauseLayer::buttonOver, this));

	button = SEEK_WIDGET_BY_NAME(Button, "Button_newGame", m_ui);
	button->addTouchEventListener(CC_CALLBACK_2(PauseLayer::buttonOver, this));

}

void PauseLayer::buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type)
{
	Node* node = (Node*)sender;

	if (type == Widget::TouchEventType::ENDED)
	{
		if (strcmp("Button_exit", node->getName().c_str())==0)
		{
			RDSceneManager::instance()->RunTargetScene(EN_SCENE_SELECTSTAGE);
		}
		else if (strcmp("Button_continute", node->getName().c_str())==0)
		{
			AnimotePack::getInstance()->showEaseScallOutAndFadeOut(m_ui, CallFunc::create([this]()
			{
				Value value = Value(false);
				Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(PAUSEGAME, &value);
				this->runAction(RemoveSelf::create(true));
			}));
		}
		else if (strcmp("Button_newGame",node->getName().c_str()) == 0)
		{
			Value value = Value(false);
			Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(PAUSEGAME, &value);
			Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(REPLAYGAME);
			this->runAction(RemoveSelf::create(true));
		}
	}
}