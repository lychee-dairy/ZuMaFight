#include "MenuLayer.h"
#include "RDSceneManager.h"
#include "PlayerManager.h"

MenuLayer::MenuLayer()
{
	m_nCurrFenShu = 0;
	m_nNowShowScore = 0;
	m_isEdit = false;
}

MenuLayer::~MenuLayer()
{
	//SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("");
	//Director::getInstance()->getTextureCache()->removeTextureForKey("");
}

bool MenuLayer::init()
{
	if (!Layer::init()) return false;

	addUI();

	schedule(schedule_selector(MenuLayer::runChangeScore), 0.01f);
	return true;
}

void MenuLayer::addUI()
{
	m_ui = dynamic_cast<Layout*>(SP_GUIReader->widgetFromJsonFile("fight/ui/fight_menu.ExportJson"));
	m_ui->setAnchorPoint(Vec2(0.0f,0.0f));
	m_ui->setScale(0.9f);
	m_ui->setPosition(Vec2(VisibleRect::top().x - m_ui->getContentSize().width*0.5*m_ui->getScale(), VisibleRect::top().y - m_ui->getContentSize().height*m_ui->getScale()*0.98f));
	this->addChild(m_ui, 2);
	

	Button* image = SEEK_WIDGET_BY_NAME(Button, "Button_pause", m_ui);
	image->addTouchEventListener(CC_CALLBACK_2(MenuLayer::buttonOver, this));

	ImageView* scoreBack = SEEK_WIDGET_BY_NAME(ImageView, "score_back_0", m_ui);

	m_pScoreLabel = SEEK_WIDGET_BY_NAME(TextBMFont, "m_scoreNums", m_ui);
	m_pScoreLabel->setString(UGameTool::converInt2String(0));

	m_pStageLabel = SEEK_WIDGET_BY_NAME(TextBMFont, "m_StageNums", m_ui);
	m_pStageLabel->setString("1/30");
}

void MenuLayer::initLevel(int stage)
{
	string str = UGameTool::converInt2String(stage) + "/" + UGameTool::converInt2String(PlayerManager::instance()->getTotalStage());
	m_pStageLabel->setString(str);
}

void MenuLayer::addSore(int score)
{
	m_nCurrFenShu += score;
}

void MenuLayer::runChangeScore(float delta)
{
	//temScore:当前分数;
	//mScore:最后的显示分数;
	int temScore = m_nNowShowScore;
	int addScore = m_nCurrFenShu - temScore;

	if (abs(addScore) > 10) {
		temScore += addScore / 10;
	}
	else if (abs(addScore) > 2 && abs(addScore) <= 10){
		temScore += addScore / abs(addScore);
	}
	else{
		temScore = m_nCurrFenShu;
	}

	m_nNowShowScore = temScore;
	string str = UGameTool::converInt2String(temScore);
	if (NULL != m_pScoreLabel)
		m_pScoreLabel->setString(str);

}

void MenuLayer::buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type)
{
	Node* node = (Node*)sender;

	if (type == Widget::TouchEventType::ENDED)
	{
		if (strcmp("Button_pause", node->getName().c_str()) == 0)
		{
			if (!m_isEdit)
			{
				RDSceneManager::instance()->showPauseLayer();
				Value value = Value(true);
				Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(PAUSEGAME, &value);
				RDPlatManager::instance()->showAdmobAds(EN_ADS_INSTER, true);
			}
			else
			{
				Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(SAVEDATA);
			}
				
		}
	}
}