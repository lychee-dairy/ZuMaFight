
#ifndef _ZHUMAFIGHT_PauseLayer_H_
#define _ZHUMAFIGHT_PauseLayer_H_

#include "cocos2d.h"
#include "Header.h"

class PauseLayer :public LayerColor
{
public:
	PauseLayer();
	~PauseLayer();

	CREATE_FUNC(PauseLayer);

	virtual bool init();

	virtual bool onTouchBegan(Touch *touch, Event *unused_event);
private:
	void addUI();
	void buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type);



private:
	EventListenerTouchOneByOne* m_touch;
	Layout* m_ui;
};
#endif _ZHUMAFIGHT_PauseLayer_H_
