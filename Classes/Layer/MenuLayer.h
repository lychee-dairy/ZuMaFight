
#ifndef _ZHUMAFIGHT_MenuLayer_H_
#define _ZHUMAFIGHT_MenuLayer_H_

#include "cocos2d.h"
#include "Header.h"

class MenuLayer :public Layer
{
public:
	MenuLayer();
	~MenuLayer();

	CREATE_FUNC(MenuLayer);

	virtual bool init();


	void addSore(int score);
	void initLevel(int stage);

private:
	void addUI();
	void buttonOver(cocos2d::Ref *sender, Widget::TouchEventType type);


	void runChangeScore(float delta);
private:
	Layout* m_ui;
	TextBMFont* m_pScoreLabel;
	TextBMFont* m_pStageLabel;

	CC_SYNTHESIZE(int, m_nCurrFenShu, CurrFenShu);
	CC_SYNTHESIZE(int, m_nNowShowScore, NowShowScore);

	CC_SYNTHESIZE(bool, m_isEdit, Edit);
};
#endif _ZHUMAFIGHT_MenuLayer_H_
