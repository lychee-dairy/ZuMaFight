#include "ZMBallSprite.h"
#include "publicFunc.h"
#include "AnimotePack.h"

ZMBallSprite::ZMBallSprite()
{
	m_nProType = EN_PRO_NONE;
	m_indexTypes = 1;
}

ZMBallSprite::~ZMBallSprite()
{

}

ZMBallSprite* ZMBallSprite::create(EN_ZM_TYPE nBallType,int totalTypes)
{
	ZMBallSprite *sprite = new (std::nothrow) ZMBallSprite();
	if (sprite && sprite->init(nBallType,totalTypes))
	{
		sprite->autorelease();
		return sprite;
	}

	CC_SAFE_DELETE(sprite);
	return nullptr;
}

bool ZMBallSprite::init(EN_ZM_TYPE nBallType, int totalTypes)
{
	//init shuxin
	m_nBallType = nBallType;
	m_nNextTargetPosIndex = 0;
	m_nDeleteTag = false;

	m_totalTypes = totalTypes;
	

	//init sprite
	switch (nBallType)
	{
	case EN_ZM_NONE:
		return false;
		break;
	case EN_ZM_Ball1:
	case EN_ZM_Ball2:
	case EN_ZM_Ball3: 
	case EN_ZM_Ball4:
	case EN_ZM_Ball5:
	case EN_ZM_Ball6:
	{
		//ball = Sprite::create("ball" + UGameTool::converInt2String(nBallType)+".png");
		ball = Sprite::createWithSpriteFrameName("ball" + UGameTool::converInt2String(nBallType) + ".png");
		ball->setScale(0.6f);
		this->addChild(ball, 0);

		Label* label = Label::create("", "����", 15);
		label->setString(UGameTool::converInt2String(this->getLocalZOrder()));
		label->setName("label");
		//this->addChild(label,2);

		m_nBallSize = Rect(0,0,21.6f,21.6f);
		break;
	}
	default:
		return false;
		break;
	}

	return true;
}

void ZMBallSprite::changleType(EN_ZM_TYPE nBallType)
{
	if (m_nBallType == nBallType) return;

	m_nBallType = nBallType;
	ball->setDisplayFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ball" + UGameTool::converInt2String(nBallType) + ".png"));
}

void ZMBallSprite::openUpdateColorType()
{
	this->schedule(schedule_selector(ZMBallSprite::updateColorType),1.5f);
}

void ZMBallSprite::updateColorType(float ft)
{
	m_indexTypes++;
	if (m_indexTypes > m_totalTypes)
	{
		m_indexTypes = 1;
	}
	
	changleType((EN_ZM_TYPE)m_indexTypes);

}

void ZMBallSprite::changleToPro(EN_PRO_TYPE nProType)
{
	m_nProType = nProType;
	switch (nProType)
	{
	case EN_PRO_NONE:
	{
		this->unschedule(schedule_selector(ZMBallSprite::updateColorType));
	}
		break;
	case EN_PRO_STOP:
	{
		Sprite* sp = Sprite::create("effect/x_effect_pause.png");
		sp->setPosition(ball->getContentSize()/2);
		sp->setScale(0.8f);
		ball->addChild(sp);
	}
		break;
	case EN_PRO_BOMB:
	{
		Sprite* sp = Sprite::create("effect/x_effect_bomb.png");
		sp->setPosition(ball->getContentSize() / 2);
		sp->setScale(0.8f);
		ball->addChild(sp);
	}
		break;
	case EN_PRO_BACK:
	{
		Sprite* sp = Sprite::create("effect/x_effect_back.png");
		sp->setPosition(ball->getContentSize() / 2);
		sp->setScale(0.8f);
		ball->addChild(sp);
	}
		break;
	case EN_PRO_CHANGECOLOR:
	{
		/*Animate* a = AnimotePack::getInstance()->AddAnimate("ball", 0.2f, 6, "changecolor", Widget::TextureResType::PLIST);
		ball->runAction(RepeatForever::create(a));*/
		openUpdateColorType();
	}
		break;
	case EN_PRO_GO:
	{
		Sprite* sp = Sprite::create("effect/x_effect_go.png");
		sp->setPosition(ball->getContentSize() / 2);
		sp->setScale(0.8f);
		ball->addChild(sp);
	}
		break;
	case EN_RPO_GETSCORE:
	{
		Sprite* sp = Sprite::create("effect/m_effect_money.png");
		sp->setPosition(ball->getContentSize() / 2);
		ball->addChild(sp);
	}
		break;
	default:
		break;
	}
}