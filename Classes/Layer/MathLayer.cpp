#include "MathLayer.h"
#include "Header.h"

MathLayer::MathLayer()
{

}

MathLayer::~MathLayer()
{

}

bool MathLayer::init()
{
	if (!Layer::init()) return false;
	

	return true;
}  

bool MathLayer::isLine(Point ptFirst, Point ptSecond , Point ptThird)
{
	float fAngle =getAngleBetweenTowLines(ptFirst , ptSecond ,ptFirst ,ptThird);

	if (fAngle > -0.02f && fAngle < 0.02f)
		return true;

	return false;

	/*    float nCheckValue = (ptFirst.x-ptSecond.x)*(ptThird.y-ptFirst.y)-(ptFirst.x-ptThird.x)*(ptSecond.y-ptFirst.y);
	if (nCheckValue > -3 && nCheckValue < 3)   // it is a line
	return true;
	return false;*/
}

float MathLayer::getParamOfCircleEquation(int nParamIndex, Point ptFirst, Point ptSecond, Point ptThird)
{
	//    float nCheckValue = (ptFirst.x-ptSecond.x)*(ptThird.y-ptFirst.x)-(ptFirst.x-ptThird.x)*(ptSecond.y-ptFirst.y);
	//    if (nCheckValue > -3 && nCheckValue < 3)
	//    {
	//        return 0;
	//    }

	if (isLine(ptFirst, ptSecond, ptThird))
	{
		return 0;
	}

	// To avoid crashing because of zero /
	if (ptFirst.y == ptSecond.y)
	{
		Point temp = ptFirst;
		ptFirst = ptThird;
		ptThird = temp;
	}
	else if (ptFirst.y == ptThird.y)
	{
		Point temp = ptFirst;
		ptFirst = ptSecond;
		ptSecond = temp;
	}

	// get the position fo the centre of circle (cx, cy) and radius (r)
	float cx, cy, r;
	float fTemp1 = (ptThird.x*ptThird.x + ptThird.y*ptThird.y - ptFirst.x*ptFirst.x - ptFirst.y*ptFirst.y) / (2 * (ptThird.y - ptFirst.y));
	float fTemp2 = (ptSecond.x*ptSecond.x + ptSecond.y*ptSecond.y - ptFirst.x*ptFirst.x - ptFirst.y*ptFirst.y) / (2 * (ptSecond.y - ptFirst.y));
	float fTemp3 = (ptFirst.x - ptSecond.x) / (ptSecond.y - ptFirst.y) - (ptFirst.x - ptThird.x) / (ptThird.y - ptFirst.y);
	cx = (fTemp1 - fTemp2) / fTemp3;

	fTemp1 = 2 * (ptFirst.x - ptSecond.x)*cx + ptSecond.x*ptSecond.x + ptSecond.y*ptSecond.y - ptFirst.x*ptFirst.x - ptFirst.y*ptFirst.y;
	fTemp2 = 2 * (ptSecond.y - ptFirst.y);
	cy = fTemp1 / fTemp2;

	fTemp1 = (ptFirst.x - cx)*(ptFirst.x - cx) + (ptFirst.y - cy)*(ptFirst.y - cy);
	r = sqrt(fTemp1);

	if (nParamIndex == 0)
		return cx;
	else if (nParamIndex == 1)
		return cy;
	else if (nParamIndex == 2)
		return r;

	return 0;

}

float MathLayer::getCXOfCircleEquation(Point ptFirst, Point ptSecond, Point ptThird)
{
	return getParamOfCircleEquation(0 , ptFirst , ptSecond , ptThird);
}

float MathLayer::getCYOfCircleEquation(Point ptFirst, Point ptSecond, Point ptThird)
{
	return getParamOfCircleEquation(1 , ptFirst , ptSecond , ptThird);
}

float MathLayer::getROfCircleEquation(Point ptFirst, Point ptSecond, Point ptThird)
{
	return getParamOfCircleEquation(2 , ptFirst , ptSecond , ptThird);
}

float MathLayer::getParamOfLineEquation(int nParamIndex, Point ptFrom, Point ptTo)// y=ax+b
{
	if (ptFrom.x == ptTo.x)
		return  0;

	float a = (ptTo.y - ptFrom.y) / (ptTo.x - ptFrom.x);
	float b = ptFrom.y - ptFrom.x*a;

	if (nParamIndex == 0)
		return a;
	else if (nParamIndex == 1)
		return b;

	return 0;
}

float MathLayer::getAOfLineEquation(Point ptFrom, Point ptTo)// y=ax+b
{
	return getParamOfLineEquation(0 , ptFrom , ptTo);
}

float MathLayer::getBOfLineEquation(Point ptFrom, Point ptTo) // y=ax+b
{
	return getParamOfLineEquation(1,ptFrom ,ptTo);
}

float MathLayer::intersectionEquationParamOfLineAndCircle(int nParam, Point ptFrom, Point ptTo, int cx, int cy, int r)
{
	// get line equation y = mx+c
	float m = getAOfLineEquation(ptFrom , ptTo);
	float c = getBOfLineEquation(ptFrom , ptTo);

	// intersection equation Ax^2+Bx+C=0
	float A = m*m + 1;
	float B = 2 * (m*c - m*cy - cx);
	float C = cy*cy - r*r + cx*cx - 2 * c*cy + c*c;

	if (B*B - 4 * A*C < 0)
		return 0;

	float x1 = (-B + sqrt(B*B - 4 * A*C)) / (2 * A);
	float x2 = (-B - sqrt(B*B - 4 * A*C)) / (2 * A);

	if (abs(ptFrom.x - x1) > abs(ptFrom.x - x2))
		x1 = x2;

	if (nParam == 0)
	{
		return  x1;
	}
	else if (nParam == 1)
	{
		float y = m*x1 + c;
		return y;
	}

	return 0;
}

float MathLayer::intersectionEquationYOfLineAndCircle(Point ptFrom, Point ptTo, int cx, int cy, int r)
{
	return intersectionEquationParamOfLineAndCircle(1, ptFrom , ptTo , cx , cy , r);
}

float MathLayer::intersectionEquationXOfLineAndCircle(Point ptFrom, Point ptTo, int cx, int cy, int r)
{
	return intersectionEquationParamOfLineAndCircle(0 , ptFrom , ptTo , cx , cy , r);
}

float MathLayer::getAlphaFromArcSinCos(float acosValue, float asinValue)
{
	if (acosValue < 0)
		acosValue = 0;
	if (acosValue > PI)
		acosValue = PI;
	if (asinValue < -PI / 2)
		asinValue = -PI / 2;
	if (asinValue > PI / 2)
		asinValue = PI / 2;

	if (acosValue >= 0 && acosValue < PI / 2 &&
		asinValue >= -PI / 2 && asinValue < 0)
	{
		return asinValue;
	}
	else if (acosValue >= 0 && acosValue < PI / 2 &&
		asinValue >= 0 && asinValue < PI / 2)
	{
		return asinValue;
	}
	else if (acosValue >= PI / 2 && acosValue < PI &&
		asinValue >= -PI / 2 && asinValue < 0)
	{
		return -acosValue;
	}

	return acosValue;
}

float MathLayer::getAngleBetweenTowLines(Point ptFirstFrom, Point ptFirstTo, Point ptSecondFrom, Point ptSecondTo)
{
	ptSecondTo.x -= (ptSecondFrom.x - ptFirstFrom.x);
	ptSecondTo.y -= (ptSecondFrom.y - ptFirstFrom.y);
	ptSecondFrom = ptFirstFrom;

	ptFirstTo.x = ptFirstTo.x - ptFirstFrom.x;
	ptFirstTo.y = ptFirstTo.y - ptFirstFrom.y;
	ptSecondTo.x = ptSecondTo.x - ptSecondFrom.x;
	ptSecondTo.y = ptSecondTo.y - ptSecondFrom.y;
	ptFirstFrom.x = 0; ptFirstFrom.y = 0;
	ptSecondFrom.x = 0; ptSecondFrom.y = 0;

	float firstLineDaeGak = sqrt(ptFirstTo.x*ptFirstTo.x + ptFirstTo.y*ptFirstTo.y);
	if (ptFirstTo.x == 0)
		firstLineDaeGak = ptFirstTo.y;
	else if (ptFirstTo.y == 0)
		firstLineDaeGak = ptFirstTo.x;
	float a;
	a = ptFirstTo.x / firstLineDaeGak;
	float alpha1 = acosfnear(a);
	a = ptFirstTo.y / firstLineDaeGak;
	float alpha2 = asinfnear(a);
	float firstalpha = getAlphaFromArcSinCos(alpha1 , alpha2);
	float secondLineDaeGak = sqrt(ptSecondTo.x*ptSecondTo.x + ptSecondTo.y*ptSecondTo.y);
	if (ptSecondTo.x == 0)
		secondLineDaeGak = ptSecondTo.y;
	else if (ptSecondTo.y == 0)
		secondLineDaeGak = ptSecondTo.x;
	a = ptSecondTo.x / secondLineDaeGak;
	alpha1 = acosfnear(a);
	a = ptSecondTo.y / secondLineDaeGak;
	alpha2 = asinfnear(a);
	float secondalpha = getAlphaFromArcSinCos(alpha1 , alpha2);
	return firstalpha - secondalpha;
}

float MathLayer::asinfnear(float sinValue)
{
	float returnValue = sinValue;
	if (sinValue > 1/* && sinValue-1 < 3*/)
		returnValue = 1;
	if (sinValue < -1/* && (-1-sinValue) < 3*/)
		returnValue = -1;
	return asinf(returnValue);
}

float MathLayer::acosfnear(float cosValue)
{
	float returnValue = cosValue;
	if (cosValue > 1 && cosValue - 1 < 0.01)
		returnValue = 1;
	if (cosValue < -1 && (-1 - cosValue) < 0.01)
		returnValue = -1;
	return acosf(returnValue);
}

float MathLayer::calcRadianFromThreePoints(Point ptFirst, Point ptSecond, Point ptThird)
{
	// calculating the angle for ptSecond. I call ptFirst a, ptSecond b, and ptThird c.
	float ab = sqrt((ptFirst.x - ptSecond.x)*(ptFirst.x - ptSecond.x) + (ptFirst.y - ptSecond.y)*(ptFirst.y - ptSecond.y));
	float bc = sqrt((ptThird.x - ptSecond.x)*(ptThird.x - ptSecond.x) + (ptThird.y - ptSecond.y)*(ptThird.y - ptSecond.y));
	float ca = sqrt((ptFirst.x - ptThird.x)*(ptFirst.x - ptThird.x) + (ptFirst.y - ptThird.y)*(ptFirst.y - ptThird.y));
	float radian = (ab*ab + bc*bc - ca*ca) / (2 * ab*bc);
	return acosf(radian);
}

float MathLayer::absf(float fValue)
{
	float fReturnValue = fValue;

	if (fReturnValue < 0)
		fReturnValue = -fReturnValue;

	return fReturnValue;
}

Point MathLayer::getCircleControlPointByThreetPoint(Point ptFirst, Point ptSecond, Point ptThird)
{
	Point centerPos = Point(getCXOfCircleEquation(ptFirst, ptSecond, ptThird), getCYOfCircleEquation(ptFirst, ptSecond, ptThird));


	Point AB = ptFirst - centerPos;
	Point AC = ptThird - centerPos;

	float a1 = -(AB.x)/ AB.y;
	float b1 = -a1*ptFirst.x + ptFirst.y;



	float a2 = -(AC.x) / AC.y;
	float b2 = -a2*ptThird.x + ptThird.y;

	float x = (b2 - b1) / (a1 - a2);
	float y = a1*x + b1;

	return Point(x,y);
}
